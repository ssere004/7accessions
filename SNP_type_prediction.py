from preprocess import regression_preprocess as regrprep
import preprocess.data_reader as data_reader
import preprocess.preprocess as preprocess
import preprocess.configs as configs
import process.process as process
from sklearn.model_selection import train_test_split
from tensorflow.keras.layers import Activation,Dense
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Dropout, Flatten, Reshape
from tensorflow.keras.constraints import max_norm
from tensorflow.keras.losses import binary_crossentropy
import numpy as np
import tensorflow as tf
from sklearn.metrics import accuracy_score
import argparse

parser = argparse.ArgumentParser()

parser.add_argument('-acr', '--accession_name', help='name of the accession', required=True)
args = parser.parse_args()

sample_size = 20000
window_size = 128
tr_te_pct = 0.2
tr_val_pct = 0.1
cnfg = configs.col
rc = configs.rc
accession = args.accession_name
sequences_df = preprocess.convert_assembely_to_onehot(cnfg['og'], data_reader.readfasta(cnfg['assembly']), from_file=True)
meth_seq_dic = preprocess.get_meth_seq_dic(cnfg, rc, from_file=True)
meth_seq_dic = {contxt: {chro: np.expand_dims(meth_seq_dic[contxt][chro], axis=1) for chro in meth_seq_dic[contxt].keys()} for contxt in meth_seq_dic.keys()}
bw_dic = regrprep.get_bw_feature_seq(cnfg, from_file=True)

input_list = [sequences_df] + list(bw_dic.values()) + list(meth_seq_dic.values())
input_list = list(bw_dic.values()) + list(meth_seq_dic.values())

raw_snp_df = preprocess.read_raw_snps(accession)
sample_list = []
min_num_conversions = preprocess.find_min_conversion_num(raw_snp_df)
print('Each cotext sample size is ' + str(min(min_num_conversions, sample_size)))
for conv in [('AT', 'CG'), ('AT', 'GC'), ('AT', 'TA'), ('CG', 'GC'), ('CG', 'TA'), ('GC', 'TA')]:
    raw_snp_sub_df = raw_snp_df[((raw_snp_df.REF == conv[0][0]) & (raw_snp_df.ALT == conv[1][0])) |
                                ((raw_snp_df.REF == conv[0][1]) & (raw_snp_df.ALT == conv[1][1]))].sample(n=min(min_num_conversions, sample_size))
    sample_list.append(raw_snp_sub_df[['chr', 'position']])

X, Y = process.input_maker_contextbase(input_list, sample_list, window_size=window_size)
#X, Y = input_maker_contextbase(input_list, sample_list, window_size=window_size)
#convert Y to onehot encode for 6 conversions
Y = np.eye(len(sample_list))[Y.astype(int)]
x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=tr_te_pct, random_state=None)
x_train, x_val, y_train, y_val = train_test_split(x_train, y_train, test_size=tr_val_pct, random_state=None)

with tf.device('/device:GPU:0'):
    W_maxnorm = 3
    model = Sequential()
    model.add(Conv2D(64, kernel_size=(10, 10), activation='relu', input_shape=(X.shape[1], X.shape[2], 1), padding='same', kernel_constraint=max_norm(W_maxnorm)))
    model.add(MaxPooling2D(pool_size=(4, 4), strides=(1, 3)))
    model.add(Conv2D(256, kernel_size=(18, 8), activation='relu', padding='same', kernel_constraint=max_norm(W_maxnorm)))
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(1, 3)))
    model.add(Flatten())
    model.add(Dense(64, activation='relu'))
    model.add(Dropout(0.6))
    model.add(Dense(len(sample_list)))
    model.add(Activation('softmax'))
    myoptimizer = tf.keras.optimizers.Adam(learning_rate=1e-5)
    model.compile(loss=binary_crossentropy, optimizer=myoptimizer, metrics=['accuracy'])

model.fit(x_train, y_train, epochs=10, validation_data=(x_val, y_val), batch_size=64)
y_pred = model.predict(x_test)
accu = accuracy_score(y_test, np.eye(y_pred.shape[1])[np.argmax(y_pred, axis=1)])
print(accession, str(accu))
with open("snp_type_prediction_results.txt", "a") as file_object:
    file_object.write(accession + ' ' + str(accu))
    file_object.write("\n")

# python SNP_type_prediction.py --accession_name c24
# python SNP_type_prediction.py --accession_name mutation_bias
# python SNP_type_prediction.py --accession_name g1001_low
# python SNP_type_prediction.py --accession_name g1001_high




