from preprocess import data_reader
import pandas as pd
from preprocess import preprocess
import re
import random
import matplotlib.pyplot as plt
from datetime import datetime
#from pandarallel import pandarallel #uncomment this for the genic region real varibales computations.
import numpy as np
import re
import os
import time
import multiprocessing
from multiprocessing import Manager
import preprocess.constants as constants
from functools import partial
import concurrent
import evaluate as plotting
#from statsmodels.tools.eval_measures import rmse
#import statsmodels.api as sm

#for the regions outside the chromosme range it returns Zero
def average_bw_interval_seq(chro, start, stop, bw_seq):
    if stop <= start:
        return np.nan
    seq_ = bw_seq[chro][start:stop]
    return np.mean(seq_) if len(seq_) > 0 else 0

def gc_content_interval_seq(chro, start, stop, sequences_df):
    if stop <= start:
        return np.nan
    return (np.sum(sequences_df[chro][start:stop, 1]) + np.sum(sequences_df[chro][start:stop, 2]))/(stop-start)

def methylation_interval_seq(chro, start, stop, meth_seq, threshold=0.5):
    if stop <= start:
        return np.nan
    return np.sum(np.where(meth_seq[chro][start:stop] > threshold, 1, 0)) / (stop - start)

def count_snps_interval_seq(chro, start, stop, snp_seq, sequences_df, snp_cntx):
    if stop <= start:
        return np.nan
    if snp_cntx != None:
        ohdic = {'A': 0, 'C': 1, 'G': 2, 'T': 3}
        return np.sum(snp_seq[chro][start:stop])/(np.sum(sequences_df[chro][start:stop, ohdic[snp_cntx[0]]]) + np.sum(sequences_df[chro][start:stop, ohdic[snp_cntx[1]]]))
    else:
        return np.sum(snp_seq[chro][start:stop])/(stop - start)

#Gets a feature_df(which must have 'chr', 'start', 'stop' and 'length').
# For this regions based on our own bw files it calculates the features as well as snp rate
def make_feature_df(features_df, sequences_df, meth_seq_dic, bw_dic, snp_seq, specific_feature=None, snp_cntx=None):
    res_df = pd.DataFrame({'chr': features_df['chr'], 'start': features_df['start'], 'stop': features_df['stop'], 'length': features_df['length']})
    for epi_mark in bw_dic.keys():
        colname = re.split('_|-', epi_mark)[0]
        if colname == 'ATAC':
            colname = 'atac_pct'
        if specific_feature == None or specific_feature in colname:
            res_df[colname] = res_df.apply(lambda row: average_bw_interval_seq(row['chr'], row['start'], row['stop'], bw_dic[epi_mark]), axis=1)
    if specific_feature == None or specific_feature in 'GC_content_pct':
        res_df['GC_content_pct'] = res_df.apply(lambda row: gc_content_interval_seq(row['chr'], row['start'], row['stop'], sequences_df), axis=1)
    if len(meth_seq_dic) > 0:
        if specific_feature == None or specific_feature in 'CG_pct':
            res_df['CG_pct'] = res_df.apply(lambda row: methylation_interval_seq(row['chr'], row['start'], row['stop'], meth_seq_dic['CG']), axis=1)
        if specific_feature == None or specific_feature in 'CHG_pct':
            res_df['CHG_pct'] = res_df.apply(lambda row: methylation_interval_seq(row['chr'], row['start'], row['stop'], meth_seq_dic['CHG']), axis=1)
        if specific_feature == None or specific_feature in 'CHH_pct':
            res_df['CHH_pct'] = res_df.apply(lambda row: methylation_interval_seq(row['chr'], row['start'], row['stop'], meth_seq_dic['CHH']), axis=1)
    if specific_feature == None or specific_feature in 'MA_SNV_pct':
        res_df['MA_SNV_pct'] = res_df.apply(lambda row: count_snps_interval_seq(row['chr'], row['start'], row['stop'], snp_seq, sequences_df, snp_cntx), axis=1)
    return res_df

# def make_feature_df_parallel(args):
#     print('hello')
#     features_df, sequences_df, meth_seq_dic, snp_seq = args.fd, args.sd, args.md, args.ss
#     bw_dic_keys = ['H3K14ac-SRX1469119.bw', 'H3K23ac_merged.bw', 'H3K27ac_merged.bw', 'H3K27me1-SRX905130.bw',
#                'H3K36ac_merged.bw', 'H3K36me3-SRX1518744.bw', 'H3K4me1_merged.bw', 'H3K4me2_merged.bw',
#                'H3K4me3_merged.bw', 'H3K56ac-SRX1518745.bw', 'H3K9ac_merged.bw', 'H3K9me1-SRX361945.bw',
#                'H3K9me2_merged.bw', 'ATAC_merged.bw']
#     bw_dic = {attr_name: getattr(args, attr_name) for attr_name in bw_dic_keys}
#     specific_feature=None
#     snp_cntx=None
#     res_df = pd.DataFrame({'chr': features_df['chr'], 'start': features_df['start'], 'stop': features_df['stop'], 'length': features_df['length']})
#     for epi_mark in bw_dic.keys():
#         colname = re.split('_|-', epi_mark)[0]
#         if colname == 'ATAC':
#             colname = 'atac_pct'
#         if specific_feature == None or specific_feature in colname:
#             res_df[colname] = res_df.apply(lambda row: average_bw_interval_seq(row['chr'], row['start'], row['stop'], bw_dic[epi_mark]), axis=1)
#     if specific_feature == None or specific_feature in 'GC_content_pct':
#         res_df['GC_content_pct'] = res_df.apply(lambda row: gc_content_interval_seq(row['chr'], row['start'], row['stop'], sequences_df), axis=1)
#     if len(meth_seq_dic) > 0:
#         if specific_feature == None or specific_feature in 'CG_pct':
#             res_df['CG_pct'] = res_df.apply(lambda row: methylation_interval_seq(row['chr'], row['start'], row['stop'], meth_seq_dic['CG']), axis=1)
#         if specific_feature == None or specific_feature in 'CHG_pct':
#             res_df['CHG_pct'] = res_df.apply(lambda row: methylation_interval_seq(row['chr'], row['start'], row['stop'], meth_seq_dic['CHG']), axis=1)
#         if specific_feature == None or specific_feature in 'CHH_pct':
#             res_df['CHH_pct'] = res_df.apply(lambda row: methylation_interval_seq(row['chr'], row['start'], row['stop'], meth_seq_dic['CHH']), axis=1)
#     if specific_feature == None or specific_feature in 'MA_SNV_pct':
#         res_df['MA_SNV_pct'] = res_df.apply(lambda row: count_snps_interval_seq(row['chr'], row['start'], row['stop'], snp_seq, sequences_df, snp_cntx), axis=1)
#     return res_df


def make_feature_df_parallel(args):
    print('hello16')
    features_df, sequences_df, meth_seq_dic, snp_seq = args.fd, args.sd, args.md, args.ss
    print('mean of sequences df chr1', np.mean(sequences_df['chr1']))
    bw_dic_keys = ['H3K14ac-SRX1469119.bw', 'H3K23ac_merged.bw', 'H3K27ac_merged.bw', 'H3K27me1-SRX905130.bw',
               'H3K36ac_merged.bw', 'H3K36me3-SRX1518744.bw', 'H3K4me1_merged.bw', 'H3K4me2_merged.bw',
               'H3K4me3_merged.bw', 'H3K56ac-SRX1518745.bw', 'H3K9ac_merged.bw', 'H3K9me1-SRX361945.bw',
               'H3K9me2_merged.bw', 'ATAC_merged.bw']
    bw_dic = {attr_name: getattr(args, attr_name) for attr_name in bw_dic_keys}
    specific_feature=None
    snp_cntx=None
    res_df = pd.DataFrame({'chr': features_df['chr'], 'start': features_df['start'], 'stop': features_df['stop'], 'length': features_df['length']})
    for epi_mark in bw_dic.keys():
        colname = re.split('_|-', epi_mark)[0]
        if colname == 'ATAC':
            colname = 'atac_pct'
        if specific_feature == None or specific_feature in colname:
            res_df[colname] = res_df.apply(lambda row: average_bw_interval_seq(row['chr'], row['start'], row['stop'], bw_dic[epi_mark]), axis=1)
    if specific_feature == None or specific_feature in 'GC_content_pct':
        res_df['GC_content_pct'] = res_df.apply(lambda row: gc_content_interval_seq(row['chr'], row['start'], row['stop'], sequences_df), axis=1)
    if len(meth_seq_dic) > 0:
        if specific_feature == None or specific_feature in 'CG_pct':
            res_df['CG_pct'] = res_df.apply(lambda row: methylation_interval_seq(row['chr'], row['start'], row['stop'], meth_seq_dic['CG']), axis=1)
        if specific_feature == None or specific_feature in 'CHG_pct':
            res_df['CHG_pct'] = res_df.apply(lambda row: methylation_interval_seq(row['chr'], row['start'], row['stop'], meth_seq_dic['CHG']), axis=1)
        if specific_feature == None or specific_feature in 'CHH_pct':
            res_df['CHH_pct'] = res_df.apply(lambda row: methylation_interval_seq(row['chr'], row['start'], row['stop'], meth_seq_dic['CHH']), axis=1)
    if specific_feature == None or specific_feature in 'MA_SNV_pct':
        res_df['MA_SNV_pct'] = res_df.apply(lambda row: count_snps_interval_seq(row['chr'], row['start'], row['stop'], snp_seq, sequences_df, snp_cntx), axis=1)
    return res_df



def features_df_column_maker_general_interval(seq_dic, column_name, task_queue):
    reg_name = task_queue.get()
    features_df = pd.read_csv('./dump_files/general_intervals/'+reg_name+'.csv')
    if column_name == 'GC_content_pct':
        res = (column_name, reg_name, features_df.apply(lambda row: gc_content_interval_seq(row['chr'], row['start'], row['stop'], seq_dic), axis=1))
    elif 'bw' in column_name:
        colname = re.split('_|-', column_name)[0]
        if colname == 'ATAC': colname = 'atac_pct'
        res = (colname, reg_name, features_df.apply(lambda row: average_bw_interval_seq(row['chr'], row['start'], row['stop'], seq_dic), axis=1))
    elif column_name in ['CG_pct', 'CHG_pct', 'CHH_pct']:
        res = (column_name, reg_name, features_df.apply(lambda row: methylation_interval_seq(row['chr'], row['start'], row['stop'], seq_dic), axis=1))
    return res



#For each region it saves all the averaged feateures.
def save_general_interval_avgs_parallel(sequences_df, meth_seq_dic, bw_dic, bin_size=10, flanking_region=2000, num_cores=10):
    #condition on creating the general intervals or not.
    reg_names = [rg+'-'+str(i) for i in range(int(flanking_region/bin_size)) for rg in ['ds', 'gbl', 'gbr', 'us']]
    epi_marks = list(bw_dic.keys())
    seq_dics = [sequences_df] + [bw_dic[epi_mark] for epi_mark in epi_marks] + [meth_seq_dic[c_cntx] for c_cntx in ['CG', 'CHG', 'CHH']]
    colnames = ['GC_content_pct'] + [epi_mark for epi_mark in epi_marks] + [c_cntx for c_cntx in ['CG', 'CHG', 'CHH']]
    task_queue = multiprocessing.Queue()
    result_queue = multiprocessing.Queue()
    pool = [multiprocessing.Process(target=features_df_column_maker_general_interval, args=(seq_dic, task_queue)) for seq_dic, colname in list(zip(seq_dics, colnames))]
    for process in pool:
        process.start()
    for reg_name in reg_names:
        if os.path.exists('./dump_files/general_intervals_averages/'+reg_name+'.csv'):
            continue
        start_t = time.time()
        res_df = pd.DataFrame()
        task_queue.put((reg_name))
        for _ in pool:
            task_queue.put(None)
        results = []
        for _ in range(len(seq_dics)):
            res = result_queue.get()
            results.append(res)
        for colinfo in results:
            if colinfo != None:
                res_df[colinfo[0]] = colinfo[1]
        for process in pool:
            process.join()
        res_df.to_csv('./dump_files/general_intervals_averages/'+reg_name+'.csv')
        print('region ' + reg_name + ' Done!!!', time.time() - start_t)




import multiprocessing

def worker_process(task_queue, result_queue):
    while True:
        task = task_queue.get()
        if task is None:
            break
        result = process_task(task)
        result_queue.put(result)

def main_process(tasks):
    task_queue = multiprocessing.Queue()
    result_queue = multiprocessing.Queue()
    num_workers = multiprocessing.cpu_count()
    workers = [multiprocessing.Process(target=worker_process, args=(task_queue, result_queue)) for _ in range(num_workers)]
    for worker in workers:
        worker.start()
    for task in tasks:
        task_queue.put(task)
    for _ in range(num_workers):
        task_queue.put(None)
    results = []
    for _ in range(len(tasks)):
        result = result_queue.get()
        results.append(result)
    for worker in workers:
        worker.join()
    return results

def process_task(task):
    result = compute_result(task)
    return result

def compute_result(task):
    # Your computation logic here
    # This is just a placeholder, you should replace it with your actual computation logic
    return task * 2

if __name__ == "__main__":
    # Your list of tasks to be processed
    tasks = [1, 2, 3, 4, 5]

    # Call the main process function
    results = main_process(tasks)

    print(results)
