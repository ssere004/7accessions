import pandas as pd
from preprocess import data_reader
from preprocess import preprocess
import preprocess.configs as configs
import preprocess.regression_preprocess as regrprep
from sklearn.model_selection import train_test_split
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Dropout, Flatten, Reshape, Dense
import numpy as np
import evaluate
from sklearn.linear_model import LinearRegression
from argparse import Namespace
from scipy.stats import pearsonr

def snp_reg_random_intervals_from_one_seq(args, seq_dic):
    cnfg = configs.col
    rc = configs.rc
    cnfg_snp = configs.c24
    is_accession_spec = args.is_accession_spec
    conversion_base = args.conversion_base
    conv = args.conv
    sequences_df = preprocess.convert_assembely_to_onehot(cnfg['og'], data_reader.readfasta(cnfg['assembly']), from_file=True)
    random_intervals = regrprep.make_random_intervals_df(sequences_df, total_sample=20000)
    raw_snp_df = preprocess.read_raw_snps(cnfg_snp['og'])
    min_num_conversions = preprocess.find_min_conversion_num(raw_snp_df)
    if conversion_base:
        raw_snp_df = raw_snp_df[((raw_snp_df.REF == conv[0][0]) & (raw_snp_df.ALT == conv[1][0])) | ((raw_snp_df.REF == conv[0][1]) & (raw_snp_df.ALT == conv[1][1]))]
    snp_seq = preprocess.make_seq_dic(sequences_df, raw_snp_df)
    res_annot = regrprep.make_feature_df(random_intervals, sequences_df, {}, {}, snp_seq)
    Y = res_annot['MA_SNV_pct']
    X = random_intervals.apply(lambda row: seq_dic[row['chr']][row['start']:row['stop']], axis=1)
    X = np.stack(X)[:,:,0]
    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.1, random_state=42)
    model = LinearRegression()
    model.fit(X_train, y_train)
    y_pred = model.predict(X_test)
    corr_coef, p_value = pearsonr(y_test, y_pred)
    print('corr_coef, p_value : ' + str(corr_coef) + ' ' + str(p_value))

def make_gc_pct_bins(chro, start, stop, sequences_df, bin_size):
    if (stop - start) % bin_size != 0:
        raise Exception('Size of interval is not a bin size multiplier for GC pcnt bins ')
    if start == stop:
        raise Exception('stop == start !!!! ')
    if len(sequences_df[chro]) > stop and start > 0 and stop > 0:
        seqs_C = np.sum(sequences_df[chro][start:stop, 1].reshape((int((stop - start) / bin_size), bin_size)), axis=1)
        seqs_G = np.sum(sequences_df[chro][start:stop, 2].reshape((int((stop - start) / bin_size), bin_size)), axis=1)
        return (seqs_C + seqs_G) / bin_size
    else:
        return np.asarray([0]*(int((stop - start) / bin_size)))

def test_one_seq_tracks(args, seq_type):
    cnfg = configs.col
    cnfg_snp = preprocess.convert_og_to_cnfg(args.accession)
    rc = configs.rc
    bw_root = cnfg['bw_root']
    sequences_df = preprocess.convert_assembely_to_onehot(cnfg['og'], data_reader.readfasta(cnfg['assembly']), from_file=True)
    res_seq_dic = {}
    res_title = ''
    annot_df = data_reader.read_annot(cnfg['gene_annotation'])
    interval_length = 200
    if seq_type == 'annot':
        annot_df = annot_df[annot_df.type == 'gene']
        res_seq_dic = preprocess.make_annotseq_dic(annot_df, ['gene'], sequences_df, strand_spec=False)
        res_title = 'annot'
    if 'epi' in seq_type:
        bw_seqs, bw_names = preprocess.read_bwseqs(bw_root, from_file=True)
        for i in range(len(bw_names)):
            if seq_type.split('_')[-1] in bw_names[i]:
                res_seq_dic = bw_seqs[i]
                res_title = seq_type.split('_')[-1]
    if 'meth' in seq_type:
        context = seq_type.split('_')[-1]
        res_seq_dic = preprocess.get_meth_seq_dic(cnfg, rc, from_file=True)[context]
        res_title = seq_type
    corr_coef_lst = []
    for i in range(10):
        if args.random_intervals == 'genome-wide':
            random_intervals = regrprep.make_random_intervals_df(sequences_df, total_sample=50000)
        else:
            random_intervals = regrprep.make_random_intervals_genic_regions_df(annot_df[annot_df.type == 'gene'], interval_length=interval_length, total_sample=50000)
        raw_snp_df = preprocess.read_raw_snps(cnfg_snp['og'])
        snp_seq = preprocess.make_seq_dic(sequences_df, raw_snp_df)
        res_annot = regrprep.make_feature_df(random_intervals, sequences_df, {}, {}, snp_seq)
        Y = res_annot['MA_SNV_pct']
        if 'gc_content' in seq_type:
            X = random_intervals.apply(lambda row: make_gc_pct_bins(row['chr'], row['start'], row['stop'], sequences_df, 10), axis=1)
            Y = Y[X.apply(lambda x: len(x) == interval_length/10)]
            X = X[X.apply(lambda x: len(x) == interval_length/10)]
        else:
            X = random_intervals.apply(lambda row: res_seq_dic[row['chr']][row['start']:row['stop']], axis=1)
            Y = Y[X.apply(lambda x: len(x) == interval_length)]
            X = X[X.apply(lambda x: len(x) == interval_length)]
        X = np.stack(X)
        if len(X.shape) > 2:
            X = X[:, :, 0]
        print('Size of X: ', X.shape)
        X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.1, random_state=42)
        model = LinearRegression()
        model.fit(X_train, y_train)
        y_pred = model.predict(X_test)
        corr_coef, p_value = pearsonr(y_test, y_pred)
        corr_coef_lst.append(corr_coef)
    print(res_title)
    return corr_coef_lst, res_title

args = Namespace()
res_df = pd.DataFrame({'accession': [], 'scope': [], 'seq_type': [], 'corr':[]})
for accession in ['c24', 'mutation_bias']:
    args.accession = accession
    for scope in ['genome-wide', 'genic']:
        args.random_intervals = scope
        for epi in configs.epi_marks:
            seq_type = 'epi_' + epi
            corr_coef_lst, _ = test_one_seq_tracks(args, seq_type)
            res_df = pd.concat([res_df, pd.DataFrame({'accession': accession, 'scope': scope, 'seq_type': seq_type, 'corr': corr_coef_lst})])
            res_df.to_csv('mark_predictability_results.csv', index=False)
        for context in ['CG', 'CHG', 'CHH']:
            seq_type = 'meth_' + context
            corr_coef_lst, _ = test_one_seq_tracks(args, seq_type)
            res_df = pd.concat([res_df, pd.DataFrame({'accession': accession, 'scope': scope, 'seq_type': seq_type, 'corr': corr_coef_lst})])
            res_df.to_csv('mark_predictability_results.csv', index=False)
        seq_type = 'gc_content'
        corr_coef_lst, _ = test_one_seq_tracks(args, seq_type)
        res_df = pd.concat([res_df, pd.DataFrame({'accession': accession, 'scope': scope, 'seq_type': seq_type, 'corr': corr_coef_lst})])
        res_df.to_csv('mark_predictability_results.csv', index=False)
res_df.to_csv('mark_predictability_results.csv', index=False)

# args = Namespace()
# args.accession = 'c24'
# seq_type = 'epi_' + configs.epi_marks[0]
# args.random_intervals = 'genic'



