from preprocess import data_reader
import pandas as pd
from preprocess import preprocess
import random
from functools import partial
from datetime import datetime
#from pandarallel import pandarallel #uncomment this for the genic region real varibales computations.
import numpy as np
import re
import multiprocessing
from multiprocessing import Manager
import time
import pickle
import os
from preprocess import regression_preprocess as regprep
from preprocess import configs


def average_bw_interval_seq(chro, start, stop, bw_seq):
    if stop <= start:
        return np.nan
    seq_ = bw_seq[chro][start:stop]
    return np.mean(seq_) if len(seq_) > 0 else 0

def features_df_column_maker(features_df, mngr_ns):
    print('process started')
    s_t = time.time()
    seq_dic = mngr_ns.sd
    print('seq_dic processed', time.time() - s_t)
    res = features_df.apply(lambda row: average_bw_interval_seq(row['chr'], row['start'], row['stop'], seq_dic), axis=1)
    print('process ended', time.time() - s_t)
    return res

def parallel_feature_df_maker(annot_df, seq_dic):
    annot_df = annot_df[annot_df.type == 'gene']
    general_intervals = annot_df.apply(lambda row: regprep.get_gene_intervals(row, bin_size=10, flanking_region=2000), axis=1)
    feature_df_lst = []
    count = 0
    mngr = Manager()
    mngr_ns = mngr.Namespace()
    mngr_ns.sd = seq_dic
    for col in list(set(general_intervals.columns) - {'chr', 'length'}):
        intvl_df = pd.DataFrame({'chr': general_intervals['chr'], 'start': general_intervals[str(col)], 'stop': general_intervals[str(col)]+general_intervals['length'], 'length': general_intervals['length']}).dropna()
        intvl_df = intvl_df.astype({'chr': str, 'start': int, 'stop': int, 'length': int})
        feature_df_lst.append(intvl_df)
        if count == 5:
            break
        count += 1
    print('multiprocessing ...')
    pool = multiprocessing.Pool(processes=6)
    ans = pool.map(partial(features_df_column_maker, mngr_ns=mngr_ns), feature_df_lst)
    pool.close()
    pool.join()
    return ans


cnfg = configs.col
cnfg_snp = preprocess.convert_og_to_cnfg('c24')
rc = configs.rc
features_df = data_reader.read_features_data(cnfg['ff_address'])
annot_df = data_reader.read_annot(cnfg['gene_annotation'])
fn = './dump_files/H3K14ac-SRX1469119.bw_annot_seq.pkl'
seq_dic = data_reader.load_dic(fn)
ans = parallel_feature_df_maker(annot_df, seq_dic)
