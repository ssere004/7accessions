from preprocess import regression_preprocess as regrprep
import preprocess.data_reader as data_reader
import preprocess.preprocess as preprocess
import preprocess.configs as configs
import process.process as process
from sklearn.model_selection import train_test_split
from tensorflow.keras.layers import Activation,Dense
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Dropout, Flatten, Reshape
from tensorflow.keras.constraints import max_norm
from tensorflow.keras.losses import binary_crossentropy
import numpy as np
import tensorflow as tf
import pandas as pd
from sklearn.metrics import accuracy_score



sample_size = 20000
freq_threshold = 0.9
window_size = 128
tr_te_pct = 0.2
tr_val_pct = 0.1
cnfg = configs.col
rc = configs.rc
sequences_df = preprocess.convert_assembely_to_onehot(cnfg['og'], data_reader.readfasta(cnfg['assembly']), from_file=True)
meth_seq_dic = preprocess.get_meth_seq_dic(cnfg, rc, from_file=True)
meth_seq_dic = {contxt: {chro: np.expand_dims(meth_seq_dic[contxt][chro], axis=1) for chro in meth_seq_dic[contxt].keys()} for contxt in meth_seq_dic.keys()}
bw_dic = regrprep.get_bw_feature_seq(cnfg, from_file=True)

input_list = [sequences_df] + list(bw_dic.values()) + list(meth_seq_dic.values())

high_af, low_af = preprocess.read_raw_snps('g1001_high'), preprocess.read_raw_snps('g1001_low')
sample_p = high_af[high_af.REF_freq > freq_threshold][['chr', 'position']].sample(n=sample_size)
sample_n = low_af[low_af.REF_freq < (1-freq_threshold)][['chr', 'position']].sample(n=sample_size)

X, Y = process.input_maker(input_list, sample_n, sample_p, window_size=window_size)
Y = np.asarray(pd.cut(Y, bins=2, labels=[0, 1], right=False))
b = np.zeros((Y.size, Y.max()+1))
b[np.arange(Y.size), Y] = 1
Y = b
x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=tr_te_pct, random_state=None)
x_train, x_val, y_train, y_val = train_test_split(x_train, y_train, test_size=tr_val_pct, random_state=None)

with tf.device('/device:GPU:0'):
    W_maxnorm = 3
    model = Sequential()
    model.add(Conv2D(128, kernel_size=(10, 16), activation='relu', input_shape=(X.shape[1], X.shape[2], 1), padding='same', kernel_constraint=max_norm(W_maxnorm)))
    model.add(MaxPooling2D(pool_size=(4, 4), strides=(1, 3)))
    model.add(Conv2D(256, kernel_size=(18, 8), activation='relu', padding='same', kernel_constraint=max_norm(W_maxnorm)))
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(1, 3)))
    model.add(Flatten())
    model.add(Dense(64, activation='relu'))
    model.add(Dropout(0.6))
    model.add(Dense(2))
    model.add(Activation('softmax'))
    myoptimizer = tf.keras.optimizers.Adam(learning_rate=1e-5)
    model.compile(loss=binary_crossentropy, optimizer=myoptimizer, metrics=['accuracy'])

model.fit(x_train, y_train, epochs=20, validation_data=(x_val, y_val), batch_size=64)
y_pred = model.predict(x_test)
print(accuracy_score(y_test, y_pred.round()))
