import tensorflow as tf
import preprocess.data_reader as data_reader
import preprocess.preprocess as preprocess
import preprocess.configs as configs
import process.process as process
import numpy as np
import argparse
import matplotlib.pyplot as plt


parser = argparse.ArgumentParser()

parser.add_argument('-acr', '--ref_accession_name', help='name of the accession', required=True)
parser.add_argument('-acs', '--snp_accession_name', help='name of accession with snps', required=True)
parser.add_argument('-ws', '--window_size', help='sequence window size', required=False, default=40)

args = parser.parse_args()

cnfg_ref = process.get_cnfg_by_og(args.ref_accession_name)
cnfg_snp = process.get_cnfg_by_og(args.snp_accession_name)
rc = configs.rc

window_size = int(args.window_size)



# cnfg_ref = configs.col
# cnfg_snp = configs.c24
# rc = configs.rc
# window_size = 10

methylations = data_reader.read_methylations(cnfg_ref['methylation'], '', coverage_threshold=rc['ct'])
assemebly = data_reader.readfasta(cnfg_ref['assembly'])
syri = data_reader.read_syri(cnfg_snp['syri'], annot_type='SNP')

sequences_df = preprocess.convert_assembely_to_onehot(cnfg_ref['og'], assemebly, from_file=True)
meth_seq = preprocess.make_methseq_dic(cnfg_ref['og'], methylations, assemebly, rc['ct'], from_file=True)
#annot_seq = preprocess.make_annotseq_dic(cnfg_ref['og'], data_reader.read_annot(cnfg_ref['gene_annotation']), cnfg_ref['annot_types'], assemebly)
annot_seq = preprocess.make_annotseq_dic( data_reader.read_annot(cnfg_ref['gene_annotation']), ['gene'], assemebly)

# repeat_annot = data_reader.read_annot(cnfg_ref['repeat_annotation'])
# repeat_annot['type'] = 'repeat'
# rep_seq = preprocess.make_annotseq_dic(cnfg_ref['og'], repeat_annot, ['repeat'], assemebly, strand_spec=False)

bp_pairs = [['A', 'C'], ['A', 'G'], ['A', 'T'],
       ['C', 'G'], ['C', 'T'],
       ['G', 'T']
       ]

fig, axs = plt.subplots(2, 3, figsize=(15, 10))
fig.tight_layout(h_pad=3)
i_p, j_p = 0, 0
for [control_bp, test_bp] in bp_pairs:
    com_control_bp, com_test_bp = preprocess.complement_mutation(control_bp, test_bp)
    syri_df = syri[((syri.seq1 == control_bp) & (syri.seq2 == test_bp)) | ((syri.seq1 == com_control_bp) & (syri.seq2 == com_test_bp))][['chr1', 'start1']]

    syri_df['start1'] = syri_df['start1'].astype(int) - 1

    #make a dictionary of each chromosome mutated basepair loci
    targets_p = process.convert_df_to_dic(syri_df)

    sample_size = len(syri_df)
    #sample_size = 1000

    #make a dictionary of positions for all control and compeliment of control basepair.
    spec_bps_1 = process.get_all_bps(sequences_df, control_bp)
    spec_bps_2 = process.get_all_bps(sequences_df, com_control_bp)
    spec_bps = process.combine_specs(spec_bps_1, spec_bps_2)
    #Subtract mutated positions dictiornary from all positions dictionary to get unmutated samples.
    targets_n = process.subtract_bps(spec_bps, targets_p)
    print('len targets_p and targets_n before filtering %d %d' %(len(targets_n['chr1']), len(targets_p['chr1'])))
    targets_p = process.annot_base_filter(targets_p, annot_seq, positive=True)
    targets_n = process.annot_base_filter(targets_n, annot_seq, positive=True)
    print('len targets_p and targets_n after filtering %d %d' %(len(targets_n['chr1']), len(targets_p['chr1'])))

    #making two DataFrames for positive and negative samples which have two columns['chr', 'position']
    sample_n = process.sample_from_chr_dic(targets_n, sample_size)
    sample_p = process.sample_from_chr_dic(targets_p, sample_size)

    #sample_n.to_csv('./dump_files/sample_n.csv', sample_n, sep='\t')
    #sample_n.to_csv('./dump_files/sample_p.csv', sample_p, sep='\t')


    X, Y = process.input_maker([meth_seq], sample_n, sample_p, window_size=window_size)
    X = X[:, :window_size, :, :]
    mutated_avgs = np.zeros(window_size)
    unmutated_avgs = np.zeros(window_size)
    for i in range(window_size):
        if len(X[np.nonzero(X[Y == 1, i, :, :])]) > 0: mutated_avgs[i] = np.mean(X[np.nonzero(X[Y == 1, i, :, :])])
        if len(X[np.nonzero(X[Y == 0, i, :, :])]) > 0: unmutated_avgs[i] = np.mean(X[np.nonzero(X[Y == 0, i, :, :])])

    if control_bp != "all" or test_bp != "all":
        conversion = control_bp + ':' + com_control_bp+ '->' + test_bp + ':' + com_test_bp
    else:
        conversion = 'all->all'

    ax_x = [i for i in range(len(mutated_avgs))]
    line1, = axs[i_p, j_p].plot(ax_x, mutated_avgs, label='mutated')
    line2, = axs[i_p, j_p].plot(ax_x, unmutated_avgs, label='unmutated')
    axs[i_p, j_p].set_title(cnfg_snp['og'] + ' ' + conversion)
    axs[i_p, j_p].set_ylim([0, 0.07])
    axs[i_p, j_p].set_xticks(ax_x, minor=True)
    axs[i_p, j_p].grid(which='minor')
    j_p += 1
    if j_p > 2:
        j_p = 0
        i_p += 1

lines = [line1, line2]
labels = ['mutated', 'unmutated']
fig.legend(lines, labels, loc='center right', bbox_to_anchor=[1, 0.5])

plt.savefig('./plots/window_meth_avg_ws:%s.png' %(cnfg_snp['og']))









