import tensorflow as tf
import preprocess.data_reader as data_reader
import preprocess.preprocess as preprocess
import preprocess.configs as configs
import process.process as process
from tensorflow import keras
from sklearn.model_selection import train_test_split
from tensorflow.keras.layers import Activation,Dense
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Dropout, Flatten, Reshape, Bidirectional,LSTM
from sklearn.metrics import accuracy_score
from tensorflow.keras.constraints import max_norm
from tensorflow.keras.losses import binary_crossentropy
import numpy as np
import argparse
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
import lstm_att as lstm_att

parser = argparse.ArgumentParser()

parser.add_argument('-acr', '--ref_accession_name', help='name of the accession', required=True)
parser.add_argument('-acs', '--snp_accession_name', help='name of accession with snps', required=True)
parser.add_argument('-cbp', '--control_basepair', help='base pair in the reference accession/ or \"all\"', required=True)
parser.add_argument('-tbp', '--test_basepair', help='base pair in the snp accession/ or \"all\"', required=True)
parser.add_argument('-ws', '--window_size', help='sequence window size', required=False, default=1600)
parser.add_argument('-m', '--model_type', help='cpgenie/rf/att/tmp', required=False, default='rf')

#CUDA_VISIBLE_DEVICES=1 python snp_prediction_seqbase.py -acr col -acs c24 -cbp C -tbp T

args = parser.parse_args()

cnfg_ref = process.get_cnfg_by_og(args.ref_accession_name)
cnfg_snp = process.get_cnfg_by_og(args.snp_accession_name)
rc = configs.rc

control_bp = args.control_basepair
test_bp = args.test_basepair

mt = args.model_type

# cnfg_ref = configs.col
# cnfg_snp = configs.c24
# rc = configs.rc
#
# control_bp = 'A'
# test_bp = 'C'

window_size = int(args.window_size)
tr_te_pct = 0.2
tr_val_pct = 0.1

methylations = data_reader.read_methylations(cnfg_ref['methylation'], '', coverage_threshold=rc['ct'])
assemebly = data_reader.readfasta(cnfg_ref['assembly'])
syri = data_reader.read_syri(cnfg_snp['syri'], annot_type='SNP')

sequences_df = preprocess.convert_assembely_to_onehot(cnfg_ref['og'], assemebly, from_file=True)
meth_seq = preprocess.make_methseq_dic(cnfg_ref['og'], methylations, assemebly, rc['ct'], from_file=False)
#annot_seq = preprocess.make_annotseq_dic(cnfg_ref['og'], data_reader.read_annot(cnfg_ref['gene_annotation']), cnfg_ref['annot_types'], assemebly)

if test_bp == 'all' or control_bp == 'all':
    syri_df = syri
else:
    syri_df = syri[(syri.seq1 == control_bp) & (syri.seq2 == test_bp)][['chr1', 'start1']]

syri_df['start1'] = syri_df['start1'].astype(int) - 1
targets_p = process.convert_df_to_dic(syri_df)
sample_size = sum(len(lst) for lst in targets_p.values())
#sample_size = int(cnfg_snp['min_snp_size'])

spec_bps = process.get_all_bps(sequences_df, control_bp)
targets_n = process.subtract_bps(spec_bps, targets_p)

sample_n = process.sample_from_chr_dic(targets_n, sample_size)
sample_p = process.sample_from_chr_dic(targets_p, sample_size)

#sample_n.to_csv('./dump_files/sample_n.csv', sample_n, sep='\t')
#sample_n.to_csv('./dump_files/sample_p.csv', sample_p, sep='\t')

X, Y = process.input_maker([sequences_df, meth_seq], sample_n, sample_p, window_size=window_size)

if mt == 'cpgenie':
    X = np.swapaxes(X, 1, 2)
    Y = np.asarray(pd.cut(Y, bins=2, labels=[0, 1], right=False))
    b = np.zeros((Y.size, Y.max()+1))
    b[np.arange(Y.size), Y] = 1
    Y = b
    with tf.device('/device:GPU:0'):
        W_maxnorm = 3
        model = Sequential()
        model.add(Conv2D(128, kernel_size=(1, 5), activation='relu', input_shape=(X.shape[1], X.shape[2], 1), padding='same', kernel_constraint=max_norm(W_maxnorm)))
        model.add(MaxPooling2D(pool_size=(1, 5), strides=(1, 3)))
        model.add(Conv2D(256, kernel_size=(1, 5), activation='relu', padding='same', kernel_constraint=max_norm(W_maxnorm)))
        model.add(MaxPooling2D(pool_size=(1, 5), strides=(1, 3)))
        model.add(Conv2D(512, kernel_size=(1, 5), activation='relu', padding='same', kernel_constraint=max_norm(W_maxnorm)))
        model.add(MaxPooling2D(pool_size=(1, 5), strides=(1, 3)))
        model.add(Flatten())
        model.add(Dense(64, activation='relu'))
        model.add(Dropout(0.5))
        model.add(Dense(64, activation='relu'))
        model.add(Dropout(0.5))
        model.add(Dense(2))
        model.add(Activation('softmax'))
        myoptimizer = keras.optimizers.RMSprop(lr=0.001, rho=0.9, epsilon=1e-06)
        model.compile(loss=binary_crossentropy, optimizer=myoptimizer, metrics=['accuracy'])
    x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=tr_te_pct, random_state=None)
    x_train, x_val, y_train, y_val = train_test_split(x_train, y_train, test_size=tr_val_pct, random_state=None)
    print('model fitting started for %s and %s    %s --> %s' % (cnfg_ref['og'], cnfg_snp['og'], control_bp, test_bp))
    model.fit(x_train,  y_train, batch_size=32, epochs=15, verbose=1, validation_data=(x_val, y_val))
    y_pred = model.predict(x_test)
if mt =='rf':
    x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=tr_te_pct, random_state=None)
    x_train, x_val, y_train, y_val = train_test_split(x_train, y_train, test_size=tr_val_pct, random_state=None)
    with tf.device('/device:GPU:0'):
        clf = RandomForestClassifier(random_state=0, n_estimators=50, warm_start=True, n_jobs=-1)
        for chunk in range(0, len(x_train), 10000):
            if chunk+10000 < len(x_train):
                xx = x_train[chunk: chunk+10000]
                nsamples, nx, ny, nz = xx.shape
                xx = xx.reshape((nsamples, nx*ny))
                yy = y_train[chunk: chunk+10000]
                clf.fit(xx, yy)
                clf.n_estimators += 100
        nsamples, nx, ny, nz = x_test.shape
        x_test = x_test.reshape((nsamples, nx*ny))
        y_pred=clf.predict(x_test)
if mt =='att':
    X = lstm_att.convert_X_to_number(X)
    with tf.device('/device:GPU:0'):
        model = lstm_att.define_model(X.shape[1], X.max() + 1)
        x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=tr_te_pct, random_state=None)
        x_train, x_val, y_train, y_val = train_test_split(x_train, y_train, test_size=tr_val_pct, random_state=None)
        print('model fitting started for %s and %s    %s --> %s' % (cnfg_ref['og'], cnfg_snp['og'], control_bp, test_bp))
        model.fit(x_train,  y_train, batch_size=32, epochs=15, verbose=1, validation_data=(x_val, y_val))
        y_pred = model.predict(x_test)
if mt == 'tmp':
    X = np.swapaxes(X, 1, 2)
    Y = np.asarray(pd.cut(Y, bins=2, labels=[0, 1], right=False))
    b = np.zeros((Y.size, Y.max()+1))
    b[np.arange(Y.size), Y] = 1
    Y = b
    with tf.device('/device:GPU:0'):
        W_maxnorm = 3
        model = Sequential()
        model.add(Conv2D(128, kernel_size=(1, 5), activation='relu', input_shape=(X.shape[1], X.shape[2], 1), padding='same', kernel_constraint=max_norm(W_maxnorm)))
        model.add(MaxPooling2D(pool_size=(1, 5), strides=(1, 3)))
        model.add(Conv2D(256, kernel_size=(1, 5), activation='relu', padding='same', kernel_constraint=max_norm(W_maxnorm)))
        model.add(MaxPooling2D(pool_size=(1, 5), strides=(1, 3)))
        model.add(Flatten())
        model.add(Dense(64, activation='relu'))
        model.add(Dropout(0.6))
        model.add(Dense(2))
        model.add(Activation('softmax'))
        myoptimizer = tf.keras.optimizers.Adam(learning_rate=1e-4)
        model.compile(loss=binary_crossentropy, optimizer=myoptimizer, metrics=['accuracy'])

    x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=tr_te_pct, random_state=None)
    x_train, x_val, y_train, y_val = train_test_split(x_train, y_train, test_size=tr_val_pct, random_state=None)
    print('model fitting started for %s and %s    %s --> %s' % (cnfg_ref['og'], cnfg_snp['og'], control_bp, test_bp))
    model.fit(x_train,  y_train, batch_size=32, epochs=15, verbose=1, validation_data=(x_val, y_val))
    y_pred = model.predict(x_test)
if mt == 'tmp2':
    X = np.swapaxes(X, 1, 2)
    Y = np.asarray(pd.cut(Y, bins=2, labels=[0, 1], right=False))
    b = np.zeros((Y.size, Y.max()+1))
    b[np.arange(Y.size), Y] = 1
    Y = b
    with tf.device('/device:GPU:0'):
        W_maxnorm = 3
        model = Sequential()
        model.add(Conv2D(128, kernel_size=(1, 5), activation='relu', input_shape=(X.shape[1], X.shape[2], 1), padding='same', kernel_constraint=max_norm(W_maxnorm)))
        model.add(MaxPooling2D(pool_size=(1, 5), strides=(1, 3)))
        model.add(Conv2D(256, kernel_size=(1, 5), activation='relu', padding='same', kernel_constraint=max_norm(W_maxnorm)))
        model.add(MaxPooling2D(pool_size=(1, 5), strides=(1, 3)))
        model.add(Reshape((model.output.shape[1] * model.output.shape[2], model.output.shape[3])))
        model.add(Bidirectional(LSTM(32, return_sequences=True)))
        model.add(Flatten())
        model.add(Dense(64, activation='relu'))
        model.add(Dropout(0.6))
        model.add(Dense(2))
        model.add(Activation('softmax'))
        myoptimizer = tf.keras.optimizers.Adam(learning_rate=1e-4)
        model.compile(loss=binary_crossentropy, optimizer=myoptimizer, metrics=['accuracy'])

    x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=tr_te_pct, random_state=None)
    x_train, x_val, y_train, y_val = train_test_split(x_train, y_train, test_size=tr_val_pct, random_state=None)
    print('model fitting started for %s and %s    %s --> %s' % (cnfg_ref['og'], cnfg_snp['og'], control_bp, test_bp))
    model.fit(x_train,  y_train, batch_size=32, epochs=15, verbose=1, validation_data=(x_val, y_val))
    y_pred = model.predict(x_test)

with open("snp_prediction_results_modelselection.txt", "a") as file_object:
    file_object.write('%s\t%s\t%s\t%s\t%s\t%s\t%s' %(cnfg_ref['og'], cnfg_snp['og'], control_bp, test_bp, mt, accuracy_score(y_test, y_pred.round()), str(len(x_train))))
    file_object.write("\n")

print(cnfg_ref['og'], cnfg_snp['og'], control_bp, test_bp, mt,  accuracy_score(y_test, y_pred.round()), len(x_train))

