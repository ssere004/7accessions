import pandas as pd
import argparse

parser = argparse.ArgumentParser()

parser.add_argument('-sy', '--syri_file', help='syri file address', required=True)

args = parser.parse_args()

sy_df = pd.read_csv('an1.syri.out', header=None, sep='\t')
sy_df.columns = ['chr1', 'start1', 'end1', 'seq1', 'seq2', 'chr2', 'start2', 'end2', 'ID', 'parent', 'annot', 'copy_stat']

