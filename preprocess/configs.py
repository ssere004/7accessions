import os

if os.path.isdir("/home/csgrads/ssere004/"):
       base = '/home/csgrads/ssere004/'
       root = base + 'Organisms/7accessions/'
else:
       base = '/home/ssere004/'
       root = base + 'Organisms/7accessions/'

epi_marks = ['H3K14ac', 'H3K23ac', 'H3K27ac', 'H3K27me1', 'H3K36ac',
             'H3K36me3', 'H3K4me1', 'H3K4me2', 'H3K4me3', 'H3K56ac',
             'H3K9ac', 'H3K9me1', 'H3K9me2', 'ATAC']

col = {'og': 'col',
       'assembly': root + 'col-0/col-0_as.fa',
       'methylation': root + 'col-0/col-0_.deduplicated.CX_report.txt',
       'gene_annotation': root + 'col-0/TAIR10_GFF3_genes.gff',
       'annot_types': ['gene', 'CDS', 'exon'],
       'repeat_annotation': root + 'col-0/col-0_repeat.gff',
       'min_snp_size': '',
       'features': root + 'col-0/features_data.csv',
       'methylation_tsv': root + 'col-0/epi_data/GSM1085222_mC_calls_Col_0.tsv',
       'bw_root': root + 'col-0/epi_data/grouped/',
       'ff_address': root + 'col-0/epi_data/features_data.csv'
       }

mutation_bias = {'og': 'mutation_bias',}

g1001_high = {'og': 'g1001_high',
              'snv_file': root + '1001/high_freq_SNVs.csv'}

g1001_high05 = {'og': 'g1001_high05',
              'snv_file': root + '1001/high_freq_SNVs_0.05.csv'}

g1001_high1 = {'og': 'g1001_high1',
              'snv_file': root + '1001/high_freq_SNVs_0.1.csv'}

g1001_low = {'og': 'g1001_low',
             'snv_file': root + '1001/low_freq_SNVs.csv'}

g1001_low05 = {'og': 'g1001_low05',
             'snv_file': root + '1001/low_freq_SNVs_0.05.csv'}

g1001_low01 = {'og': 'g1001_low01',
             'snv_file': root + '1001/low_freq_SNVs_0.01.csv'}


c24 = {'og': 'c24',
       'assembly': root + 'c24/c24_as.fa',
       'methylation': root + 'c24/c24_.deduplicated.CX_report.txt',
       'syri': root + 'c24/c24.syri.out',
       'gene_annotation': root + 'c24/C24.protein-coding.genes.v2.5.2019-10-09.gff3',
       'annot_types': ['gene', 'CDS', 'exon'],
       'min_snp_size': '30258'}

cvi = {'og': 'cvi',
       'assembly': root + 'cvi-0/cvi-0_as.fa',
       'methylation': root + 'cvi-0/cvi-0_.deduplicated.CX_report.txt',
       'syri': root + 'cvi-0/cvi.syri.out',
       'gene_annotation': root + 'cvi-0/Cvi.protein-coding.genes.v2.5.2019-10-09.gff3',
       'annot_types': ['gene', 'CDS', 'exon'],
       'min_snp_size': '36592'}

ler = {'og': 'ler',
       'assembly': root + 'ler-0/ler-0_as.fa',
       'methylation': root + 'ler-0/ler-0_.deduplicated.CX_report.txt',
       'syri': root + 'ler-0/ler.syri.out',
       'gene_annotation': root + 'ler-0/Ler.protein-coding.genes.v2.5.2019-10-09.gff3',
       'annot_types': ['gene', 'CDS', 'exon'],
       'min_snp_size': '29369'}

sha = {'og': 'sha',
       'assembly': root + 'sha/sha_as.fa',
       'methylation': root + 'sha/sha_.deduplicated.CX_report.txt',
       'syri': root + 'sha/sha.syri.out',
       'gene_annotation': root + 'sha/Sha.protein-coding.genes.v2.5.2019-10-09.gff3',
       'annot_types': ['gene', 'CDS', 'exon'],
       'min_snp_size': '30886'}

rc = {'ct': 3}

