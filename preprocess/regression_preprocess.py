from preprocess import data_reader
import pandas as pd
from preprocess import preprocess
import random
from functools import partial
from datetime import datetime
from pandarallel import pandarallel #uncomment this for the genic region real varibales computations.
import numpy as np
import re
import multiprocessing
from multiprocessing import Manager
import time
import pickle
import os


#for the regions outside the chromosme range it returns Zero
def average_bw_interval_seq(chro, start, stop, bw_seq):
    start, stop = int(start), int(stop)
    if stop <= start:
        return np.nan
    seq_ = bw_seq[chro][start:stop]
    return np.mean(seq_) if len(seq_) > 0 else 0

def gc_content_interval_seq(chro, start, stop, sequences_df):
    start, stop = int(start), int(stop)
    if stop <= start:
        return np.nan
    return (np.sum(sequences_df[chro][start:stop, 1]) + np.sum(sequences_df[chro][start:stop, 2]))/(stop-start)

def methylation_interval_seq(chro, start, stop, meth_seq, threshold=0.5):
    start, stop = int(start), int(stop)
    if stop <= start:
        return np.nan
    return np.sum(np.where(meth_seq[chro][start:stop] > threshold, 1, 0)) / (stop - start)

def count_snps_interval_seq(chro, start, stop, snp_seq, sequences_df, snp_cntx):
    start, stop = int(start), int(stop)
    if stop <= start:
        return np.nan
    if snp_cntx != None:
        ohdic = {'A': 0, 'C': 1, 'G': 2, 'T': 3}
        cntx_count = (np.sum(sequences_df[chro][start:stop, ohdic[snp_cntx[0]]]) + np.sum(sequences_df[chro][start:stop, ohdic[snp_cntx[1]]]))
        if cntx_count == 0: return 0
        else : return np.sum(snp_seq[chro][start:stop])/ cntx_count
    else:
        return np.sum(snp_seq[chro][start:stop])/(stop - start)


def features_df_column_maker(features_df, seq_dic, column_name):
    if column_name == 'GC_content_pct':
        res = (column_name, features_df.apply(lambda row: gc_content_interval_seq(row['chr'], row['start'], row['stop'], seq_dic), axis=1))
    elif 'bw' in column_name:
        colname = re.split('_|-', column_name)[0]
        if colname == 'ATAC': colname = 'atac_pct'
        res = (colname, features_df.apply(lambda row: average_bw_interval_seq(row['chr'], row['start'], row['stop'], seq_dic), axis=1))
    elif column_name in ['CG_pct', 'CHG_pct', 'CHH_pct']:
        res = (column_name, features_df.apply(lambda row: methylation_interval_seq(row['chr'], row['start'], row['stop'], seq_dic), axis=1))
    return res

def parallel_make_features_df(features_df, sequences_df, meth_seq_dic, bw_dic, snp_seq, num_cores=8, snp_cntx=None):
    start_t = time.time()
    features_df = features_df[['chr', 'start', 'stop']].copy()
    inputs = []
    inputs.append((features_df, sequences_df, 'GC_content_pct',))
    for epi_mark in bw_dic.keys():
        inputs.append((features_df, bw_dic[epi_mark], epi_mark,))
    for colname, c_cntx in [('CG_pct', 'CG'), ('CHG_pct', 'CHG'), ('CHH_pct', 'CHH')]:
        inputs.append((features_df, meth_seq_dic[c_cntx], colname,))
    pool = multiprocessing.Pool(processes=num_cores)
    results = pool.starmap(features_df_column_maker, inputs)
    pool.close()
    pool.join()
    for colinfo in results:
        if colinfo != None:
            features_df[colinfo[0]] = colinfo[1]
    features_df['MA_SNV_pct'] = features_df.apply(lambda row: count_snps_interval_seq(row['chr'], row['start'], row['stop'], snp_seq, sequences_df, snp_cntx), axis=1)
    print("parallel computing ended after:", time.time() - start_t, "seconds")
    return features_df

# Gets a feature_df(which must have 'chr', 'start', 'stop' and 'length').
# For this regions based on our own bw files it calculates the features as well as snp rate
def make_feature_df(features_df, sequences_df, meth_seq_dic, bw_dic, snp_seq, specific_feature=None, snp_cntx=None):
    #print('make feature df started for one task')
    res_df = pd.DataFrame({'chr': features_df['chr'], 'start': features_df['start'], 'stop': features_df['stop'], 'length': features_df['length']})
    for epi_mark in bw_dic.keys():
        colname = re.split('_|-', epi_mark)[0]
        if colname == 'ATAC':
            colname = 'atac_pct'
        if specific_feature == None or specific_feature in colname:
            res_df[colname] = res_df.apply(lambda row: average_bw_interval_seq(row['chr'], row['start'], row['stop'], bw_dic[epi_mark]), axis=1)
    if specific_feature == None or specific_feature in 'GC_content_pct':
        res_df['GC_content_pct'] = res_df.apply(lambda row: gc_content_interval_seq(row['chr'], row['start'], row['stop'], sequences_df), axis=1)
    if len(meth_seq_dic) > 0:
        if specific_feature == None or specific_feature in 'CG_pct':
            res_df['CG_pct'] = res_df.apply(lambda row: methylation_interval_seq(row['chr'], row['start'], row['stop'], meth_seq_dic['CG']), axis=1)
        if specific_feature == None or specific_feature in 'CHG_pct':
            res_df['CHG_pct'] = res_df.apply(lambda row: methylation_interval_seq(row['chr'], row['start'], row['stop'], meth_seq_dic['CHG']), axis=1)
        if specific_feature == None or specific_feature in 'CHH_pct':
            res_df['CHH_pct'] = res_df.apply(lambda row: methylation_interval_seq(row['chr'], row['start'], row['stop'], meth_seq_dic['CHH']), axis=1)
    if specific_feature == None or specific_feature in 'MA_SNV_pct':
        if snp_seq !=None:
            res_df['MA_SNV_pct'] = res_df.apply(lambda row: count_snps_interval_seq(row['chr'], row['start'], row['stop'], snp_seq, sequences_df, snp_cntx), axis=1)
        else:
            res_df['MA_SNV_pct'] = np.nan
    return res_df


# makes a feature df (which must have 'chr', 'start', 'stop' and 'length') for random intervals.
def make_random_intervals_df(sequeces_df, total_sample=20000, bin_size=20):
    chro_to_num_dic = {'chr1': 1, 'chr2': 2, 'chr3': 3, 'chr4': 4, 'chr5': 5, 'chrm': 6, 'chrc': 7}
    gs = sum(len(arr) for arr in sequeces_df.values())
    starts = np.zeros((total_sample),dtype=np.int32)
    ends = np.zeros((total_sample),dtype=np.int32)
    chrs = np.zeros((total_sample),dtype=np.int32)
    last = 0
    for chro in sequeces_df.keys():
        chro_sample = int(float(len(sequeces_df[chro])/gs) * total_sample)
        sample = np.asarray(random.sample(range(len(sequeces_df[chro]) + 1), chro_sample))
        starts[last: min(last+chro_sample, len(starts))] = sample - int(bin_size/2)
        ends[last: min(last+chro_sample, len(ends))] = sample + int(bin_size/2)
        chrs[last: min(last+chro_sample, len(chrs))] = chro_to_num_dic[chro]
        last += chro_sample
    res = pd.DataFrame({'start': starts, 'stop': ends, 'chr': chrs, 'length': bin_size})
    res['chr'] = res['chr'].replace({1: 'chr1', 2: 'chr2', 3: 'chr3', 4: 'chr4', 5: 'chr5', 6: 'chrm', 7: 'chrc'})
    return res[res.chr != 0]

# makes a feature df (which must have 'chr', 'start', 'stop' and 'length') for a generated random dataset which are in the input_interval_df with a probability of respected to the interval lengths
def make_random_intervals_genic_regions_df(input_intervals_df, interval_length=200, total_sample=20000):
    Ls = input_intervals_df['end'] - input_intervals_df['start'] - interval_length
    selected_genes = random.choices(range(len(input_intervals_df)), weights=Ls, k=total_sample)
    chro_to_num_dic = {'chr1': 1, 'chr2': 2, 'chr3': 3, 'chr4': 4, 'chr5': 5, 'chrm': 6, 'chrc': 7}
    starts = np.zeros((total_sample),dtype=np.int32)
    ends = np.zeros((total_sample),dtype=np.int32)
    chrs = np.zeros((total_sample),dtype=np.int32)
    for idx ,gene_index in enumerate(selected_genes):
        start = random.randint(input_intervals_df.iloc[gene_index]['start'], input_intervals_df.iloc[gene_index]['end'] - interval_length)
        starts[idx] = start
        ends[idx] = start + interval_length
        chrs[idx] = chro_to_num_dic[input_intervals_df.iloc[gene_index]['chr']]
    res = pd.DataFrame({'start': starts, 'stop': ends, 'chr': chrs, 'length': interval_length})
    res['chr'] = res['chr'].replace({1: 'chr1', 2: 'chr2', 3: 'chr3', 4: 'chr4', 5: 'chr5', 6: 'chrm', 7: 'chrc'})
    return res[(res.chr != 0) & (res.start > 0) & (res.stop > 0)]

# For a gene it makes the bins. output is is a Series containing tagged bins and length and chr.
def get_gene_intervals(row, bin_size=10, flanking_region=2000):
    fr = flanking_region #flanking region
    chro = row['chr']
    start = row['start']
    end = row['end']
    strand = row['strand']
    res_dic = {}
    if strand == '+':
        for i, strt in enumerate(range(start - fr, start, bin_size)):
            res_dic['ds-'+str(i)] = strt
        for i, strt in enumerate(range(start, start + min(fr, end-start), bin_size)):
            res_dic['gbl-'+str(i)] = strt
        for i, strt in enumerate(range(end - min(fr, end-start), end, bin_size)):
            res_dic['gbr-'+str(i)] = strt
        for i, strt in enumerate(range(end, end+fr, bin_size)):
            res_dic['us-'+str(i)] = strt
        res = pd.Series(res_dic)
    else:
        for i, strt in enumerate(range(end + fr, end, -bin_size)):
            res_dic['ds-'+str(i)] = strt
        for i, strt in enumerate(range(end, max(end-fr, start), -bin_size)):
            res_dic['gbl-'+str(i)] = strt
        for i, strt in enumerate(range(min(start+fr, end), start, -bin_size)):
            res_dic['gbr-'+str(i)] = strt
        for i, strt in enumerate(range(start, start-fr, -bin_size)):
            res_dic['us-'+str(i)] = strt
        res = pd.Series(res_dic)
    res['length'] = bin_size
    res['chr'] = chro
    return res

def save_genral_intervals_df(annot_df, bin_size, flanking_region):
    annot_df = annot_df[annot_df.type == 'gene']
    general_intervals = annot_df.apply(lambda row: get_gene_intervals(row, bin_size=bin_size, flanking_region=flanking_region), axis=1)
    for col in list(set(general_intervals.columns) - {'chr', 'length'}):
        intvl_df = pd.DataFrame({'chr': general_intervals['chr'], 'start': general_intervals[str(col)], 'stop': general_intervals[str(col)]+general_intervals['length'], 'length': general_intervals['length']}).dropna()
        intvl_df.to_csv('./dump_files/general_intervals/'+col+'.csv', index=False, header=True)


def features_df_column_maker_general_interval(seq_dic, column_name, reg_name):
    features_df = pd.read_csv('./dump_files/general_intervals/'+reg_name+'.csv')
    if column_name == 'GC_content_pct':
        res = (column_name, reg_name, features_df.apply(lambda row: gc_content_interval_seq(row['chr'], row['start'], row['stop'], seq_dic), axis=1))
    elif 'bw' in column_name:
        colname = re.split('_|-', column_name)[0]
        if colname == 'ATAC': colname = 'atac_pct'
        res = (colname, reg_name, features_df.apply(lambda row: average_bw_interval_seq(row['chr'], row['start'], row['stop'], seq_dic), axis=1))
    elif column_name in ['CG_pct', 'CHG_pct', 'CHH_pct']:
        res = (column_name, reg_name, features_df.apply(lambda row: methylation_interval_seq(row['chr'], row['start'], row['stop'], seq_dic), axis=1))
    return res


def save_genic_region_feature_avgs(annot_df, sequences_df, meth_seq_dic, bw_dic, bin_size=10, flanking_region=2000):
    annot_df = annot_df[annot_df.type == 'gene']
    gi_address = './dump_files/general_intervals_b%d_fr%d.csv' %(bin_size, flanking_region)
    if os.path.exists(gi_address):
        general_intervals = pd.read_csv(gi_address)
    else:
        pandarallel.initialize(progress_bar=True)
        general_intervals = annot_df.parallel_apply(lambda row: get_gene_intervals(row, bin_size=bin_size, flanking_region=flanking_region), axis=1)
    reg_names = [rg+'-'+str(i) for i in range(int(flanking_region/bin_size)) for rg in ['ds', 'gbl', 'gbr', 'us']]
    #number of columns for general_intervals is (flanking_region/bin_size) * 4 --------- 4 is [us, gbl, gbr, ds]------> 800 columns for the numbers defined in the function definition line
    np.seterr(invalid='ignore')
    for col in reg_names:
        if os.path.exists('./dump_files/general_intervals_averages_b%d_fr%d/' %(bin_size, flanking_region) +col+'.csv'):
            continue
        start_t = time.time()
        intvl_df = pd.DataFrame({'chr': general_intervals['chr'], 'start': general_intervals[str(col)], 'stop': general_intervals[str(col)]+general_intervals['length'], 'length': general_intervals['length']}).dropna()
        intvl_df = intvl_df.astype({'chr': str, 'start': int, 'stop': int, 'length': int})
        fdf = make_feature_df(intvl_df, sequences_df, meth_seq_dic, bw_dic, None)
        os.makedirs('./dump_files/general_intervals_averages_b%d_fr%d/' % (bin_size, flanking_region), exist_ok=True)
        fdf.to_csv('./dump_files/general_intervals_averages_b%d_fr%d/' %(bin_size, flanking_region) +col+'.csv')
        print('region ' + col + ' Done!!!', time.time() - start_t)

#Make general intervals dataframe which is a dataframe that has ds-i , gbl-i gbr-i and us-i for each gene.
#then for each region, it makes a feature dataframe. each row is for a gene, and columns are average of each features for that specific region(for example gbl-0)
#then averages the prediction of model over the specified averaged regions.
def get_snp_reg_prediction(model, bin_size=10, flanking_region=2000, avgs=True, annot_df=None):
    reg_names = [rg+'-'+str(i) for i in range(int(flanking_region/bin_size)) for rg in ['ds', 'gbl', 'gbr', 'us']]
    result_dic = {}
    for indx, col in enumerate(reg_names):
        try:
            fdf = pd.read_csv('./dump_files/general_intervals_averages_b%d_fr%d/' %(bin_size, flanking_region) +col+'.csv')
            if avgs:
                result_dic[col] = np.mean(predict_snp(model, fdf.dropna()))
            else:
                fdf = fdf.dropna()
                preds = pd.Series([np.nan] * len(annot_df))
                preds.iloc[fdf['Unnamed: 0']] = predict_snp(model, fdf)[:, 0]
                result_dic[col] = preds.tolist()
            if indx % (len(reg_names)//10) == 0: print(col + 'Done!' + str(indx // (len(reg_names)//10) * 10) + '% is done')
        except FileNotFoundError:
            print('Error! You should first save all the feature averages for the bins with calling save_genic_region_feature_avgs')
    return result_dic


def get_general_inrvals_avg(row, seq_dic):
    res = pd.Series(dtype=float)
    bin_len = row['length']
    for col in list(set(row.keys()) - {'chr', 'length'}):
        if not pd.isna(row[col]) and int(row[col]) > 0:
            interval = seq_dic[row['chr']][int(row[col]): int(row[col]) + bin_len]
            res[col] = np.mean(interval)
        else:
            res[col] = np.nan
    return res

#when using this function uncomment the import pandarallele. That is commented due to the python version inconsistencies in Dipankar machine.
def get_genic_avg_marks(annot_df, seq_dic, bin_size=10, flanking_region=2000, avgs=True, force_annot_df=False):
    pandarallel.initialize(progress_bar=False)
    gi_address = './dump_files/general_intervals_b%d_fr%d.csv' %(bin_size, flanking_region)
    if os.path.exists(gi_address) and not force_annot_df:
        general_intervals = pd.read_csv(gi_address)
    else:
        annot_df = annot_df[annot_df.type == 'gene']
        general_intervals = annot_df.parallel_apply(lambda row: regprep.get_gene_intervals(row, bin_size=bin_size, flanking_region=flanking_region), axis=1)
    avg_vlus_df = general_intervals.parallel_apply(lambda row: regprep.get_general_inrvals_avg(row, seq_dic), axis=1)
    if avgs:
        return avg_vlus_df.mean().to_dict()
    else:
        return avg_vlus_df


def snp_avg_cntxbase(annot_df, snp_seq, sequences_df, snp_cntx, bin_size=10, flanking_region=2000):
    pandarallel.initialize(progress_bar=False)
    gi_address = './dump_files/general_intervals_b%d_fr%d.csv' %(bin_size, flanking_region)
    if os.path.exists(gi_address):
        general_intervals = pd.read_csv(gi_address)
    else:
        annot_df = annot_df[annot_df.type == 'gene']
        general_intervals = annot_df.parallel_apply(lambda row: get_gene_intervals(row, bin_size=bin_size, flanking_region=flanking_region), axis=1)
    res = {}
    for idx, col in enumerate(list(set(general_intervals.columns) - {'chr', 'length'})):
        if idx % int((len(general_intervals.columns) - 2)//20) == 0: print('{} out of {}'.format(idx, len(general_intervals.columns) - 2))
        reg_df = pd.DataFrame({'chr': general_intervals['chr'], 'length': general_intervals['length'],
                               'start': general_intervals[col], 'stop': general_intervals[col] + general_intervals['length']})
        reg_df = reg_df.dropna()
        res[col] = np.mean(reg_df.apply(lambda row: count_snps_interval_seq(row['chr'], row['start'], row['stop'], snp_seq, sequences_df, snp_cntx), axis=1))
    return res


def subset_snp_df(raw_snp_df, convs):
    sub_dfs = []
    for conv in convs:
        conv_df = raw_snp_df[((raw_snp_df.REF == conv[0][0]) & (raw_snp_df.ALT == conv[1][0])) |
                                    ((raw_snp_df.REF == conv[0][1]) & (raw_snp_df.ALT == conv[1][1]))]
        sub_dfs.append(conv_df)
        print('conv {}, size of convs{}'.format(str(conv), len(conv_df)))
    return pd.concat(sub_dfs, axis=0)


def predict_snp(model, fdf):
    feature_set = ['H3K14ac', 'H3K23ac', 'H3K27ac',
       'H3K27me1', 'H3K36ac', 'H3K36me3', 'H3K4me1', 'H3K4me2', 'H3K4me3',
       'H3K56ac', 'H3K9ac', 'H3K9me1', 'H3K9me2', 'atac_pct', 'GC_content_pct',
       'CG_pct', 'CHG_pct', 'CHH_pct']
    X = fdf[feature_set]
    return model.predict(X)


def get_bw_feature_seq(cnfg, from_file=False):
    bw_root = cnfg['bw_root']
    bw_seqs, bw_names = preprocess.read_bwseqs(bw_root, from_file=from_file)
    bw_dic = {bw_names[i]: bw_seqs[i] for i in range(len(bw_names))}
    return bw_dic


def convert_annot_df_to_feature_df(annot_df, bin_num=0, bin_count=1, type='gb', frs=1000):
    if type == 'gb':
        res_df = pd.DataFrame({'chr': annot_df['chr'],
                               'start': annot_df['start'] + (((annot_df['end'] - annot_df['start'])*bin_num).astype(int)/bin_count).astype(int),
                               'stop': annot_df['start'] + (((annot_df['end'] - annot_df['start'])*(bin_num+1)).astype(int)/bin_count).astype(int),
                               'length': ((annot_df['end'] - annot_df['start'])/5).astype(int)})
    elif type == 'us':
        res_df = pd.DataFrame({'chr': annot_df['chr'],
                               'start': annot_df['start']-frs + int((frs*(bin_num))/bin_count),
                               'stop': annot_df['start']-frs + int((frs*(bin_num+1))/bin_count),
                               'length': int(frs/bin_count)})
    elif type == 'ds':
        res_df = pd.DataFrame({'chr': annot_df['chr'],
                               'start': annot_df['end']+ int((frs*(bin_num))/bin_count),
                               'stop': annot_df['end'] + int((frs*(bin_num+1))/bin_count),
                               'length': int(frs/bin_count)})
    return res_df

def remove_outliers(X, Y, threshold=2):
    xy = np.column_stack((X, Y))
    mean_y = np.mean(Y)
    std_y = np.std(Y)
    threshold_y = threshold * std_y
    print(threshold_y)
    filtered_xy = xy[(xy[:, X.shape[1]] > mean_y - threshold_y) & (xy[:, X.shape[1]] < mean_y + threshold_y)]
    filtered_X, filtered_Y = filtered_xy[:,0], filtered_xy[:, X.shape[1]]
    return np.array(filtered_X), np.array(filtered_Y)


def input_maker(cnfg, sequences_df, meth_seq_dic, bw_dic, snp_seq, strand, bin_count=5, frs=1000, include_flanking=False):
    annot_df = data_reader.read_annot(cnfg['gene_annotation'])
    annot_df = annot_df[annot_df.type == 'gene']
    annot_df['ID'] = annot_df['attributes'].str.extract(r'ID=([^;]+);')
    annot_df = annot_df[annot_df.end - annot_df.start > bin_count]
    annot_df = annot_df[annot_df.strand == strand]
    res_annots_dic = {}
    for bin_num in range(bin_count):
        for reg in ['gb', 'us', 'ds']:
            res_annot_ = make_feature_df(convert_annot_df_to_feature_df(annot_df, bin_num=bin_num, bin_count=bin_count, type=reg, frs=frs), sequences_df, meth_seq_dic, bw_dic, snp_seq)
            res_annots_dic[reg+str(bin_num)] = res_annot_
        if bin_num%20 == 0:
            print("%d out of %d" %(bin_num, bin_count))
    #res_annots_dic is a dictionary of dataframes,  which maps reg+bin_num to a dataframe which has all the features + 'MA_SNV_pct'
    if include_flanking:
        Y = annot_df.apply(lambda row: count_snps_interval_seq(row['chr'], row['start']-frs, row['end']+frs, snp_seq, sequences_df, None), axis=1) #contain gene flanking regions SNPS
    else:
        Y = annot_df.apply(lambda row: count_snps_interval_seq(row['chr'], row['start'], row['end'], snp_seq, sequences_df, None), axis=1) #only gene body SNPs
    ignore_cols = ['chr', 'start', 'stop', 'length', 'MA_SNV_pct']
    for key in res_annots_dic.keys():
        res_annots_dic[key] = res_annots_dic[key].drop(ignore_cols, axis=1)
    if strand == '+':
        X = pd.concat([res_annots_dic['us'+str(i)] for i in range(bin_count)] +
                      [res_annots_dic['gb'+str(i)] for i in range(bin_count)] +
                      [res_annots_dic['ds'+str(i)] for i in range(bin_count)], axis=1)
    else:
        X = pd.concat([res_annots_dic['ds'+str(i)] for i in range(bin_count-1, -1, -1)] +
                      [res_annots_dic['gb'+str(i)] for i in range(bin_count-1, -1, -1)] +
                      [res_annots_dic['us'+str(i)] for i in range(bin_count-1, -1, -1)], axis=1)
    X['ID'] = annot_df['ID']
    Y = Y[~X.isnull().any(axis=1)]
    X = X.dropna()
    for i in range(3):
        print(X.iloc[i]['ID'], Y.iloc[i])
    return X, Y
