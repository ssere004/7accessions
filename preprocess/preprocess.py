from preprocess import data_reader
import numpy as np
from os import path
import pandas as pd
import sys
import pyBigWig
from preprocess import configs
from scipy.stats import pearsonr, spearmanr
import pickle

def make_methseq_dic(filename, methylations, sequences, coverage_thrshld, from_file=False):
    fn = './dump_files/' + filename
    if from_file and path.exists(fn):
        return data_reader.load_dic(fn)
    methylations['mlevel'] = methylations['meth']/(methylations['meth'] + methylations['unmeth'])
    methylations['coverage'] = methylations['meth'] + methylations['unmeth']
    methylations['mlevel'] = methylations['mlevel'].fillna(0)
    methylations.loc[(methylations.mlevel == 0), 'mlevel'] = 0
    methylations.loc[(methylations.coverage < coverage_thrshld), 'mlevel'] = 0
    #methylations.loc[(methylations.strand == '-'), 'mlevel'] = -1 * methylations.mlevel
    meth_seq = {}
    count = 0
    for chr in sequences.keys():
        meths = np.zeros(len(sequences[chr]))
        meth_subset = methylations[methylations['chr'] == chr]
        meths[[meth_subset['position'] - 1]] = meth_subset['mlevel']
        meth_seq[chr.lower()] = np.expand_dims(meths, axis=1)
        count += 1
        print(count, len(sequences.keys()))
    data_reader.save_dic(fn, meth_seq)
    return meth_seq

def make_seq_dic(assembly, df, from_file=False, fn=None):
    if fn!=None:
        fn = './dump_files/' + fn + '_seq.pkl'
    if from_file and path.exists(fn):
        return data_reader.load_dic(fn)
    res_dic = {}
    for chro in assembly.keys():
        res = np.zeros(len(assembly[chro]))
        res[[df[df['chr'] == chro]['position']]] = df[df['chr'] == chro]['value']
        res_dic[chro.lower()] = res
    if fn!=None:
        data_reader.save_dic(fn, res_dic)
    return res_dic

def make_snp_seq(syri, assemebly):
    res = {}
    for chr in assemebly.keys():
        snps = np.zeros(len(assemebly[chr]))
        syri_subset = syri[syri.chr1 == chr]
        snps[[syri_subset['start1'].astype(int)-1]] = 1
        res[chr.lower()] = np.expand_dims(snps, axis=1)
    return res


def make_annotseq_dic(annot_df, annot_types, sequences, strand_spec=False):
    annot_seqs = {}
    strand_num = 2 if strand_spec else 1
    for chr in sequences.keys():
        annot_seqs[chr] = np.zeros((len(sequences[chr]), strand_num*len(annot_types)))
        for at_idx, at in enumerate(annot_types):
            annot_subset_df = annot_df[annot_df.type == at]
            annot_seq_p = np.zeros((len(sequences[chr]), 1), dtype='short')
            annot_seq_n = np.zeros((len(sequences[chr]), 1), dtype='short')
            annot_df_chr_subset = annot_subset_df[annot_subset_df['chr'] == chr]
            for index, row in annot_df_chr_subset.iterrows():
                if row['strand'] == '+' or not strand_spec:
                    annot_seq_p[int(row['start'] - 1): int(row['end'] - 1)] = 1
                else:
                    annot_seq_n[int(row['start'] - 1): int(row['end'] - 1)] = 1
            annot_seqs[chr.lower()][:, at_idx*strand_num] = annot_seq_p[:, 0]
            if strand_spec:
                annot_seqs[chr.lower()][:, at_idx*strand_num + 1] = annot_seq_n[:, 0]
    return annot_seqs

def interval_filler(df, l):
    arr = np.zeros(l, dtype=int)
    df.apply(lambda row: fill_interval(arr, row['start'], row['end']), axis=1)
    return arr

def fill_interval(arr, start, stop):
    arr[start:stop] = 1

# Makes a seqeuence the same as seq_dic and assign 1 to the intervals of df
def make_interval_seq_dic(seq_dic, df):
    res_seq_dic = {}
    for key in seq_dic.keys():
        res_seq_dic[key] = interval_filler(df[df['chr'] == key], len(seq_dic[key]))
    return res_seq_dic


def make_snp_reg_dataframe(syri_df, window_size):
    syri_interval_df = pd.DataFrame({'chr':syri_df['chr1'], 'type':'snp', 'strand': '+',
                                         'start':pd.to_numeric(syri_df['start1']) - window_size,
                                         'end':pd.to_numeric(syri_df['start1']) + window_size})
    syri_interval_df['chr'] = syri_interval_df['chr'].apply(str.lower)
    return syri_interval_df

def normalize_arr(arr, min, max, nan_replace = 0):
    res = (arr - min) / (max - min)
    res[np.isnan(res)] = nan_replace
    return res

#this function gets a dicitonary of 1d numpy arrays and converts them into a dataframe considering the numpy arrays are showing the positions for the chrs.
def convert_dic_to_df(seq_dic):
    total_elements = sum(len(positions) for positions in seq_dic.values())
    chr_values = np.empty(total_elements, dtype=object)
    position_values = np.empty(total_elements, dtype=int)
    current_index = 0
    for chr_name, positions in seq_dic.items():
        num_positions = len(positions)
        chr_values[current_index: current_index + num_positions] = chr_name
        position_values[current_index: current_index + num_positions] = positions
        current_index += num_positions
    df = pd.DataFrame({'chr': chr_values, 'position': position_values})
    return df


def convert_bigWig_to_seq_dic(bw, nan_replace=0, fn=None, from_file=False, log=True):
    fn = './dump_files/' + fn + '_annot_seq.pkl'
    if from_file and path.exists(fn):
        return data_reader.load_dic(fn, log=log)
    annot_seqs = {}
    min = 2147483647
    max = -2147483648
    for chro in bw.chroms().keys():
        arr = np.asarray(bw.values(chro, 0, bw.chroms()[chro]))
        if np.max(arr[~np.isnan(arr)]) > max:
            max = np.max(arr[~np.isnan(arr)])
        if np.min(arr[~np.isnan(arr)]) < min:
            min = np.min(arr[~np.isnan(arr)])
        annot_seqs[chro.lower()] = arr
    for chro in annot_seqs.keys():
        annot_seqs[chro] = np.expand_dims(normalize_arr(annot_seqs[chro], min, max, nan_replace), axis=1)
    print('bw file min and max are %s %s' % (min, max))
    data_reader.save_dic(fn, annot_seqs)
    return annot_seqs

#Get average of normalized values of several bigWig files. if all of the files at one position were nan, it saves 0 at that place.
def combine_bigWig_files(filenames, output_dir):
    bw_seqs = []
    for fn in filenames:
        bw_seqs.append(convert_bigWig_to_seq_dic(data_reader.read_bigWig(fn), nan_replace=np.nan))
    res_bw_seq = {}
    for chr in bw_seqs[0].keys():
        stacked_arrays = np.stack([bw_seqs[i][chr] for i in range(len(bw_seqs))])
        res_bw_seq[chr] = np.nan_to_num(np.nanmean(stacked_arrays, axis=0), 0)
    save_bw_seq_to_file(res_bw_seq, output_dir)

def save_bw_seq_to_file(bw_seqs, output_dir):
    chroms = list(bw_seqs.keys())
    sizes = [len(bw_seqs[key]) for key in bw_seqs.keys()]
    bw = pyBigWig.open(output_dir, 'w')
    bw.addHeader(list(zip(chroms, sizes)))
    for chrom in chroms:
        starts = list(range(len(bw_seqs[chrom])))
        ends = [i+1 for i in range(len(bw_seqs[chrom]))]
        chrs = [chrom for i in range(len(bw_seqs[chrom]))]
        values = bw_seqs[chrom]
        bw.addEntries(chrs, starts, ends=ends, values=values)
    bw.close()

def convert_seq_to_onehot(seq):
    seq_np = np.asarray(list(seq))
    seq_np = np.char.lower(seq_np)
    idx = np.char.equal(seq_np, np.asarray(['a'] * len(seq)))
    As = np.zeros((len(seq), 1), dtype='short')
    As[idx] = 1
    idx = np.char.equal(seq_np, np.asarray(['c'] * len(seq)))
    Cs = np.zeros((len(seq), 1), dtype='short')
    Cs[idx] = 1
    idx = np.char.equal(seq_np, np.asarray(['g'] * len(seq)))
    Gs = np.zeros((len(seq), 1), dtype='short')
    Gs[idx] = 1
    idx = np.char.equal(seq_np, np.asarray(['t'] * len(seq)))
    Ts = np.zeros((len(seq), 1), dtype='short')
    Ts[idx] = 1
    res = np.concatenate([As, Cs, Gs, Ts], axis=1)
    return res

def convert_assembely_to_onehot(organism_name, sequences, from_file=False):
    fn = './dump_files/' + organism_name + '_sequences_onehot.pkl'
    if from_file and path.exists(fn):
        return data_reader.load_dic(fn)
    one_hots = {}
    for key in sequences.keys():
        one_hots[key.lower()] = convert_seq_to_onehot(sequences[key])
    data_reader.save_dic(fn, one_hots)
    return one_hots


def get_window_seqgene_df(sequences_df, methseq_df, chro, center, window_size):
    profile_df = sequences_df[chro][center - int(window_size/2): center + int(window_size/2)]
    profile_df = np.concatenate([profile_df, methseq_df[chro][center - int(window_size/2): center + int(window_size/2)]], axis=1)
    return profile_df

def get_profiles(methylations, sample_set, sequences_onehot, annot_seqs_onehot, num_to_chr_dic, window_size=3200):
    boundary_cytosines = 0
    column_num = 4
    for i in range(len(annot_seqs_onehot)):
        column_num += annot_seqs_onehot[i][list((annot_seqs_onehot[i].keys()))[0]].shape[1]

    profiles = np.zeros([len(sample_set), window_size, column_num], dtype='short')
    targets = np.zeros(len(sample_set), dtype='short')
    for index, position in enumerate(sample_set):
        row = methylations.iloc[position]
        center = int(row['position'] - 1)
        chro = num_to_chr_dic[row['chr']]
        targets[index] = round(float(row['mlevel']))
        try:
            profiles[index] = get_window_seqgene_df(sequences_onehot, annot_seqs_onehot, chro, center, window_size)
        except:
            boundary_cytosines += 1
    if boundary_cytosines > 1000:
        print('ERRORR in profile generator')
    return profiles, targets

def complement_mutation(control_bp, test_bp):
    complements = {'A': 'T', 'C': 'G', 'G': 'C', 'T': 'A'}
    return complements[control_bp], complements[test_bp]



def get_available_chrs(input_list, syri):
    chrs = set(syri.chr1.unique())
    for inp in input_list:
        chrs = chrs & inp.keys()
    res = []
    for chro in chrs:
        ref_size = len(input_list[0][chro])
        all_same_size =True
        for inp in input_list:
            if len(inp[chro]) != ref_size:
                all_same_size = False
        if all_same_size:
            res.append(chro)
    return res


def remove_extra_chrs(input_list, available_chrs):
    for inp in input_list:
        chrs =list(inp.keys())
        for chro in chrs:
            if chro not in available_chrs:
                del inp[chro]
    return input_list

def read_bwseqs(bw_root, from_file=False, log=True):
    with open("./file_records/bw_addresses.txt", "r") as file:
        bw_addresses = [bw_root+line.strip() for line in file]
    bw_seqs = []
    bw_names = []
    for bw_fn in bw_addresses:
        fn = bw_fn.split('/')[-1]
        bw_seqs.append(convert_bigWig_to_seq_dic(data_reader.read_bigWig(bw_fn), fn=fn, from_file=from_file, log=log))
        bw_names.append(fn)
    return bw_seqs, bw_names

def pearson_sec_dics(seq_dic1, seq_dic2, bin_size):
    assert len(seq_dic1) == len(seq_dic2), 'two seq dics are not having the same length'
    assert seq_dic1.keys() == seq_dic2.keys(), 'two seq dics are not having the same keys'
    corr_dic = {}
    for chro in seq_dic1.keys():
        assert len(seq_dic1[chro]) == len(seq_dic2[chro]), 'one of the seq dics chromosomes are not equal size'
        seq1 = np.mean(seq_dic1[chro][:len(seq_dic1[chro]) - len(seq_dic1[chro]) % bin_size].reshape((int(len(seq_dic1[chro])/bin_size), bin_size)), axis=1)
        seq2 = np.mean(seq_dic2[chro][:len(seq_dic2[chro]) - len(seq_dic2[chro]) % bin_size].reshape((int(len(seq_dic2[chro])/bin_size), bin_size)), axis=1)
        corr_dic[chro], _ = pearsonr(seq1, seq2)
    chro_list = list(seq_dic1.keys())
    return np.average([corr_dic[chro] for chro in chro_list], weights=[len(seq_dic1[chro]) for chro in chro_list])


def gene_specify(targets, annot_seq_dic, reverse=False):
    res_dic = {}
    for key in targets.keys():
        if reverse:
            res_dic[key] = np.setdiff1d(targets[key], np.nonzero(annot_seq_dic[key][:, 0])[0])
        else:
            res_dic[key] = np.intersect1d(np.nonzero(annot_seq_dic[key][:, 0])[0], targets[key])
    return res_dic


def find_min_conversion_num(raw_snp_df):
    min = len(raw_snp_df)
    for conv in [('AT', 'CG'), ('AT', 'GC'), ('AT', 'TA'), ('CG', 'GC'), ('CG', 'TA'), ('GC', 'TA')]:
        sub_df = raw_snp_df[((raw_snp_df.REF == conv[0][0]) & (raw_snp_df.ALT == conv[1][0])) | ((raw_snp_df.REF == conv[0][1]) & (raw_snp_df.ALT == conv[1][1]))]
        if len(sub_df) < min:
            min = len(sub_df)
    return min

def read_raw_snps(accession):
    if accession == configs.mutation_bias['og']:
        return data_reader.read_raw_mutation_bias_snps(configs.root + 'col-0/epi_data/raw_variants.txt')
    elif accession == configs.c24['og'] or accession == configs.ler['og'] or accession == configs.sha['og'] or accession == configs.cvi['og']:
        cnfg_snp = convert_og_to_cnfg(accession)
        raw_snp_df = data_reader.read_syri(cnfg_snp['syri'], annot_type='SNP')
        raw_snp_df = raw_snp_df.rename(columns={'chr1': 'chr', 'start1': 'position', 'seq1': 'REF', 'seq2': 'ALT'})
        raw_snp_df['position'] = raw_snp_df['position'].astype(int)
        raw_snp_df['value'] = 1
        return raw_snp_df
    elif configs.g1001_high['og'] == accession or configs.g1001_low['og'] == accession \
            or configs.g1001_low01['og'] == accession or configs.g1001_low05['og'] == accession\
            or configs.g1001_high1['og'] == accession or configs.g1001_high05['og'] == accession:
        return data_reader.read_allele_freq_snvs(convert_og_to_cnfg(accession)['snv_file'])

def get_meth_seq_dic(cnfg, rc, from_file=False):
    fn = './dump_files/' + cnfg['og'] + '_covthreshold_' + str(rc['ct']) + '_meth_seq_dic.pkl'
    if from_file and path.exists(fn):
        return data_reader.load_dic(fn)
    sequences_df = convert_assembely_to_onehot(cnfg['og'], data_reader.readfasta(cnfg['assembly']), from_file=from_file)
    meth_seq_dic = {}
    methylations = data_reader.read_geo_tsv_file(cnfg['methylation_tsv'], '', coverage_threshold=rc['ct'])
    for context in ['CG', 'CHG', 'CHH']:
        methyl_sub_df = methylations[methylations.context == context]
        meth_seq_dic[context] = make_seq_dic(sequences_df, methyl_sub_df.rename(columns={'methylation_call': 'value'}), from_file=from_file, fn=cnfg['og']+'_meth_'+context)
    data_reader.save_dic(fn, meth_seq_dic)
    return meth_seq_dic

def convert_og_to_cnfg(cnfg_name):
    cnfgs = [configs.mutation_bias, configs.c24, configs.cvi, configs.ler, configs.sha, configs.g1001_high, configs.g1001_low,
             configs.g1001_high1, configs.g1001_high05, configs.g1001_low01, configs.g1001_low05]
    ogs = [configs.mutation_bias['og'], configs.c24['og'], configs.cvi['og'], configs.ler['og'], configs.sha['og'], configs.g1001_high['og'], configs.g1001_low['og'],
           configs.g1001_high1['og'], configs.g1001_high05['og'], configs.g1001_low01['og'], configs.g1001_low05['og']]
    return cnfgs[ogs.index(cnfg_name)]


def get_corr_rowwise(df1, df2):
    if df1.shape != df2.shape:
        raise ValueError("Both dataframes must have the same shape")
    correlations = []
    for index, (row1, row2) in enumerate(zip(df1.iterrows(), df2.iterrows())):
        _, row1_data = row1
        _, row2_data = row2
        valid_mask = ~np.isnan(row1_data) & ~np.isnan(row2_data)
        valid_row1 = row1_data[valid_mask]
        valid_row2 = row2_data[valid_mask]
        if len(valid_row1) < 2:
            correlations.append(np.nan)  # Can't compute correlation for less than 2 values
        else:
            correlation = np.corrcoef(valid_row1, valid_row2)[0, 1]
            correlations.append(correlation)
    return correlations

def weighted_pearson(x, y):
    weights = np.exp(-((np.linspace(0, 1, len(x)) - 0.5) ** 2) / (2 * (0.2 ** 2)))
    mean_xw = np.sum(weights * x) / np.sum(weights)
    mean_yw = np.sum(weights * y) / np.sum(weights)
    numerator = np.sum(weights * (x - mean_xw) * (y - mean_yw))
    denominator = np.sqrt(np.sum(weights * (x - mean_xw) ** 2) * np.sum(weights * (y - mean_yw) ** 2))
    rw = numerator / denominator
    return rw



def seq_dic_gw_average(seq_dic):
    sum_len = np.sum([len(seq_dic[key]) for key in seq_dic.keys()])
    sum_vals = np.sum([np.sum(seq_dic[key]) for key in seq_dic.keys()])
    return sum_vals / sum_len


def seq_dic_windower(seq_dic, window_size):
    # Gets a binary seqeuence dictionary. where for each chromosome it contain a binary numpy array,
    # For each of the 1 values in the array it expand the 1s to the window size.
    # Returns another seq dic with expanded window ones.
    dfs = []
    for key in seq_dic.keys():
        arr = seq_dic[key]
        pos = np.where(arr == 1)[0]
        dfs.append(pd.DataFrame({'chr': key, 'start': pos - int(window_size / 2), 'end': pos + int(window_size / 2)+1}))
    df = pd.concat(dfs, axis=0, ignore_index=True)
    return make_interval_seq_dic(seq_dic, df)


def gw_fragments_stat(seq_dic, mask_dic, avg=True):
    # Gets two sequence dictionaries, where one has the original values and the other has a mask for the first numpy array(mask is dictionary of binary numpy array)
    # Return the average or all values of seq_dic values where the mask is one.
    if avg:
        sum_len = np.sum([len(seq_dic[key][mask_dic[key] == 1]) for key in seq_dic.keys()])
        sum_vals = np.sum([np.sum(seq_dic[key][mask_dic[key] == 1]) for key in seq_dic.keys()])
        return sum_vals/sum_len
    else:
        return np.concatenate([seq_dic[key][mask_dic[key] == 1] for key in seq_dic.keys()])


def gw_sample_stat(seq_dic, mask_dic, n):
    genome_size = np.sum([len(seq_dic[key]) for key in seq_dic.keys()])
    sample_sizes = {key: len(seq_dic[key])*n/genome_size for key in seq_dic.keys()}
    values = []
    for key in seq_dic.keys():
        indices = np.where(mask_dic[key] == 1)[0]
        selected_indices = np.random.choice(indices, int(sample_sizes[key]), replace=False)
        values.append(seq_dic[key][selected_indices])
    return np.concatenate(values)


def compare_feature_snps(seq_dic, snp_seq, snp_seq_window, avg=True):
    if 'chrm' in seq_dic.keys(): del seq_dic['chrm']
    if 'chrm' in snp_seq.keys(): del snp_seq['chrm']
    w_snp_seq = seq_dic_windower(snp_seq, snp_seq_window)
    if avg:
        return gw_fragments_stat(seq_dic, w_snp_seq), seq_dic_gw_average(seq_dic)
    else:
        return gw_fragments_stat(seq_dic, w_snp_seq, avg=False), \
               gw_sample_stat(seq_dic, {key:np.logical_not(w_snp_seq[key]).astype(int) for key in w_snp_seq.keys()}, 100000)


# This function gets a dictionary of nupmy array and a dataframe containing [chr, start, stop] columns,
# returns a subset of seq_dic where it is defined in the regs.
def subset_seq_dic(seq_dic, reg_df, reverse=False):
    reg_seq = make_interval_seq_dic(seq_dic, reg_df)
    if reverse:
        for chr in reg_seq.keys():
            reg_seq[chr] = np.logical_not(reg_seq[chr]).astype(int)
    res_dic = {}
    for chr in seq_dic.keys():
        res_dic[chr] = seq_dic[chr][np.where(reg_seq[chr] == 1)[0]]
    return res_dic
