NON_METH_TAG = 0.001
bw_dic_keys = ['H3K14ac-SRX1469119.bw', 'H3K23ac_merged.bw', 'H3K27ac_merged.bw', 'H3K27me1-SRX905130.bw',
               'H3K36ac_merged.bw', 'H3K36me3-SRX1518744.bw', 'H3K4me1_merged.bw', 'H3K4me2_merged.bw',
               'H3K4me3_merged.bw', 'H3K56ac-SRX1518745.bw', 'H3K9ac_merged.bw', 'H3K9me1-SRX361945.bw',
               'H3K9me2_merged.bw', 'ATAC_merged.bw']

feature_set = ['H3K14ac', 'H3K23ac', 'H3K27ac',
    'H3K27me1', 'H3K36ac', 'H3K36me3', 'H3K4me1', 'H3K4me2', 'H3K4me3',
    'H3K56ac', 'H3K9ac', 'H3K9me1', 'H3K9me2', 'atac_pct', 'GC_content_pct',
    'CG_pct', 'CHG_pct', 'CHH_pct']
