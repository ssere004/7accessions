import pandas as pd
import vcf

#the big vcf file is located at H4 /home/ssere004/Arabidopsis/1001/snp

accession = '6911'
cnt = 1
res = pd.DataFrame({'#CHROM': [], 'POS': [], 'REF': [], 'ALT': [], 'INFO': [], 'FORMAT': [], accession: []})
for chunk in pd.read_csv("1001genomes_snp-short-indel_only_ACGTN.vcf", sep='\t', skiprows=8, chunksize=10000):
    sub = chunk[['#CHROM', 'POS', 'REF', 'ALT', 'INFO', 'FORMAT', accession]]
    res = pd.concat([res, sub], axis=0)
    print(cnt * 100000, 'is done')
    cnt += 1
res.to_csv(accession + '_snps_.csv', sep='\t')

#substring = './.'
#mask = sub.applymap(lambda x: substring in x.lower() if isinstance(x,str) else False).to_numpy()


def vcf_parser():
    #vcf_reader = vcf.Reader(open('1001genomes_snp-short-indel_only_ACGTN.vcf', 'r'))
    vcf_reader = vcf.Reader(open('intersection_6911.vcf', 'r'))
    GT_lst = set()
    cnt = 0
    for record in vcf_reader:
        for sample in record.samples:
            GT_lst.add(sample['GT'])
            break
        break
        cnt += 1
        if cnt == 1000:
            break
    return GT_lst

def extract_snps():
    df = pd.read_csv('../../7213_snps.csv', sep = '\t')
    df['7213'] = df['7213'].str[:3]
    df_ = df[(df['7213'] != '0|0') & (df['7213'] != './.')]
    tmp = df_[['#CHROM', 'POS']]
    tmp.columns = ['chr1', 'start1']
    tmp['chr1'] = tmp['chr1'].replace([1, 2, 3, 4, 5], ['chr1', 'chr2', 'chr3', 'chr4', 'chr5'])
    tmp.to_csv('7213_final_snps.csv', sep = '\t')




def count_snps_in_positons():
    vcf_reader = vcf.Reader(open('1001genomes_snp-short-indel_only_ACGTN.vcf'))
    snp_data = {}
    progress = 0
    errors = 0
    for record in vcf_reader:
        try:
            chrom = record.CHROM
            pos = record.POS
            ref = record.REF
            alt = str(record.ALT[0])
            total_snp_count = 0
            for sample in record.samples:
                gt = sample['GT']
                if gt == '1|1':
                    total_snp_count += 1
            if pos in snp_data:
                snp_data[pos]['pos'] = pos
                snp_data[pos]['SNP Count'] += total_snp_count
                snp_data[pos]['Chromosome'] = chrom
                snp_data[pos]['REF'] = ref
                snp_data[pos]['ALT'] = alt
            else:
                snp_data[pos] = {'pos': pos,
                                 'SNP Count': total_snp_count,
                                 'Chromosome': chrom,
                                 'REF': ref,
                                 'ALT': alt}
            progress += 1
            if progress % 10000 == 0:
                print(str(progress) + ' out of 12,883,863  %' + str(progress*100/12883863))
        except:
            errors+=1
    print('finished with this number of errors: ' + str(errors))
    df = pd.DataFrame.from_dict(snp_data, orient='index')
    df.sort_index(inplace=True)
    return df


def convert_syri_to_vcf(syri_df, output_fn):
    header = '##fileformat=VCFv4.2\n#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\n'
    progress = 0
    with open(output_fn, 'w') as f:
        f.write(header)
        for _, row in syri_df.iterrows():
            chrom = row['chr1']
            pos = row['start1']
            ref = row['seq1']
            alt = row['seq2']
            vcf_record = f'{chrom}\t{pos}\t.\t{ref}\t{alt}\t.\t.\t.\n'
            f.write(vcf_record)
            progress+=1
            if progress % 10000 == 0:
                print('Progress ' + str(progress) + ' from ' + str(len(syri_df)))


