from Bio import SeqIO
import pandas as pd
import pickle
import pyBigWig

def readfasta(address):
    recs = SeqIO.parse(address, "fasta")
    sequences = {}
    for chro in recs:
        sequences[chro.id.lower()] = chro.seq
    for i in sequences.keys():
        sequences[i] = sequences[i].upper()
    return sequences

def read_bigWig(address):
    return pyBigWig.open(address)

def read_features_data(ff_address):
    features_df = pd.read_csv(ff_address, sep=',')
    features_df.sort_values(by=['chr', 'start'], ascending=[True, True], inplace=True)
    replace_chr_dic = {1: 'chr1', 2: 'chr2', 3: 'chr3', 4: 'chr4', 5: 'chr5'}
    features_df['chr'] = features_df['chr'].replace(replace_chr_dic)
    features_df = features_df[features_df['start'] < features_df['stop']]
    return features_df

def read_raw_mutation_bias_snps(raw_snp_address):
    raw_snp_df = pd.read_csv(raw_snp_address, sep=',')
    raw_snp_df = raw_snp_df[(raw_snp_df.src == 'MA_somatic') | (raw_snp_df.src == 'MA_germline')]
    raw_snp_df['value'] = 1
    raw_snp_df = raw_snp_df.rename(columns={'CHROM': 'chr', 'POS': 'position'})
    replace_chr_dic = {1: 'chr1', 2: 'chr2', 3: 'chr3', 4: 'chr4', 5: 'chr5'}
    raw_snp_df['chr'] = raw_snp_df['chr'].replace(replace_chr_dic)
    raw_snp_df = raw_snp_df[raw_snp_df.TYPE == 'SNP']
    return raw_snp_df

def read_allele_freq_snvs(raw_snp_address):
    raw_snp_df = pd.read_csv(raw_snp_address, sep=',')
    raw_snp_df = raw_snp_df.rename(columns={'pos': 'position'})
    raw_snp_df['value'] = 1
    return raw_snp_df

def read_annot(address, chromosomes = None):
    annot_df = pd.read_table(address, sep='\t', comment='#')
    annot_df.columns = ['chr', 'source', 'type', 'start', 'end', 'score', 'strand', 'phase', 'attributes']
    if chromosomes != None:
        annot_chrs = annot_df.chr.unique()
        for chr in annot_chrs:
            if chr not in chromosomes:
                annot_df = annot_df[annot_df['chr'] != chr]
    annot_df['chr'] = annot_df['chr'].apply(str.lower)
    return annot_df

def read_methylations(address, context, coverage_threshold = 10):
    methylations = pd.read_table(address)
    methylations.columns = ['chr', 'position', 'strand', 'meth', 'unmeth', 'context', 'three']
    methylations['chr'] = methylations['chr'].str.lower()
    methylations.drop(['three'], axis=1)
    if len(context) != 0:
        methylations = methylations[methylations['context'] == context]
    methylations = methylations[methylations['meth'] + methylations['unmeth'] > coverage_threshold]
    if len(context) != 0:
        methylations.drop(['context'], axis=1)
    methylations.drop(['strand'], axis=1)
    methylations = methylations.reset_index(drop=True)
    return methylations

def read_syri(address, annot_type=''):
    sy_df = pd.read_csv(address, header=None, sep='\t')
    sy_df.columns = ['chr1', 'start1', 'end1', 'seq1', 'seq2', 'chr2', 'start2', 'end2', 'ID', 'parent', 'annot', 'copy_stat']
    sy_df['chr1'] = sy_df['chr1']. apply(str. lower)
    if len(annot_type) > 0:
        sy_df = sy_df[sy_df.annot == annot_type]
    return sy_df

#we can remove this function and just convert the .tsv file to Bismark output format,
#Just keep in mind the position of this file are not aligned with Bismark, that is why we add one to the pos column
def read_geo_tsv_file(address, context, coverage_threshold=5):
    df = pd.read_csv(address, sep="\t")
    replace_dic = {'CAG': 'CHG', 'CCA': 'CHH', 'CAT': 'CHH', 'CCC': 'CHH', 'CCT': 'CHH', 'CTA': 'CHH', 'CCG': 'CHG', 'CGA': 'CG',
                   'CGG': 'CG', 'CTC': 'CHH', 'CTG': 'CHG', 'CAA': 'CHH', 'CAC': 'CHH', 'CGT': 'CG', 'CTT': 'CHH', 'CGC': 'CG',
                   'CNN': 'CHH', 'CTN': 'CHH', 'CCN': 'CHH', 'CAN': 'CHH', 'CGN': 'CG'}
    replace_chr_dic = {1: 'chr1', 2: 'chr2', 3: 'chr3', 4: 'chr4', 5: 'chr5'}
    df['three'] = df['mc_class']
    df['mc_class'] = df['mc_class'].replace(replace_dic)
    df['chrom'] = df['chrom'].replace(replace_chr_dic)
    df['unmeth'] = df['total_bases'] - df['methylated_bases']
    df['pos'] = df['pos'] + 1
    colname_replace_dic = {'chrom': 'chr', 'pos': 'position', 'mc_class': 'context', 'methylated_bases': 'meth'}
    df = df.rename(columns=colname_replace_dic)
    if len(context) != 0:
        df = df[df['context'] == context]
    df = df[df['meth'] + df['unmeth'] > coverage_threshold]
    if len(context) != 0:
        df.drop(['context'], axis=1)
    df.drop(['strand'], axis=1)
    df = df.reset_index(drop=True)
    return df

def find_MAF(row):
    bp_freq_dic = {'REF': 'REF_freq', 'ALT1': 'ALT_freq1', 'ALT2': 'ALT_freq2', 'ALT3': 'ALT_freq3', 'ALT4': 'ALT_freq4'}
    ref = 'REF'
    ref_bp = row['REF']
    ref_freq = row['REF_freq']
    res = ''
    for bp in bp_freq_dic.keys():
        if (row[bp_freq_dic[bp]] > ref_freq) & (len(row[bp]) == 1):
            ref_freq = row[bp_freq_dic[bp]]
            ref_bp = row[bp]
            ref = bp
    for alt in bp_freq_dic.keys() - {ref}:
        if len(row[alt]) == 1:
            res += (ref_bp+':'+row[alt]+'-MAF>'+str(row[bp_freq_dic[alt]]) + '/')
    return res[:-1]

def parse_freq_allel(address):
    address = 'allele_freq.vcf.frq'
    df = pd.read_csv(address, sep='\t', skiprows=1, names=['chr', 'pos', 'alleles_num', 'chr_num', 'REF:freq', 'ALT:freq1', 'ALT:freq2', 'ALT:freq3', 'ALT:freq4'], dtype={'chr': str, 'pos':int, 'alleles_num': int})
    df[['REF', 'REF_freq']] = df['REF:freq'].str.split(':', expand=True)
    for col in ['1', '2', '3', '4']:
        df[['ALT'+col, 'ALT_freq'+col]] = df['ALT:freq' + col].str.split(':', expand=True)
    df.drop('REF:freq', axis=1, inplace=True)
    for col in ['freq1', 'freq2', 'freq3', 'freq4']:
        df.drop('ALT:'+col, axis=1, inplace=True)
    df = df[(df['REF'].str.len() == 1) & ((df['ALT1'].str.len() == 1) | (df['ALT2'].str.len() == 1) | (df['ALT3'].str.len() == 1)| (df['ALT4'].str.len() == 1))]
    df['chr'] = df['chr'].replace({'1': 'chr1', '2': 'chr2', '3': 'chr3', '4': 'chr4', '5': 'chr5'})
    df = df.astype({'chr': str, 'pos': int, 'REF': str, 'ALT1': str, 'ALT2': str, 'ALT3': str, 'ALT4': str,
                    'ALT_freq1': float, 'ALT_freq2': float, 'ALT_freq3': float, 'ALT_freq4': float,
                    'REF_freq': float, 'alleles_num': int, 'chr_num': int})
    maf_df = df[['chr', 'pos', 'chr_num']]
    maf_df['mafs'] = df.apply(lambda row: find_MAF(row), axis=1)
    maf_df[['maf1', 'maf2', 'maf3']] = maf_df['mafs'].str.split('/', expand=True)
    for i in range(1, 4):
        maf_df[['REF'+str(i), 'ALT'+str(i), 'MAF'+str(i)]] = maf_df['maf'+str(i)].str.extract(r'^(.*):(.*)-MAF>(.*)$')
    res_df = pd.DataFrame(columns=['chr', 'pos', 'chr_num', 'REF', 'ALT', 'MAF'])
    for i in range(1, 4):
        maf_sub_df = maf_df[['chr', 'pos', 'chr_num', 'REF'+str(i), 'ALT'+str(i), 'MAF'+str(i)]]
        maf_sub_df.columns = ['chr', 'pos', 'chr_num', 'REF', 'ALT', 'MAF']
        res_df = pd.concat([res_df, maf_sub_df], axis=0)
    res_df.MAF = res_df.MAF.astype(float)
    res_df = res_df.dropna()
    res_df.to_csv('Minor_allele_frequencies.csv', index=False)
    #if minor allele frequency is low it means there is lower number of that SNP in the population.
    res_df[res_df.MAF <= 0.05].to_csv('low_freq_SNVs_0.05.csv', index=False)
    res_df[res_df.MAF > 0.05].to_csv('high_freq_SNVs_0.05.csv', index=False)
    res_df[res_df.MAF <= 0.01].to_csv('low_freq_SNVs_0.01.csv', index=False)
    res_df[res_df.MAF > 0.1].to_csv('high_freq_SNVs_0.1.csv', index=False)
    return res_df

def save_dic(file_name, dic):
    f = open(file_name, "wb")
    pickle.dump(dic, f)
    f.close()
    print(file_name + ' saved as pickle ')

def load_dic(file_name, log=True):
    f = open(file_name, "rb")
    res = pickle.load(f)
    f.close()
    if log: print(file_name + ' loaded from a pickle ')
    return res
