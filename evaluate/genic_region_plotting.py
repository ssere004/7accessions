import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from datetime import datetime

def violin_plot_snp_density(fn, data, titles, main_title, plot_type='violin'):
    #max = np.max([np.max(dd) for dd in data])
    #ylim = (0, max)
    fig, axs = plt.subplots(1, len(data), figsize=(12, 6), sharey=True)
    for i in range(len(data)):
        if plot_type == 'violin':
            sns.violinplot(y=data[i], ax=axs[i])
        elif plot_type == 'box':
            sns.boxplot(data=data[i], ax=axs[i])
        #axs[i].set_ylim(ylim)
        axs[i].set_title(titles[i])
    fig.suptitle(main_title)
    plt.subplots_adjust(wspace=0.5)
    plt.savefig(fn)
    plt.clf()
    plt.close()


#based on a trained model. it plot the averaged snp probabilities for flanking regions and gene bodies.
def plot_genic_intervals_avg_predictions(avgs_dic, max_val, flanking_size=2000, plot_tag=''):
    bin_num = len([kk for kk in avgs_dic.keys() if 'ds-' in kk])
    res_l = [avgs_dic['ds-'+str(i)] for i in range(bin_num)] + [avgs_dic['gbl-'+str(i)] for i in range(bin_num)]
    res_r = [avgs_dic['gbr-'+str(i)] for i in range(bin_num)] + [avgs_dic['us-'+str(i)] for i in range(bin_num)]
    fig, axs = plt.subplots(1, 2, figsize=(10, 4))
    axs[0].plot(range(len(res_l)), res_l)
    axs[1].plot(range(len(res_r)), res_r)
    for ax in axs:
        ax.set_xticks([0, bin_num, 2*bin_num-1])
        ax.set_xticklabels([-flanking_size, 0, flanking_size])
        ax.set_ylim([0, max_val*1.1])
    fig.suptitle(plot_tag, fontsize=14, fontweight="bold", y=0.95)
    plt.savefig('reg_pred_' + plot_tag +'_' + str(datetime.now()) + '.png')
    plt.close()

def plot_gene_body(tracks_l, tracks_r, track_names, plot_tag, max_value=-1):
    fig, axs = plt.subplots(1, 3, figsize=(12, 4), gridspec_kw={'width_ratios': [1, 1, 0.4]})
    for i in range(len(track_names)):
        res_l = tracks_l[i]
        res_r = tracks_r[i]
        axs[0].plot(range(len(res_l)), res_l, label=track_names[i])
        axs[1].plot(range(len(res_r)), res_r, label=track_names[i])
    if len(track_names) < 5:
        nlegendcol = len(track_names)
    else:
        nlegendcol = 2
    if max_value == -1:
        max_value = max(max([max(sublist) for sublist in tracks_l]), max([max(sublist) for sublist in tracks_r]))
    axs[0].set_ylim([0, max_value*1.1])
    axs[1].set_ylim([0, max_value*1.1])
    axs[2].set_xticks([])
    axs[2].set_yticks([])
    axs[2].set_frame_on(False)
    handles, labels = axs[0].get_legend_handles_labels()
    axs[2].legend(handles, labels, loc='center left', frameon=False, ncol=nlegendcol, fontsize=8)
    plt.savefig('gene_avg_' + plot_tag +'_' + str(datetime.now()) + '.png')
    plt.close()

def correlation_plot_saved_np_files():
    y_pred = np.load('y_pred.npy')
    y_test = np.load('y_test.npy')
    plt.scatter(y_pred, y_test, s=2)
    plt.xlabel('y_pred')
    plt.ylabel('Y-test')
    plt.title('Correlation between two vectors')
    plt.show()
    plt.close()

def correlation_plot(fn, y_test, y_pred, corr_coef):
    plt.scatter(y_pred, y_test, s=2)
    plt.xlabel('y_pred')
    plt.ylabel('Y-test')
    plt.title('Corr_coeff : ' + str(corr_coef))
    plt.savefig(fn)
    plt.close()
