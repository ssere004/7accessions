import pandas as pd
import matplotlib.pyplot as plt
import preprocess.data_reader as dr
import seaborn as sns


#
#
# # Creating a DataFrame from the vals dictionary
root = '/Users/salehsereshki/Desktop/ServerDownloadTemp/snp_prediction/gw_vs_snp_violin_data/c24_convs/'
# genic = False
# conv = 'AT-TA'
# convs = ('AT', 'CG'), ('AT', 'TA'), ('CG', 'GC'), ('CG', 'TA'), ('GC', 'TA')
# for c in convs:
#     for genic in [True, False]:
#         conv = c[0]+'-'+c[1]
#         vals = dr.load_dic(root+'c24_genic_{}_snpwindow_10_{}.pkl'.format(str(genic), conv))
#         vals_ = {}
#         for key in vals.keys():
#             if len(vals[key].shape) > 1:
#                 vals_[key] = vals[key][:,0]
#             else:
#                 vals_[key] = vals[key]
#
#         vals = vals_
#         df = pd.DataFrame.from_dict(vals, orient='index').transpose()
#         df = pd.DataFrame.from_dict(vals, orient='index').transpose()
#
#         tags = set(key.rsplit('_')[0] for key in vals.keys())
#         n_tags = len(tags)
#         fig, axes = plt.subplots(1, n_tags, figsize=(5 * n_tags, 20))
#         for i, (ax, tag) in enumerate(zip(axes, sorted(tags))):
#             gw_key = f"{tag}_gw_vals"
#             snps_key = f"{tag}_snps_vals"
#             sub_df = df[[gw_key, snps_key]]
#             sns.violinplot(data=sub_df, palette="Set3", bw=.2, cut=1, linewidth=1, ax=ax)
#             ax.set_title(tag)
#             if i != 0:
#                 ax.set_yticklabels([])
#                 ax.set_yticks([])
#             ax.set_ylim(ax.get_ylim()[0], 0.2)
#             ax.set_xticklabels(ax.get_xticklabels(), rotation='vertical')
#         plt.tight_layout()
#         plt.savefig('/Users/salehsereshki/Desktop/ServerDownloadTemp/snp_prediction/gw_vs_snp_violin_data/c24_convs/c24_genic_{}_conv_{}.png'.format(str(genic), conv))


# for genic in [True, False]:
#     add = 'c24_genic_{}_snpwindow_10_all.pkl'.format(genic)
def plot(vals, fn):
    vals_ = {}
    for key in vals.keys():
        if len(vals[key].shape) > 1:
            vals_[key] = vals[key][:,0]
        else:
            vals_[key] = vals[key]

    vals = vals_
    df = pd.DataFrame.from_dict(vals, orient='index').transpose()
    df = pd.DataFrame.from_dict(vals, orient='index').transpose()

    tags = set(key.rsplit('_')[0] for key in vals.keys())
    n_tags = len(tags)
    fig, axes = plt.subplots(1, n_tags, figsize=(5 * n_tags, 20))
    for i, (ax, tag) in enumerate(zip(axes, sorted(tags))):
        gw_key = f"{tag}_gw_vals"
        snps_key = f"{tag}_snps_vals"
        sub_df = df[[gw_key, snps_key]]
        sns.violinplot(data=sub_df, palette="Set3", bw=.2, cut=1, linewidth=1, ax=ax)
        ax.set_title(tag, fontsize=40)
        if i != 0:
            ax.set_yticklabels([])
            ax.set_yticks([])
        ax.set_ylim(ax.get_ylim()[0], 0.2)
        ax.set_xticklabels(ax.get_xticklabels(), rotation='vertical', fontsize=40)
        if i == 0:
            ax.set_yticklabels(ax.get_yticklabels(), fontsize=40)
    plt.tight_layout()
    plt.savefig(fn)
