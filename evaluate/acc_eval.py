import evaluate.file_sys as fs
import preprocess.data_reader as dr
import preprocess.preprocess as prep
import pandas as pd

tags = fs.tags

accessions = ['mutation_b', 'c24', 'ler', 'sha', 'cvi', 'g1001_high05', 'g1001_low05', 'g1001_high1', 'g1001_low01']
corr_pairs_all = [('{}_real_all'.format(acc), '{}_prediction_all'.format(acc), acc) for acc in accessions]
corr_pairs_convs = [('{}_real_convs'.format(acc), '{}_prediction_convs'.format(acc), acc) for acc in accessions]

setups = ['small_bin_training_all', 'NN_model_all', 'GC_exclude_all']
corr_pairs_setups = [('c24_real_all', 'c24_{}'.format(setup)) for setup in setups]

corr_pair_trans = [('c24_transversion_pred_all', 'c24_transversion_real_all'),
                   ('c24_transition_pred_all', 'c24_transition_real_all'),
                   ('c24_transversion_pred_all', 'c24_transition_real_all'),
                   ('c24_transition_pred_all', 'c24_transversion_real_all'),
                   ]

res_all = pd.DataFrame(columns=accessions, index=['all']+['AT_'+'CG', 'AT_'+'GC', 'AT_'+ 'TA', 'CG_'+ 'GC', 'CG_'+ 'TA', 'GC_'+ 'TA'])
for real, pred, acc in corr_pairs_all:
    dic_a = dr.load_dic(fs.root+tags[real])
    dic_b = dr.load_dic(fs.root+tags[pred])
    reg_names = [rg+'-'+str(i) for rg in ['ds', 'gbl', 'gbr', 'us'] for i in range(int(len(dic_a)/4))]
    corr = prep.weighted_pearson([dic_a[k] for k in reg_names], [dic_b[k] for k in reg_names])
    res_all.loc['all'][acc] = corr

for real, pred, acc in corr_pairs_convs:
    dic_a = dr.load_dic(fs.root+tags[real])
    dic_b = dr.load_dic(fs.root+tags[pred])
    print(real, pred)
    for conv in dic_a.keys():
        print(conv, acc)
        a = dic_a[conv]
        b = dic_b[conv]
        reg_names = [rg+'-'+str(i) for rg in ['ds', 'gbl', 'gbr', 'us'] for i in range(int(len(a)/4))]
        corr = prep.weighted_pearson([a[k] for k in reg_names], [b[k] for k in reg_names])
        res_all.loc[conv[0]+'_'+conv[1]][acc] = corr

res_all.to_csv('pearsons.csv')

res_setups = pd.DataFrame(columns=['small_bin', 'NN_model', 'GC_exclude'], index=['c24'])
for real, pred in corr_pairs_setups:
    dic_a = dr.load_dic(fs.root+tags[real])
    dic_b = dr.load_dic(fs.root+tags[pred])
    reg_names = [rg+'-'+str(i) for rg in ['ds', 'gbl', 'gbr', 'us'] for i in range(int(len(dic_a)/4))]
    corr = prep.weighted_pearson([dic_a[k] for k in reg_names], [dic_b[k] for k in reg_names])
    setup = pred.split('_')[1]+'_'+pred.split('_')[2]
    res_setups.loc['c24'][setup] = corr

res_setups.to_csv('setups.csv')

res_trans = pd.DataFrame(columns=['transition', 'transversion'], index=['transition', 'transversion'])
for pred, real in corr_pair_trans:
    dic_a = dr.load_dic(fs.root+tags[real])
    dic_b = dr.load_dic(fs.root+tags[pred])
    reg_names = [rg+'-'+str(i) for rg in ['ds', 'gbl', 'gbr', 'us'] for i in range(int(len(dic_a)/4))]
    corr = prep.weighted_pearson([dic_a[k] for k in reg_names], [dic_b[k] for k in reg_names])
    trans_pred = pred.split('_')[1]
    trans_real = real.split('_')[1]
    #rows are real and columns are preds
    res_trans.loc[trans_real][trans_pred] = corr

res_trans.to_csv('trans.csv')
