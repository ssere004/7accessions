import pandas as pd
import matplotlib.pyplot as plt
import preprocess.data_reader as dr

def make_vectors(avg_dic):
    lsts = {'ds':[], 'gbl':[], 'gbr':[], 'us':[]}
    for i in range(int(len(avg_dic.keys())/4)):
        for reg in lsts.keys(): lsts[reg].append(avg_dic[reg+'-'+str(i)])
    return lsts['ds']+lsts['gbl'], lsts['gbr']+lsts['us']

#ftrs = ['H3K14ac', 'H3K23ac', 'H3K27ac', 'H3K27me1', 'H3K36ac', 'H3K36me3', 'H3K4me1', 'H3K4me2', 'H3K4me3', 'H3K56ac', 'H3K9ac', 'H3K9me1', 'H3K9me2', 'ATAC', 'CG', 'CHG', 'CHH']
def genic_feature_plot(feature):
    root = '/Users/salehsereshki/Desktop/ServerDownloadTemp/snp_prediction/avg_dics/features/gb_us_snpdiff/'
    low, high, all = 'genicreg_feature_averages_low5percent_gbus_snpdiff.pkl', 'genicreg_feature_averages_top5percent_gbus_snpdiff.pkl', 'genicreg_feature_averages.pkl'
    data = {'low': make_vectors(dr.load_dic(root+low)[feature]),
            'high': make_vectors(dr.load_dic(root+high)[feature]),
            'all': make_vectors(dr.load_dic(root+all)[feature])}
    fig, axs = plt.subplots(3, 2, figsize=(10, 5), sharey=True)
    for i, type in zip([0,1,2], ['low', 'high', 'all']):
        print(i, type)
        axs[i][0].plot(data[type][0], visible=True)
        axs[i][1].plot(data[type][1], visible=True)
        axs[i][0].set_title(type + ' - left gene side')
        axs[i][1].set_title(type + ' - right gene side')
    plt.tight_layout()
    plt.savefig(root+feature+'.png')



def feature_analysis_plot(coefficients, features, tag):
    plt.figure(figsize=(12, 8))
    plt.barh(features, coefficients)
    plt.xlabel('Coefficient Value')
    plt.ylabel('Feature Name')
    plt.title('Feature Importance from Linear Regression Coefficients')
    plt.gca().invert_yaxis()  # Display the most important feature at the top
    plt.savefig('./dump_files/feature_analysis_plots/feature_importance_{}.png'.format(tag), bbox_inches='tight')

