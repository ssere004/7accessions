import pandas as pd
import preprocess.preprocess as preprocess
import preprocess.data_reader as data_reader
import preprocess.configs as configs
import numpy as np
from argparse import Namespace
import evaluate as plotting
import preprocess.regression_preprocess as regprep
from scipy.stats import pearsonr, spearmanr


def get_reverse_coordinates(df, chr_len):
    res_df = pd.DataFrame({'chr': [], 'start': [], 'end': []})
    for chrom in chr_len.keys():
        chr_df = df[df.chr == chrom].sort_values('start', ascending=True)
        res_df = pd.concat([res_df, pd.DataFrame({'chr': chrom,
                                                  'start': pd.concat([pd.Series([0]), chr_df['end']]).reset_index(
                                                      drop=True),
                                                  'end': pd.concat(
                                                      [chr_df['start'], pd.Series([chr_len[chrom]])]).reset_index(
                                                      drop=True)})]
                           , axis=0)
    return res_df


def get_genes_snps_stat(args):
    cnfg = configs.col
    cnfg_snp = configs.c24
    is_accession_spec = args.is_accession_spec
    conversion_base = args.conversion_base
    conv = args.conv
    is_genic = args.is_genic
    assembly = data_reader.readfasta(cnfg['assembly'])
    sequences_df = preprocess.convert_assembely_to_onehot(cnfg['og'], assembly, from_file=True)
    if is_accession_spec:
        raw_snp_df = data_reader.read_syri(cnfg_snp['syri'], annot_type='SNP')
        raw_snp_df = raw_snp_df.rename(columns={'chr1': 'chr', 'start1': 'position'})
        raw_snp_df['position'] = raw_snp_df['position'].astype(int)
        raw_snp_df['value'] = 1
        if conversion_base:
            raw_snp_df = raw_snp_df[((raw_snp_df.seq1 == conv[0][0]) & (raw_snp_df.seq2 == conv[1][0])) | (
                        (raw_snp_df.seq1 == conv[0][1]) & (raw_snp_df.seq2 == conv[1][1]))]
        snp_seq = preprocess.make_seq_dic(sequences_df, raw_snp_df, from_file=False, fn='')
    else:
        raw_snp_df = data_reader.read_raw_mutation_bias_snps(configs.root + 'col-0/epi_data/raw_variants.txt')
        if conversion_base:
            raw_snp_df = raw_snp_df[((raw_snp_df.REF == conv[0][0]) & (raw_snp_df.ALT == conv[1][0])) | (
                        (raw_snp_df.REF == conv[0][1]) & (raw_snp_df.ALT == conv[1][1]))]
        snp_seq = preprocess.make_seq_dic(assembly, raw_snp_df, from_file=False, fn='')
    annot_df = data_reader.read_annot(cnfg['gene_annotation'])
    annot_df = annot_df[annot_df.type == 'gene']
    if not is_genic:
        annot_df = get_reverse_coordinates(annot_df, {chro: len(assembly[chro]) for chro in assembly.keys()})
    annot_df[['start', 'end']] = annot_df[['start', 'end']].astype(int)
    return annot_df.apply(
        lambda row: np.sum(snp_seq[row['chr']][row['start']: row['end']]) / (row['end'] - row['start']), axis=1)


def run_violin_plotting():
    args = Namespace()
    data = []
    titles = []
    for is_accession_spec in [True, False]:
        args.is_accession_spec = is_accession_spec
        for is_genic in [True, False]:
            args.is_genic = is_genic
            for conversion_base in [True, False]:
                args.conversion_base = conversion_base
                if conversion_base:
                    for conv in [('AT', 'CG'), ('AT', 'GC'), ('AT', 'TA'), ('CG', 'GC'), ('CG', 'TA'), ('GC', 'TA')]:
                        args.conv = conv
                        data.append(get_genes_snps_stat(args))
                        titles.append(conv)
                else:
                    args.conv = ''
                    data.append(get_genes_snps_stat(args))
                    titles.append('all')
            print('make a plot for this number of data: ', len(data))
            evaluate.violin_plot_snp_density(
                './plots/box_plot_isAccession-' + str(is_accession_spec) + '_isgenic-' + str(is_genic),
                data, titles,
                'isAccession-' + str(is_accession_spec) + '_isgenic-' + str(is_genic)
                , plot_type='box')
            data = []
            titles = []


def make_epimarks_geneplot(args):
    cnfg = configs.col
    cnfg_snp = preprocess.convert_og_to_cnfg(args.accession)
    rc = configs.rc
    raw_snp_df = preprocess.read_raw_snps(cnfg_snp['og'])
    annot_df = data_reader.read_annot(cnfg['gene_annotation'])
    bw_root = cnfg['bw_root']
    bw_seqs, bw_names = preprocess.read_bwseqs(bw_root, from_file=True)
    bw_dic = {bw_names[i]: bw_seqs[i] for i in range(len(bw_names))}
    assemebly = data_reader.readfasta(cnfg['assembly'])
    sequences_df = preprocess.convert_assembely_to_onehot(cnfg['og'], assemebly, from_file=True)
    meth_seq_dic = preprocess.get_meth_seq_dic(cnfg, rc, from_file=True)
    snp_seq = preprocess.make_seq_dic(sequences_df, raw_snp_df)
    # tracks_l, tracks_r, track_names = regprep.plot_genic_intervals_avg_epimark(annot_df, sequences_df, meth_seq_dic, bw_dic, snp_seq, plot_tag=args.accession+'_snps', specific_feature='MA_SNV_pct')
    # evaluate.plot_gene_body(tracks_l, tracks_r, track_names, args.accession+'_snp_all', max_value=-1)
    # for conv in [('AT', 'CG'), ('AT', 'GC'), ('AT', 'TA'), ('CG', 'GC'), ('CG', 'TA'), ('GC', 'TA')]:
    #     raw_snp_df = preprocess.read_raw_snps(cnfg_snp['og'])
    #     raw_snp_df = raw_snp_df[((raw_snp_df.REF == conv[0][0]) & (raw_snp_df.ALT == conv[1][0])) | ((raw_snp_df.REF == conv[0][1]) & (raw_snp_df.ALT == conv[1][1]))]
    #     snp_seq = preprocess.make_seq_dic(sequences_df, raw_snp_df)
    #     tracks_l, tracks_r, track_names = regprep.plot_genic_intervals_avg_epimark(annot_df, sequences_df, meth_seq_dic, bw_dic, snp_seq, plot_tag=args.accession+'_snp_'+conv[0]+'-'+conv[1], specific_feature='MA_SNV_pct')
    #     evaluate.plot_gene_body(tracks_l, tracks_r, track_names, args.accession+'_snp_'+conv[0]+'-'+conv[1], max_value=-1)
    for plot_tag in ['GCcontent', 'Methylation', 'HistoneMarks', 'Atac', 'all']:
        pt = args.accession + plot_tag
        regprep.plot_genic_intervals_avg_epimark(annot_df, sequences_df, meth_seq_dic, bw_dic, snp_seq, plot_tag=pt)
    annot_df['SNV_pct'] = annot_df.apply(
        lambda row: regprep.count_snps_interval_seq(row['chr'], row['start'], row['end'], snp_seq), axis=1)
    top5pct = annot_df[annot_df['SNV_pct'] >= annot_df['SNV_pct'].quantile(0.95)]
    for plot_tag in ['GCcontent', 'Methylation', 'HistoneMarks', 'Atac', 'all']:
        pt = args.accession + plot_tag + '_topfivepct'
        regprep.plot_genic_intervals_avg_epimark(top5pct, sequences_df, meth_seq_dic, bw_dic, snp_seq, plot_tag=pt)
    zeropct = annot_df[annot_df['SNV_pct'] == 0]
    for plot_tag in ['GCcontent', 'Methylation', 'HistoneMarks', 'Atac', 'all']:
        pt = args.accession + plot_tag + '_zeropct'
        regprep.plot_genic_intervals_avg_epimark(zeropct, sequences_df, meth_seq_dic, bw_dic, snp_seq, plot_tag=pt)


def feature_corr_analysis(args):
    cnfg = configs.col
    cnfg_snp = preprocess.convert_og_to_cnfg(args.accession)
    rc = configs.rc
    raw_snp_df = preprocess.read_raw_snps(cnfg_snp['og'])
    bw_root = cnfg['bw_root']
    bw_seqs, bw_names = preprocess.read_bwseqs(bw_root, from_file=True)
    bw_dic = {bw_names[i]: bw_seqs[i] for i in range(len(bw_names))}
    assemebly = data_reader.readfasta(cnfg['assembly'])
    sequences_df = preprocess.convert_assembely_to_onehot(cnfg['og'], assemebly, from_file=True)
    meth_seq_dic = preprocess.get_meth_seq_dic(cnfg, rc, from_file=True)
    snp_seq = preprocess.make_seq_dic(sequences_df, raw_snp_df)
    del snp_seq['chrc']
    del snp_seq['chrm']
    for i in range(len(bw_seqs)):
        if 'chrm' in bw_seqs[i].keys():
            del bw_seqs[i]['chrm']
        if 'chrc' in bw_seqs[i].keys():
            del bw_seqs[i]['chrc']
        print(preprocess.pearson_sec_dics(snp_seq, bw_seqs[i], 100), bw_names[i])
    for context in meth_seq_dic.keys():
        if 'chrm' in meth_seq_dic[context].keys():
            del meth_seq_dic[context]['chrm']
        if 'chrc' in meth_seq_dic[context].keys():
            del meth_seq_dic[context]['chrc']
        print(preprocess.pearson_sec_dics(snp_seq, meth_seq_dic[context], 100), context)
    gc_seq = {chro: sequences_df[chro][:, 1] + sequences_df[chro][:, 2] for chro in
              sequences_df.keys() - {'chrc', 'chrm'}}
    print(preprocess.pearson_sec_dics(snp_seq, gc_seq, 100), 'GC')


def corr_epimarks_snpseperated(root, epimark):
    corrs = {}
    for gene_side in ['r', 'l']:
        fn1 = 'gene_averages_c24all_topfivepct_' + epimark + '_' + gene_side + '.txt'
        fn2 = 'gene_averages_c24all_zeropct_' + epimark + '_' + gene_side + '.txt'
        arr1 = np.loadtxt(root + fn1)
        arr2 = np.loadtxt(root + fn2)
        corrs[gene_side], _ = pearsonr(arr1, arr2)
    return corrs['l'], corrs['r']


def report_corrs_epimark_bias(root):
    res = ([], [], [])
    epimarks = ['H3K4me3', 'H3K14ac', 'H3K4me1', 'H3K9me2', 'H3K23ac', 'H3K4me2', 'CHH_pct',
                'H3K9me1', 'H3K27me1', 'CG_pct', 'atac_pct', 'H3K56ac', 'GC_content_pct',
                'H3K36ac', 'H3K36me3', 'H3K9ac', 'H3K27ac', 'CHG_pct']
    for epimark in epimarks:
        corr_l, corr_r = corr_epimarks_snpseperated(root, epimark)
        res[0].append(epimark)
        res[1].append(corr_l)
        res[2].append(corr_r)
    return pd.DataFrame({'epimark': res[0], 'corr_l': res[1], 'corr_r': res[2]})


args = Namespace()
args.accession = 'c24'
make_epimarks_geneplot(args)

import preprocess.constants as cnstnts
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score


def genic_region_binary_pred(args):
    features_df = small_bin_feature_df(args.annot_df, bin_size=50, flanking_region=500, sample_size=200000)
    res_df = regprep.parallel_make_features_df(features_df, args.sequences_df, args.meth_seq_dic, args.bw_dic,
                                               args.snp_seq, num_cores=10)
    data = pd.concat(
        [res_df[res_df.MA_SNV_pct > 0], res_df[res_df.MA_SNV_pct == 0].sample(n=len(res_df[res_df.MA_SNV_pct > 0]))])
    data['label'] = data['MA_SNV_pct'].apply(lambda x: 1 if x != 0 else 0)
    X = data[cnstnts.feature_set]
    Y = np.asarray(data[['label']])[:, 0]
    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.2, random_state=42)
    clf = RandomForestClassifier(n_estimators=100, random_state=42)
    clf.fit(X_train, y_train)
    y_pred = clf.predict(X_test)
    acc = accuracy_score(y_test, y_pred)
    print("Accuracy:", acc)



