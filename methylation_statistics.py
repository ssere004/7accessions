import argparse
import pandas as pd
import numpy as np


parser = argparse.ArgumentParser()

parser.add_argument('-m', '--methylation_file', help='methylation file address', required=True)
parser.add_argument('-th', '--methylation_threshold', help='threshold for calling a cytosine methylated', required=False, default=0.5, type=float)
parser.add_argument('-o', '--output', help='result output file', required=False, default='methylation_file_stat.txt')

args = parser.parse_args()

methylations = pd.read_table(args.methylation_file, header=None)
methylations.columns = ['chr', 'position', 'strand', 'meth', 'unmeth', 'context', 'three']
methylations['coverage'] = methylations['meth'] + methylations['unmeth']
methylations = methylations.drop(['three'], axis=1)

num_chrs = len(methylations['chr'].unique())

stats = [methylations.coverage.mean(),
    methylations.coverage.std(),
    methylations.coverage.quantile(0.25),
    methylations.coverage.quantile(0.5),
    methylations.coverage.quantile(0.75)]

meth_size = len(methylations)
methylations = methylations[methylations.coverage > 3]
covered_size = len(methylations)

methylations['me_ratio'] = methylations.meth / (methylations.meth + methylations.unmeth)
methylations['status'] = np.where((methylations.meth / (methylations.meth + methylations.unmeth)) >= float(args.methylation_threshold), 1, 0)

meth_ratio = [methylations.me_ratio.mean()]
meth_status = [float(len(methylations[methylations.status > 0])) / covered_size]

for cntx in ['CG', 'CHG', 'CHH']:
    sub_meth = methylations[methylations.context == cntx]
    meth_ratio.append(sub_meth.me_ratio.mean())
    meth_status.append(float(len(sub_meth[sub_meth.status > 0])) / len(sub_meth))

f = open(args.output, "w")

f.write('number of chromosomes: %d\n' %num_chrs)
f.write('all cytosines: %d\n' %meth_size)
f.write('covered cytosines: %d\n' %covered_size)
f.write('General Stats (mean, std, 25percentile, median, 75percentile): (%f %f %f %f %f)\n' % (stats[0], stats[1], stats[2], stats[3], stats[4]))
f.write('methylation ratio: (All, CG, CHG, CHH): (%f %f %f %f)\n' % (meth_ratio[0], meth_ratio[1], meth_ratio[2], meth_ratio[3]))
f.write('methylation status: (All, CG, CHG, CHH): (%f %f %f %f)\n' % (meth_status[0], meth_status[1], meth_status[2], meth_status[3]))

f.close()



