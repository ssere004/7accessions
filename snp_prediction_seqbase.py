import pandas as pd
import tensorflow as tf
import preprocess.data_reader as data_reader
import preprocess.preprocess as preprocess
import preprocess.configs as configs
import process.process as process
from tensorflow.keras import callbacks
from sklearn.model_selection import train_test_split
from tensorflow.keras.layers import Activation,Dense
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Dropout, Flatten, Reshape
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix, f1_score, precision_score, recall_score
from tensorflow.keras.constraints import max_norm
from tensorflow.keras.losses import binary_crossentropy
import numpy as np
import argparse
from sklearn.ensemble import RandomForestClassifier

parser = argparse.ArgumentParser()

parser.add_argument('-acr', '--ref_accession_name', help='name of the accession', required=True)
parser.add_argument('-acs', '--snp_accession_name', help='name of accession with snps', required=True)
parser.add_argument('-cbp', '--control_basepair', help='base pair in the reference accession/ or \"all\"', required=True)
parser.add_argument('-tbp', '--test_basepair', help='base pair in the snp accession/ or \"all\"', required=True)
parser.add_argument('-ws', '--window_size', help='sequence window size', required=False, default=128)
parser.add_argument('-rb', '--region_base', help='region_base', required=False, default=True)
parser.add_argument('-gs', '--genome_scope', help='genic, non_genic, gw', required=False, default=False)
parser.add_argument('-ds', '--dataset_size', help='training and testing dataset size', required=False, default=100000)
parser.add_argument('-cls', '--classifier', help='classifier to use as prediction model', required=False, default='CNN')


#CUDA_VISIBLE_DEVICES=1 python snp_prediction_seqbase.py -acr col -acs c24 -cbp A -tbp C -ws 64 -ds 70000 -cls CNN -gs True -rb True
#CUDA_VISIBLE_DEVICES=1 python snp_prediction_seqbase.py -acr col -acs c24 -cbp A -tbp C -ws 64 -ds 70000 -cls CNN -gs False -rb True
#CUDA_VISIBLE_DEVICES=1 python snp_prediction_seqbase.py -acr col -acs c24 -cbp A -tbp C -ws 64 -ds 70000 -cls RF

args = parser.parse_args()

cnfg_ref = process.get_cnfg_by_og(args.ref_accession_name)
cnfg_snp = process.get_cnfg_by_og(args.snp_accession_name)
rc = configs.rc

control_bp = args.control_basepair
test_bp = args.test_basepair
dataset_size = int(args.dataset_size)
window_size = int(args.window_size)
classifier = args.classifier
region_base = args.region_base
gene_specific = args.gene_specific

# cnfg_ref = configs.col
# cnfg_snp = configs.c24
# rc = configs.rc
#
# control_bp = 'all'
# test_bp = 'all'
# window_size = 128
# dataset_size = 100000
# classifier = 'CNN'
# bw_address = configs.root + 'col-0/epi_data/pcsd/H3K27me3-SRX648274.bw'
# region_base = True
# gene_specific = True


tr_te_pct = 0.2
tr_val_pct = 0.1

#methylations = data_reader.read_methylations(cnfg_ref['methylation'], '', coverage_threshold=rc['ct'])
methylations_CG = data_reader.read_geo_tsv_file(cnfg_ref['methylation_tsv'], 'CG', coverage_threshold=rc['ct'])
methylations_CHG = data_reader.read_geo_tsv_file(cnfg_ref['methylation_tsv'], 'CHG', coverage_threshold=rc['ct'])
methylations_CHH = data_reader.read_geo_tsv_file(cnfg_ref['methylation_tsv'], 'CHH', coverage_threshold=rc['ct'])
assemebly = data_reader.readfasta(cnfg_ref['assembly'])
syri = data_reader.read_syri(cnfg_snp['syri'], annot_type='SNP')

sequences_df = preprocess.convert_assembely_to_onehot(cnfg_ref['og'], assemebly, from_file=True)
meth_seq_CG = preprocess.make_methseq_dic(cnfg_ref['og'] + '_CG_meth_seq.pkl', methylations_CG, assemebly, rc['ct'], from_file=True)
meth_seq_CHG = preprocess.make_methseq_dic(cnfg_ref['og'] + '_CHG_meth_seq.pkl', methylations_CHG, assemebly, rc['ct'], from_file=True)
meth_seq_CHH = preprocess.make_methseq_dic(cnfg_ref['og'] + '_CHH_meth_seq.pkl', methylations_CHH, assemebly, rc['ct'], from_file=True)
meth_seq_dic = {'CG': meth_seq_CG, 'CHG': meth_seq_CHG, 'CHH': meth_seq_CHH}
annot_df = data_reader.read_annot(cnfg_ref['gene_annotation'])
annot_seq = preprocess.make_annotseq_dic(annot_df, cnfg_ref['annot_types'], assemebly)
print('mean of the annot_seq chr1: ' + str(np.mean(annot_seq['chr1'])))

bw_name = 'all'
bw_root = cnfg_ref['bw_root']
bw_seqs, _ = preprocess.read_bwseqs(bw_root)

# repeat_annot = data_reader.read_annot(cnfg_ref['repeat_annotation'])
# repeat_annot['type'] = 'repeat'
# rep_seq = preprocess.make_annotseq_dic(cnfg_ref['og'], repeat_annot, ['repeat'], assemebly, strand_spec=False)

input_list = [sequences_df, meth_seq_CG, meth_seq_CHG, meth_seq_CHH] + bw_seqs
#input_list = [meth_seq_CG, meth_seq_CHG, meth_seq_CHH] + bw_seqs
#input_list = [sequences_df, meth_seq]

available_chrs = preprocess.get_available_chrs(input_list, syri)
input_list = preprocess.remove_extra_chrs(input_list, available_chrs)

if test_bp == 'all' or control_bp == 'all':
    syri_df = syri
else:
    com_control_bp, com_test_bp = preprocess.complement_mutation(control_bp, test_bp)
    syri_df = syri[((syri.seq1 == control_bp) & (syri.seq2 == test_bp)) | ((syri.seq1 == com_control_bp) & (syri.seq2 == com_test_bp))][['chr1', 'start1']]

syri_df = syri_df[syri_df['chr1'].isin(available_chrs)]
syri_df['start1'] = syri_df['start1'].astype(int) - 1
# targets_p --> {'chr':[p1, p2, ...]}
targets_p = process.convert_df_to_dic(syri_df)
sample_size = int(sum(len(lst) for lst in targets_p.values()))
#sample_size = int(cnfg_snp['min_snp_size'])

if sample_size > dataset_size:
    sample_size = int(dataset_size)

accs = []
for i in range(3):
    if region_base:
        syri_interval_df = preprocess.make_snp_reg_dataframe(syri_df, int(window_size/2))
        syri_reg_seq = preprocess.make_annotseq_dic(syri_interval_df, ['snp'], sequences_df, strand_spec=False)
        #make a dictionary of each chromosomes to position assigned 1 or zero
        targets_p, targets_n = process.get_reg_positions(syri_reg_seq)
        if 'gene' in gene_specific:
            targets_p = preprocess.gene_specify(targets_p, preprocess.make_annotseq_dic(annot_df[annot_df.type == 'gene'], ['gene'], sequences_df, strand_spec=False), reverse=gene_specific == 'non_genic')
            targets_n = preprocess.gene_specify(targets_n, preprocess.make_annotseq_dic(annot_df[annot_df.type == 'gene'], ['gene'], sequences_df, strand_spec=False), reverse=gene_specific == 'non_genic')
    else:
        spec_bps_1 = process.get_all_bps(sequences_df, control_bp)
        if control_bp != 'all':
            spec_bps_2 = process.get_all_bps(sequences_df, com_control_bp)
            spec_bps = process.combine_specs(spec_bps_1, spec_bps_2)
        else:
            spec_bps = spec_bps_1
        targets_n = process.subtract_bps(spec_bps, targets_p)

    sample_n = process.sample_from_chr_dic(targets_n, sample_size)
    sample_p = process.sample_from_chr_dic(targets_p, sample_size)

    #sample_n.to_csv('./dump_files/sample_n.csv', sample_n, sep='\t')
    #sample_n.to_csv('./dump_files/sample_p.csv', sample_p, sep='\t')

    chunk_num = 1
    x_tests = []
    y_tests = []

    if classifier == 'CNN':
        x_, _ = process.input_maker(input_list, sample_n[:1], sample_p[:1], window_size=window_size)
        with tf.device('/device:GPU:0'):
            W_maxnorm = 3
            model = Sequential()
            model.add(Conv2D(128, kernel_size=(1, 5), activation='relu', input_shape=(x_.shape[2], x_.shape[1], 1), padding='same', kernel_constraint=max_norm(W_maxnorm)))
            model.add(MaxPooling2D(pool_size=(1, 5), strides=(1, 3)))
            model.add(Conv2D(256, kernel_size=(1, 5), activation='relu', padding='same', kernel_constraint=max_norm(W_maxnorm)))
            model.add(MaxPooling2D(pool_size=(1, 5), strides=(1, 3)))
            model.add(Flatten())
            model.add(Dense(64, activation='relu'))
            model.add(Dropout(0.6))
            model.add(Dense(2))
            model.add(Activation('softmax'))
            myoptimizer = tf.keras.optimizers.Adam(learning_rate=1e-5)
            model.compile(loss=binary_crossentropy, optimizer=myoptimizer, metrics=['accuracy'])
        for i in range(chunk_num):
            ch_size_n = int(len(sample_n)/chunk_num)
            ch_size_p = int(len(sample_p)/chunk_num)
            X, Y = process.input_maker(input_list, sample_n[i * ch_size_n:min((i+1)*ch_size_n, len(sample_n))], sample_p[i * ch_size_p:min((i+1)*ch_size_p, len(sample_p))], window_size=window_size)
            X = np.swapaxes(X, 1, 2)
            Y = np.asarray(pd.cut(Y, bins=2, labels=[0, 1], right=False))
            b = np.zeros((Y.size, Y.max()+1))
            b[np.arange(Y.size), Y] = 1
            Y = b
            print('Whole sample size is %d x2, Training is started for the chunk number %d, from %d x2 to %d x2 X.shape %s' %(len(sample_n), i, i * ch_size_n, min((i+1)*ch_size_n, len(sample_n)), str(X.shape)))
            x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=tr_te_pct, random_state=None)
            x_train, x_val, y_train, y_val = train_test_split(x_train, y_train, test_size=tr_val_pct, random_state=None)
            x_tests.append(x_test)
            y_tests.append(y_test)
            with tf.device('/device:GPU:0'):
                #print('model fitting started for %s and %s    %s --> %s' % (cnfg_ref['og'], cnfg_snp['og'], control_bp, test_bp))
                model.fit(x_train,  y_train, batch_size=128, epochs=30, verbose=1, validation_data=(x_val, y_val))
            x_test = np.concatenate(x_tests)
            y_preds = []
            for xx in x_tests:
                y_preds.append(model.predict(xx))
            y_pred = np.concatenate(y_preds)
            y_test = np.concatenate(y_tests)
    elif classifier == 'RF':
        X, Y = process.input_maker(input_list, sample_n, sample_p, window_size=window_size)
        X = X[:, :window_size, :, :]
        print('X.shape: ', X.shape, 'np.mean(X): ', np.mean(X))
        x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=tr_te_pct, random_state=None)
        x_train, x_val, y_train, y_val = train_test_split(x_train, y_train, test_size=tr_val_pct, random_state=None)
        with tf.device('/device:GPU:0'):
            model = RandomForestClassifier(random_state=0, n_estimators=50, warm_start=True, n_jobs=-1)

        nsamples, nx, ny, nz = x_train.shape
        xx = x_train.reshape((nsamples, nx*ny))
        model.fit(xx, y_train)
        nsamples, nx, ny, nz = x_test.shape
        xx = x_test.reshape((nsamples, nx*ny))
        y_pred = model.predict(xx)
    accs.append(accuracy_score(y_test, y_pred.round()))

i = 0

if control_bp != "all" or test_bp != "all":
    conversion = control_bp + ':' + com_control_bp+ '->' + test_bp + ':' + com_test_bp
else:
    conversion = 'all->all'
with open("snp_prediction_results.txt", "a") as file_object:
   file_object.write('%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s' %(cnfg_ref['og'], cnfg_snp['og'], conversion, np.mean(accs), np.std(accs), str((i+1)*len(x_train)), str(window_size), bw_name))
   file_object.write("\n")
acc_txt = str(window_size) + ',' + cnfg_snp['og'] + ','
for ac in accs:
    acc_txt += str(ac)
    acc_txt += ","
with open("snp_prediction_results_window_size.txt", "a") as file_object:
    file_object.write(acc_txt)
    file_object.write("\n")
print(accs)
print(cnfg_ref['og'], cnfg_snp['og'], conversion, np.mean(accs), np.std(accs), str((i+1)*len(x_train)), str(window_size), bw_name)



# targets_p = gene_specify(targets_p, preprocess.make_annotseq_dic(annot_df[annot_df.type == 'gene'], ['gene'], sequences_df, strand_spec=False), reverse=True)
# targets_n = gene_specify(targets_n, preprocess.make_annotseq_dic(annot_df[annot_df.type == 'gene'], ['gene'], sequences_df, strand_spec=False), reverse=True)






