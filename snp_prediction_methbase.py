import tensorflow as tf
import preprocess.data_reader as data_reader
import preprocess.preprocess as preprocess
import preprocess.configs as configs
import process.process as process
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.ensemble import RandomForestClassifier
import argparse

FROM_FILE = True

parser = argparse.ArgumentParser()

parser.add_argument('-acr', '--ref_accession_name', help='name of the accession', required=True)
parser.add_argument('-acs', '--snp_accession_name', help='name of accession with snps', required=True)
parser.add_argument('-cbp', '--control_basepair', help='base pair in the reference accession', required=True)
parser.add_argument('-tbp', '--test_basepair', help='base pair in the snp accession', required=True)
parser.add_argument('-ws', '--window_size', help='methylation window size', required=False, default=40)
parser.add_argument('-ds', '--dataset_size', help='training and testing dataset size', required=False, default=100000)

#CUDA_VISIBLE_DEVICES=1 python snp_prediction_seqbase.py -acr col -acs c24 -cbp C -tbp T

args = parser.parse_args()

cnfg_ref = process.get_cnfg_by_og(args.ref_accession_name)
cnfg_snp = process.get_cnfg_by_og(args.snp_accession_name)
rc = configs.rc


control_bp = args.control_basepair
test_bp = args.test_basepair

meth_window_size = int(args.window_size)
dataset_size = int(args.dataset_size)

# cnfg_ref = configs.col
# cnfg_snp = configs.c24
# rc = configs.rc
# meth_window_size = 40
#
# control_bp = 'C'
# test_bp = 'G'
# dataset_size = 10000

tr_te_pct = 0.2
tr_val_pct = 0.1

methylations_ref = None
methylations_snp = None

if not FROM_FILE:
    methylations_ref = data_reader.read_methylations(cnfg_ref['methylation'], '', coverage_threshold=rc['ct'])
    #methylations_snp = data_reader.read_methylations(cnfg_snp['methylation'], '', coverage_threshold=rc['ct'])


assemebly_ref = data_reader.readfasta(cnfg_ref['assembly'])
#assemebly_snp = data_reader.readfasta(cnfg_snp['assembly'])
syri = data_reader.read_syri(cnfg_snp['syri'], annot_type='SNP')
#syri = pd.read_csv(configs.root+'ler-0/7213_final_snps.csv', sep = '\t')

sequences_df = preprocess.convert_assembely_to_onehot(cnfg_ref['og'], assemebly_ref, from_file=True)

meth_seq1 = preprocess.make_methseq_dic(cnfg_ref['og'], methylations_ref, assemebly_ref, rc['ct'], from_file=FROM_FILE)
#meth_seq2 = preprocess.make_methseq_dic(cnfg_snp['og'], methylations_snp, assemebly_snp, rc['ct'], from_file=FROM_FILE)
#annot_seq = preprocess.make_annotseq_dic(cnfg_ref['og'], data_reader.read_annot(cnfg_ref['gene_annotation']), cnfg_ref['annot_types'], assemebly)

if test_bp == 'all' or control_bp == 'all':
    syri_df = syri
else:
    com_control_bp, com_test_bp = preprocess.complement_mutation(control_bp, test_bp)
    syri_df = syri[((syri.seq1 == control_bp) & (syri.seq2 == test_bp)) | ((syri.seq1 == com_control_bp) & (syri.seq2 == com_test_bp))][['chr1', 'start1']]

syri_df['start1'] = syri_df['start1'].astype(int) - 1
targets_p = process.convert_df_to_dic(syri_df)
#sample_size = int(cnfg_snp['min_snp_size'])
sample_size = sum(len(lst) for lst in targets_p.values())
if sample_size > dataset_size:
    sample_size = int(dataset_size)


secure_max_sample_size = 200000

spec_bps = process.get_all_bps(sequences_df, control_bp)
targets_n = process.subtract_bps(spec_bps, targets_p)

sample_n = process.sample_from_chr_dic(targets_n, secure_max_sample_size)
sample_p = process.sample_from_chr_dic(targets_p, secure_max_sample_size)

sample_p, sample_n = process.make_equal_size_dfs(sample_p, sample_n)

#sample_n.to_csv('./dump_files/sample_n.csv', sample_n, sep='\t')
#sample_n.to_csv('./dump_files/sample_p.csv', sample_p, sep='\t')

X, Y = process.input_maker_methbase([meth_seq1], sample_n, sample_p, window_size=meth_window_size)

x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=tr_te_pct, random_state=None)
x_train, x_val, y_train, y_val = train_test_split(x_train, y_train, test_size=tr_val_pct, random_state=None)

with tf.device('/device:GPU:0'):
    model = RandomForestClassifier(random_state=0, n_estimators=50, warm_start=True, n_jobs=-1)

nsamples, nx, ny, nz = x_train.shape
xx = x_train.reshape((nsamples, nx*ny))

model.fit(xx, y_train)

nsamples, nx, ny, nz = x_test.shape
xx = x_test.reshape((nsamples, nx*ny))

y_pred = model.predict(xx)

if control_bp != "all" or test_bp != "all":
    conversion = control_bp + ':' + com_control_bp + '->' + test_bp + ':' + com_test_bp
else:
    conversion = 'all->all'

with open("snp_prediction_methbase_results.txt", "a") as file_object:
    file_object.write('%s\t%s\t%s\t%s\t%s\t%s' %(cnfg_ref['og'], cnfg_snp['og'], conversion, accuracy_score(y_pred, y_test), str(len(x_train)), str(meth_window_size)))
    file_object.write("\n")

print(cnfg_ref['og'], cnfg_snp['og'], conversion, accuracy_score(y_test, y_pred.round()), len(x_train), str(meth_window_size))
