import pandas as pd
import numpy as np
import sys
import os
project_root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(project_root)
from preprocess import data_reader
from preprocess import preprocess
from sklearn.linear_model import LinearRegression
import preprocess.configs as configs
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Dropout, Flatten, Reshape, Dense
import numpy as np
from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error
from scipy.stats import pearsonr, spearmanr
#from statsmodels.tools.eval_measures import rmse
#import statsmodels.api as sm
import preprocess.regression_preprocess as regprep
from argparse import Namespace
import pandas as pd
import process
import preprocess.constants as cnstnts
import argparse

args = Namespace()
args.accession = 'mutation_bias'
args.scope = 'non-genic'


parser = argparse.ArgumentParser()
parser.add_argument('-acc', '--accession', help='name of the accession', required=True)
parser.add_argument('-scp', '--scope', help='scope', required=True)
args = parser.parse_args()

# For each chromosome returs the indexes where there is a snp and where there is not a snp inside the specified scope
def snp_indx_finder(snp_seq, annot_seq, scope='genic'):
    pos_dic = {} #dictionary containing the location of snps presented in scope for each chr
    neg_dic = {} #dictionary containing the location of snps presented in scope for each chr
    for chr in snp_seq.keys():
        if scope == 'genic':
            condition_annot = (annot_seq[chr] == 1)
        elif scope == 'non-genic':
            condition_annot = (annot_seq[chr] == 0)
        else:
            condition_annot = (True)
        pos_dic[chr] = np.where((snp_seq[chr] == 1) & condition_annot)[0]
        neg_dic[chr] = np.where((snp_seq[chr] == 0) & condition_annot)[0]
    return pos_dic, neg_dic #dictionary of numpy arrays of shape(n,)


def convert_dic_to_df(seq_dic):
    total_elements = sum(len(positions) for positions in seq_dic.values())
    chr_values = np.empty(total_elements, dtype=object)
    position_values = np.empty(total_elements, dtype=int)
    current_index = 0
    for chr_name, positions in seq_dic.items():
        num_positions = len(positions)
        chr_values[current_index: current_index + num_positions] = chr_name
        position_values[current_index: current_index + num_positions] = positions
        current_index += num_positions
    df = pd.DataFrame({'chr': chr_values, 'position': position_values})
    return df

def prepare_data(row, seq_dics, window_size=10):
    chr = row['chr']
    position = row['position']
    return [np.mean(seq_dics[key][chr][position - window_size: position+window_size]) for key in seq_dics.keys()]

cnfg = configs.col
cnfg_snp = preprocess.convert_og_to_cnfg(args.accession)
rc = configs.rc

sequences_df = preprocess.convert_assembely_to_onehot(cnfg['og'], data_reader.readfasta(cnfg['assembly']), from_file=True)
raw_snp_df = preprocess.read_raw_snps(cnfg_snp['og'])
snp_seq = preprocess.make_seq_dic(sequences_df, raw_snp_df)

bw_seqs, bw_names = preprocess.read_bwseqs(cnfg['bw_root'], from_file=True)
bw_dic = {bw_names[i]: {chro: bw_seqs[i][chro][:, 0] for chro in bw_seqs[i].keys()} for i in range(len(bw_names))}
meth_seq_dic = preprocess.get_meth_seq_dic(cnfg, rc, from_file=True)

annot_df = data_reader.read_annot(cnfg['gene_annotation'])
annot_seq = preprocess.make_interval_seq_dic(sequences_df, annot_df)

pos_dic, neg_dic = snp_indx_finder(snp_seq, annot_seq, scope=args.scope)
print("number of SNPs at the {}".format(args.scope),sum([len(pos_dic[key]) for key in pos_dic.keys()]))

#sample from neg_dic to make equal size of positive and negative smaples in the input
neg_dic = {chro: np.random.choice(neg_dic[chro], size=len(pos_dic[chro]), replace=False) for chro in neg_dic.keys()}

pos_df, neg_df = convert_dic_to_df(pos_dic), convert_dic_to_df(neg_dic)
pos_df['label'] = 1
neg_df['label'] = 0

res_df = pd.concat([pos_df, neg_df], axis=0).sample(frac=1)

res_df['data'] = res_df.apply(lambda row:prepare_data(row, {**bw_dic, **meth_seq_dic}), axis=1)

res_df = res_df[['data', 'label']]

df = pd.DataFrame(res_df)
def reshape_data(row):
    return pd.Series(row['data'] + [row['label']])

res_df = df.apply(reshape_data, axis=1)
res_df.columns = [f'data_{i}' for i in range(len(df.iloc[0]['data']))] + ['label']

X = res_df.drop(['label'], axis=1)
Y = res_df['label']

from sklearn.model_selection import train_test_split

X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, random_state=42)

from sklearn.linear_model import LogisticRegression

classifier = LogisticRegression(random_state=42)
classifier.fit(X_train, Y_train)

from sklearn.metrics import accuracy_score

Y_pred = classifier.predict(X_test)
accuracy = accuracy_score(Y_test, Y_pred)
print(f"Accuracy: {accuracy:.2f}")



