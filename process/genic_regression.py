import sys
import os

project_root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(project_root)
from preprocess import data_reader
from preprocess import preprocess
from sklearn.linear_model import LinearRegression
import preprocess.configs as configs
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Dropout, Flatten, Reshape, Dense
import numpy as np
from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error
from scipy.stats import pearsonr, spearmanr
#from statsmodels.tools.eval_measures import rmse
#import statsmodels.api as sm
import preprocess.regression_preprocess as regprep
from argparse import Namespace
import pandas as pd
import process
import preprocess.constants as cnstnts

# accession = 'mutation_bias'
# args = Namespace()
# args.accession = accession
# args.snp_cntx_base = False


epi_files = {
    'H3K14ac': [],
    'H3K23ac': [],
    'H3K27ac': [],
    'H3K27me1': [],
    'H3K36ac': [],
    'H3K36me3': [],
    'H3K4me1': [],
    'H3K4me2': [],
    'H3K4me3': [],
    'H3K56ac': [],
    'H3K9ac': [],
    'H3K9me1': [],
    'H3K9me2': [],
    'ATAC': []
}
# root = '/home/csgrads/ssere004/Organisms/7accessions/col-0/epi_data/grouped/'
# for foldername in epi_files.keys():
#     fns = os.listdir(root + foldername + '/')
#     ab_fns = [root + foldername + '/' + fn for fn in fns]
#     epi_files[foldername] = ab_fns



# for epi_file in epi_files.keys():
#     if len(epi_files[epi_file]) > 1:
#         print('merging ', epi_files[epi_file])
#         preprocess.combine_bigWig_files(epi_files[epi_file], root + 'merged/' + epi_file+'_merged.bw')


#This function is Depricated
def find_correlation_between_features(cnfg, rc):
    features_df = data_reader.read_features_data(cnfg['ff_address'])
    raw_snp_df = preprocess.read_raw_snps(configs.mutation_bias['og'])
    res_df = regprep.prepare_dfs(cnfg, rc, features_df, raw_snp_df)
    res_df.to_csv('dump_files/my_feature_df.tsv', sep=',', index=False)
    features_df.to_csv('dump_files/feature_df.tsv', sep=',', index=False)
    for col in res_df.columns:
        if col not in['chr', 'start', 'stop']:
            corr, _ = pearsonr(features_df[col], res_df[col])
            print(col, corr)



def model_feature_analysis(model, tag):
    feature_set = cnstnts.feature_set
    feature_importance = model.coef_[0]
    feature_importance_dict = {feature_set[i]: feature_importance[i] for i in range(len(feature_set))}
    sorted_features = sorted(feature_importance_dict.items(), key=lambda item: np.abs(item[1]), reverse=True)
    for feature, importance in sorted_features:
        print(f"Feature: {feature}, Coefficient: {importance}")
    features = [item[0] for item in sorted_features]
    coefficients = [item[1] for item in sorted_features]
    import evaluate.plotting as plttng
    plttng.feature_analysis_plot(coefficients, features, tag)


def NN_model(features_df):
    features_df = features_df.sample(frac=1)
    target = 'MA_SNV_pct'
    X = features_df[cnstnts.feature_set]
    Y = np.asarray(features_df[[target]])[:, 0]
    model = Sequential()
    model.add(Dense(16, input_dim=X.shape[1],  activation='relu'))
    model.add(Dense(8, activation='relu'))
    model.add(Dense(1))
    model.compile(loss='mean_squared_error', optimizer='adam')
    model.fit(X, Y, epochs=5, verbose=0)
    y_pred = model.predict(X)
    return model, model_evaluation(Y, y_pred[:, 0], X.shape)

def model_evaluation(y, y_pred, X_shape):
    res_dic = {}
    res_dic['pearson_corr'], res_dic['pearson_p_value'] = pearsonr(y, y_pred)
    res_dic['spearman_corr'], res_dic['spearman_p_value'] = spearmanr(y, y_pred)
    res_dic['mse'] = mean_squared_error(y, y_pred)
    r2 = r2_score(y, y_pred)
    res_dic['r2'] = r2
    res_dic['mae'] = mean_absolute_error(y, y_pred)
    #rmse = rmse(y, y_pred)
    res_dic['adj_r2'] = 1 - ((1 - r2) * (len(y) - 1)) / (len(y) - X_shape[1] - 1)
    llf = -0.5 * np.sum((y - y_pred) ** 2)
    k = X_shape[1]
    n = len(y)
    res_dic['aic'] = -2 * llf + 2 * k
    res_dic['bic'] = -2 * llf + k * np.log(n)
    return res_dic

def small_bin_feature_df(annot_df, bin_size=20, flanking_region=500, sample_size=500000):
    reg_names = [rg+'-'+str(i) for i in range(int(flanking_region/bin_size)) for rg in ['ds', 'gbl', 'gbr', 'us']]
    annot_df = annot_df[annot_df.type == 'gene']
    general_intervals = annot_df.apply(lambda row: regprep.get_gene_intervals(row, bin_size=bin_size, flanking_region=flanking_region), axis=1)
    sub_dfs = []
    for i, reg in enumerate(reg_names):
        sub_dfs.append(pd.DataFrame({'chr': general_intervals['chr'], 'start':general_intervals[reg],
                                                  'stop':general_intervals[reg]+bin_size, 'length':bin_size}))
    res_df = pd.concat(sub_dfs).dropna().reset_index(drop=True)
    return res_df.sample(n=sample_size)

# For an accession it makes the averaged snp probability plots for different conversions.
def make_gene_bias_plot_conversion_spec(args):
    cnfg = configs.col
    cnfg_snp = preprocess.convert_og_to_cnfg(args.accession)
    rc = configs.rc
    features_df = data_reader.read_features_data(cnfg['ff_address'])
    annot_df = data_reader.read_annot(cnfg['gene_annotation'])
    bw_root = cnfg['bw_root']
    bw_seqs, bw_names = preprocess.read_bwseqs(bw_root, from_file=True)
    bw_dic = {bw_names[i]: bw_seqs[i] for i in range(len(bw_names))}
    sequences_df = preprocess.convert_assembely_to_onehot(cnfg['og'], data_reader.readfasta(cnfg['assembly']), from_file=True)
    meth_seq_dic = preprocess.get_meth_seq_dic(cnfg, rc, from_file=True)
    avg_dics = {}
    raw_snp_df = preprocess.read_raw_snps(cnfg_snp['og'])
    #min_num_conversions = preprocess.find_min_conversion_num(raw_snp_df)
    for conv in [('AT', 'CG'), ('AT', 'GC'), ('AT', 'TA'), ('CG', 'GC'), ('CG', 'TA'), ('GC', 'TA')]:
        #conv = ('AT', 'CG')
        raw_snp_sub_df = raw_snp_df[((raw_snp_df.REF == conv[0][0]) & (raw_snp_df.ALT == conv[1][0])) |
                                    ((raw_snp_df.REF == conv[0][1]) & (raw_snp_df.ALT == conv[1][1]))]
        snp_seq = preprocess.make_seq_dic(sequences_df, raw_snp_sub_df)
        if args.snp_cntx_base:
            res_df = regprep.make_feature_df(features_df, sequences_df, meth_seq_dic, bw_dic, snp_seq, snp_cntx=[conv[0][0], conv[0][1]])
        else:
            #res_df = regprep.make_feature_df_parallel(features_df, sequences_df, meth_seq_dic, bw_dic, snp_seq, num_cores=20)
            res_df = regprep.parallel_make_features_df(features_df, sequences_df, meth_seq_dic, bw_dic, snp_seq, num_cores=20)
            #res_df_ = regprep.make_feature_df(features_df, sequences_df, meth_seq_dic, bw_dic, snp_seq)
        res_df = res_df.dropna(subset=['MA_SNV_pct'])
        model, _, mse = process.linear_model(res_df)
        if args.feature_plotting:
            model_feature_analysis(model,'{}_{}--{}'.format(args.accession, conv[0], conv[1]))
            continue
        print(conv, len(raw_snp_sub_df))
        regprep.save_genic_region_feature_avgs(annot_df, sequences_df, meth_seq_dic, bw_dic, bin_size=10, flanking_region=2000)
        avg_dics[conv] = regprep.get_snp_reg_prediction(model, bin_size=10, flanking_region=2000, avgs=True, annot_df=annot_df)
    if args.feature_plotting: return
    values = [val for dict in avg_dics for val in avg_dics[dict].values()]
    max_val = max(values)
    data_reader.save_dic('./dump_files/avg_dics_plts/avg_dics_convbase_' + args.accession +'_snp_cntx_base:'+str(args.snp_cntx_base) +'.pkl', avg_dics)
    # for key in avg_dics.keys():
    #     print(min_num_conversions)
    #     evaluate.plot_genic_intervals_avg_predictions(avg_dics[key], max_val, plot_tag=args.accession+'--'+str(key))

def make_gene_bias_plot_accession_spec(args):
    cnfg = configs.col
    cnfg_snp = preprocess.convert_og_to_cnfg(args.accession)
    rc = configs.rc
    sequences_df = preprocess.convert_assembely_to_onehot(cnfg['og'], data_reader.readfasta(cnfg['assembly']), from_file=True)
    features_df = data_reader.read_features_data(cnfg['ff_address'])
    raw_snp_df = preprocess.read_raw_snps(cnfg_snp['og'])
    if args.trans == 'transition':
        raw_snp_df = regprep.subset_snp_df(raw_snp_df, [('AT', 'GC'), ('CG', 'TA')])
    elif args.trans == 'transversion':
        raw_snp_df = regprep.subset_snp_df(raw_snp_df, [('AT', 'CG'), ('AT', 'TA'), ('CG', 'GC'), ('GC', 'TA')])
    print('{}, size of convs{}'.format(args.trans, len(raw_snp_df)))
    snp_seq = preprocess.make_seq_dic(sequences_df, raw_snp_df)
    bw_seqs, bw_names = preprocess.read_bwseqs(cnfg['bw_root'], from_file=True)
    bw_dic = {bw_names[i]: bw_seqs[i] for i in range(len(bw_names))}
    meth_seq_dic = preprocess.get_meth_seq_dic(cnfg, rc, from_file=True)
    res_df = regprep.parallel_make_features_df(features_df, sequences_df, meth_seq_dic, bw_dic, snp_seq, num_cores=10)
    model, _, mse = process.linear_model(res_df)
    if args.feature_plotting:
        model_feature_analysis(model, '{}_all'.format(args.accession))
        return
    #model, evalu = NN_model(res_df)
    annot_df = data_reader.read_annot(cnfg['gene_annotation'])
    regprep.save_genic_region_feature_avgs(annot_df, sequences_df, meth_seq_dic, bw_dic, bin_size=10, flanking_region=2000)
    avg_dic = regprep.get_snp_reg_prediction(model, bin_size=10, flanking_region=2000, avgs=True, annot_df=annot_df)
    data_reader.save_dic('./dump_files/avg_dics_plts/avg_dics_' + args.accession + '_all_trans:'+args.trans+ '.pkl', avg_dic)
    #evaluate.plot_genic_intervals_avg_predictions(avg_dic, max_val, plot_tag=str(cnfg_snp['og']))



def make_real_snp_plots_conv_base(args):
    cnfg_snp = preprocess.convert_og_to_cnfg(args.accession)
    cnfg = configs.col
    annot_df = data_reader.read_annot(cnfg['gene_annotation'])
    sequences_df = preprocess.convert_assembely_to_onehot(cnfg['og'], data_reader.readfasta(cnfg['assembly']), from_file=True)
    raw_snp_df = preprocess.read_raw_snps(cnfg_snp['og'])
    res = {}
    for conv in [('AT', 'CG'), ('AT', 'GC'), ('AT', 'TA'), ('CG', 'GC'), ('CG', 'TA'), ('GC', 'TA')]:
        raw_snp_sub_df = raw_snp_df[((raw_snp_df.REF == conv[0][0]) & (raw_snp_df.ALT == conv[1][0])) |
                                    ((raw_snp_df.REF == conv[0][1]) & (raw_snp_df.ALT == conv[1][1]))]
        snp_seq = preprocess.make_seq_dic(sequences_df, raw_snp_sub_df)
        if args.snp_cntx_base:
            res[conv] = regprep.snp_avg_cntxbase(annot_df, snp_seq, sequences_df, [conv[0][0], conv[0][1]], bin_size=10, flanking_region=2000)
        else:
            res[conv] = regprep.get_genic_avg_marks(annot_df, snp_seq)
        print('{} is done'.format(str(conv)))
    data_reader.save_dic('avg_dics_' + args.accession + '_snp_cntx_base_'+str(args.snp_cntx_base)+'_convbase_real.pkl', res)


def make_real_snp_plots_all(args):
    cnfg_snp = preprocess.convert_og_to_cnfg(args.accession)
    cnfg = configs.col
    annot_df = data_reader.read_annot(cnfg['gene_annotation'])
    sequences_df = preprocess.convert_assembely_to_onehot(cnfg['og'], data_reader.readfasta(cnfg['assembly']), from_file=True)
    raw_snp_df = preprocess.read_raw_snps(cnfg_snp['og'])
    if args.trans == 'transition':
        raw_snp_df = regprep.subset_snp_df(raw_snp_df, [('AT', 'GC'), ('CG', 'TA')])
    elif args.trans == 'transversion':
        raw_snp_df = regprep.subset_snp_df(raw_snp_df, [('AT', 'CG'), ('AT', 'TA'), ('CG', 'GC'), ('GC', 'TA')])
    print('{}, size of convs{}'.format(args.trans, len(raw_snp_df)))
    snp_seq = preprocess.make_seq_dic(sequences_df, raw_snp_df)
    data_reader.save_dic('avg_dics_'+ args.accession+ '_trans:'+args.trans+ '_all_real.pkl', regprep.get_genic_avg_marks(annot_df, snp_seq))





#args = Namespace()
#args.accession = 'c24'
#make_epimarks_geneplot(args)
accessions = ['c24', 'mutation_bias', 'g1001_high05', 'g1001_high1', 'g1001_low05', 'g1001_low01', 'ler', 'sha', 'cvi']

accessions = ['c24', 'mutation_bias']

#accessions = ['g1001_low01']

#accession = 'mutation_bias'
# args = Namespace()
# args.accession = accession
# args.flanking_region = 2000
# args.bin_size = 10
#args.trans = ''
# args.snp_cntx_base = False

for accession in accessions:
    # for trans in ['transition', 'transversion']:
    args = Namespace()
    args.accession = accession
    args.snp_cntx_base = True
    args.flanking_region = 2000
    args.bin_size = 10
    args.trans = ''
    args.feature_plotting = True
    make_real_snp_plots_conv_base(args)
    #make_real_snp_plots_all(args)

# accession = 'c24'
# args = Namespace()
# args.accession = accession
# args.snp_cntx_base = False
# args.flanking_region = 2000
# args.bin_size = 10
# args.trans = ''
# args.feature_plotting = True
# make_gene_bias_plot_conversion_spec(args)



# for i in range(10000):
#     p = random.randint(0,len(res_df))
#     chro = res.iloc[p]['chr']
#     start = res.iloc[p]['start']
#     row = res_df_[(res_df_['chr'] == chro) & (res_df_['start'] == start)].iloc[0]
#     if row['H3K14ac'] != res.iloc[p]['H3K14ac']:
#         print(p)

