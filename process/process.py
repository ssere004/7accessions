import pandas as pd
import numpy as np
from sklearn.utils import shuffle
import preprocess.configs as configs
from numpy import concatenate, sort
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error
from preprocess import constants as cnstnts
import random

#returns a dictionary that for each chromosome it contains the list of positions of bp.
def get_all_bps(sequences_df, bp):
    bp_onehot = {'A': 0, 'C': 1, 'G': 2, 'T': 3}
    res = {}
    if bp == 'all':
        for chr in sequences_df:
            res[chr] = np.asarray(range(len(sequences_df[chr])))
    else:
        for chr in sequences_df:
            bp_indxs = sequences_df[chr][:, bp_onehot[bp]]
            res[chr] = np.where(bp_indxs == 1)[0]
    return res

def combine_specs(spec_bps_1, spec_bps_2):
    res = {}
    for chr in spec_bps_1.keys():
        c = concatenate((spec_bps_1[chr],spec_bps_2[chr]))
        c.sort(kind='mergesort')
        res[chr] = c
    return res


def linear_model(features_df):
    target = 'MA_SNV_pct'
    X = features_df[cnstnts.feature_set]
    Y = features_df[[target]]
    model = LinearRegression()
    model.fit(X, Y)
    return model, model.score(X, Y), mean_squared_error(Y, model.predict(X))

def subtract_bps(spec_bps, targets_p):
    res = {}
    for chr in spec_bps.keys():
        if chr in targets_p.keys():
            res[chr.lower()] = np.asarray(list(set(spec_bps[chr]) - set(targets_p[chr])))
        else:
            res[chr.lower()] = spec_bps[chr]
    return res

#get a dictionary {chr:[p1, p2,..]} and a annotation seq dictionary, returns the positions which the annotation seq is 1(not zero)
def annot_base_filter(target_bps, annotation_seq, positive=True):
    annot_var = 0
    if positive:
        annot_var = 1
    res = {}
    for chr in target_bps.keys():
        res[chr.lower()] = np.asarray(list(set(target_bps[chr]) - set(np.where(annotation_seq[chr][:, 0] == annot_var)[0])))
    return res

#gets a list of positions for each chromosome and returns a dataframe sampled from those positions.
def sample_from_chr_dic(target_bps, sample_size):
    res = pd.DataFrame({'chr': [], 'position': []})
    res = res.astype({'position': 'int32'})
    for chr in target_bps.keys():
        positions = target_bps[chr].astype(np.int32)
        res = pd.concat([res,
                        pd.DataFrame({'chr': pd.Series(chr.lower(), index=range(positions.size)), 'position': positions})],
                       ignore_index=True)
    if sample_size > len(res):
        sample_size = len(res)
    return res.sample(n=sample_size, replace=False)

#gets a dataframe of (chr, position) pairs and converts it to a dictionary of {'chr':[p1, p2, ...]}
def convert_df_to_dic(df, key_column='chr1', value_column='start1'):
    res = {}
    if len(df[key_column].unique()) > 50:
        print('warning number of chrs in the syri df is too much %d' %len(df[key_column].unique()))
    for chr in df[key_column].unique():
        res[chr.lower()] = df[df[key_column] == chr][value_column].to_numpy(dtype=np.int32)
    return res

def get_reg_positions(syri_reg_seq):
    targets_p = {}
    targets_n = {}
    for chro in syri_reg_seq.keys():
        targets_n[chro] = np.where(syri_reg_seq[chro] == 0)[0]
        targets_p[chro] = np.where(syri_reg_seq[chro] == 1)[0]
    return targets_p, targets_n

#sample_p and sample_n are two dataframes which has ['chr', 'position'] columns
def input_maker(input_list, sample_p, sample_n, window_size=128):
    if len(input_list) == 0:
        raise ValueError('no input in the input list')
    for inp in input_list:
        if len(inp.keys()) == 0:
            raise ValueError('sequences dataframe does not contain sequences')
    chrs = list(input_list[0].keys())
    chr_tmp = chrs[0]
    for inp in input_list:
        if len(inp[chr_tmp].shape) != 2:
            raise ValueError('all inputs must have the same number of dimension 2')
    pad_size = 0
    if window_size < 20:
        pad_size = 20 - window_size
    x_col_size = 0
    for inp in input_list:
        x_col_size += inp[chr_tmp].shape[1]
    X = np.zeros((len(sample_n) + len(sample_p), window_size + pad_size, x_col_size))
    Y = np.zeros(len(sample_n) + len(sample_p))
    profiles = {}
    for chr in chrs:
        chr_inps = []
        len_ref_chr = len(input_list[0][chr])
        same_size_chrs = True
        for inp in input_list:
            chr_inps.append(inp[chr])
            if len(inp[chr]) != len_ref_chr:
                same_size_chrs = False
        if same_size_chrs:
            profiles[chr.lower()] = np.concatenate(chr_inps, axis=1)
    for i in range(len(sample_n)):
        row = sample_n.iloc[i]
        try:
            X[i][:window_size] = profiles[row['chr']][int(row['position'] - window_size/2): int(row['position'] + window_size/2)]
            X[i][window_size: pad_size + window_size] = -1
        except ValueError:
            print(i)
        Y[i] = 0
    for i in range(len(sample_n), len(sample_n) + len(sample_p)):
        row = sample_p.iloc[i-len(sample_n)]
        try:
            X[i][:window_size] = profiles[row['chr']][int(row['position'] - window_size/2): int(row['position'] + window_size/2)]
            X[i][window_size: pad_size + window_size] = -1
        except ValueError:
            print(i-len(sample_n))
        Y[i] = 1
    if len(Y[Y == 1]) > len(Y[Y==0]):
        X = X[:len(Y) - (len(Y[Y==1]) - len(Y[Y==0]))]
        Y = Y[:len(Y) - (len(Y[Y==1]) - len(Y[Y==0]))]
    elif len(Y[Y==1]) < len(Y[Y==0]):
        X = X[(len(Y[Y==0]) - len(Y[Y==1])):]
        Y = Y[(len(Y[Y==0]) - len(Y[Y==1])):]
    print('Ratio of snp vs non snp in the sample set is %f' %float(len(Y[Y==1]) / float(len(Y))))
    X, Y = shuffle(X, Y, random_state=0)
    return np.expand_dims(np.asarray(X), 3), np.asarray(Y)

def input_maker_contextbase(input_list, sample_list, window_size=128):
    if len(input_list) == 0:
        raise ValueError('no input in the input list')
    for inp in input_list:
        if len(inp.keys()) == 0:
            raise ValueError('sequences dataframe does not contain sequences')
    chrs = list(input_list[0].keys())
    chr_tmp = chrs[0]
    for inp in input_list:
        if len(inp[chr_tmp].shape) != 2:
            raise ValueError('all inputs must have the same number of dimension 2')
    pad_size = 0
    if window_size < 20:
        pad_size = 20 - window_size
    x_col_size = 0
    for inp in input_list:
        x_col_size += inp[chr_tmp].shape[1]
    X = np.zeros((sum([len(smple) for smple in sample_list]), window_size + pad_size, x_col_size))
    Y = np.zeros(sum([len(smple) for smple in sample_list]))
    profiles = {}
    for chr in chrs:
        chr_inps = []
        len_ref_chr = len(input_list[0][chr])
        same_size_chrs = True
        for inp in input_list:
            chr_inps.append(inp[chr])
            if len(inp[chr]) != len_ref_chr:
                same_size_chrs = False
        if same_size_chrs:
            profiles[chr.lower()] = np.concatenate(chr_inps, axis=1)
    label = 0
    last_i = 0
    for sample_n in sample_list:
        for i in range(last_i, last_i + len(sample_n)):
            row = sample_n.iloc[i - last_i]
            try:
                X[i][:window_size] = profiles[row['chr']][int(row['position'] - window_size/2): int(row['position'] + window_size/2)]
                # The bp in the center by itself can be detected by the classifier and then be used to predict the context
                X[i][:int(window_size/2)] = 0
                X[i][window_size: pad_size + window_size] = -1
            except ValueError:
                print(i - last_i)
            Y[i] = label
        last_i = i
        label += 1
    if len(Y[Y == 1]) > len(Y[Y==0]):
        X = X[:len(Y) - (len(Y[Y==1]) - len(Y[Y==0]))]
        Y = Y[:len(Y) - (len(Y[Y==1]) - len(Y[Y==0]))]
    elif len(Y[Y==1]) < len(Y[Y==0]):
        X = X[(len(Y[Y==0]) - len(Y[Y==1])):]
        Y = Y[(len(Y[Y==0]) - len(Y[Y==1])):]
    print('Each sample ratio is %f' %float(len(Y[Y==1]) / float(len(Y))))
    X, Y = shuffle(X, Y, random_state=0)
    return np.expand_dims(np.asarray(X), 3), np.asarray(Y)

def input_maker_methbase(input_list, sample_p, sample_n, window_size=40, max_seq_window_size=1600):
    max_seq_window_size = int(max_seq_window_size/2)
    window_size = int(window_size/2)
    if len(input_list) == 0:
        raise ValueError('no input in the input list')
    for inp in input_list:
        if len(inp.keys()) == 0:
            raise ValueError('sequences dataframe does not contain sequences')
    chrs = list(input_list[0].keys())
    chr_tmp = chrs[0]
    for inp in input_list:
        if len(inp[chr_tmp].shape) != 2:
            raise ValueError('all inputs must have the same number of dimension 2')
    x_col_size = 0
    for inp in input_list:
        x_col_size += inp[chr_tmp].shape[1]
    X = np.zeros((len(sample_n) + len(sample_p), window_size*2, x_col_size))
    Y = np.zeros(len(sample_n) + len(sample_p))
    profiles = {}
    for chr in chrs:
        chr_inps = []
        for inp in input_list:
            chr_inps.append(inp[chr])
        profiles[chr.lower()] = np.concatenate(chr_inps, axis=1)
    ignored_idxs = []
    for i in range(len(sample_n)):
        row = sample_n.iloc[i]
        chr = row['chr']
        p = row['position']
        if len(np.nonzero(profiles[chr][:, 0][p-max_seq_window_size:p])[0]) < window_size \
                or len(np.nonzero(profiles[chr][p: p+max_seq_window_size][:, 0])[0]) < window_size:
            ignored_idxs.append(i)
        else:
            pr_p = profiles[chr][p: p+max_seq_window_size][:, 0][np.nonzero(profiles[chr][p: p+max_seq_window_size][:, 0])[0][:window_size]]
            pr_n = np.flip(profiles[chr][:, 0][p-max_seq_window_size:p], 0)[np.nonzero(np.flip(profiles[chr][:, 0][p-max_seq_window_size:p], 0))[0]][:window_size]
            res = np.concatenate([pr_n, pr_p])
            X[i] = np.expand_dims(res, axis=1)
            Y[i] = 0
    for i in range(len(sample_n), len(sample_n) + len(sample_p)):
        row = sample_p.iloc[i-len(sample_n)]
        chr = row['chr']
        p = row['position']
        if len(np.nonzero(profiles[chr][:, 0][p-max_seq_window_size:p])[0]) < window_size \
                or len(np.nonzero(profiles[chr][p: p+max_seq_window_size][:, 0])[0]) < window_size:
            ignored_idxs.append(i)
        else:
            pr_p = profiles[chr][p: p+max_seq_window_size][:, 0][np.nonzero(profiles[chr][p: p+max_seq_window_size][:, 0])[0][:window_size]]
            pr_n = np.flip(profiles[chr][:, 0][p-max_seq_window_size:p], 0)[np.nonzero(np.flip(profiles[chr][:, 0][p-max_seq_window_size:p], 0))[0]][:window_size]
            res = np.concatenate([pr_n, pr_p])
            X[i] = np.expand_dims(res, axis=1)
            Y[i] = 1
    X = np.delete(X, ignored_idxs, axis=0)
    Y = np.delete(Y, ignored_idxs)
    if len(Y[Y==1]) > len(Y[Y==0]):
        X = X[:len(Y) - (len(Y[Y==1]) - len(Y[Y==0]))]
        Y = Y[:len(Y) - (len(Y[Y==1]) - len(Y[Y==0]))]
    elif len(Y[Y==1]) < len(Y[Y==0]):
        X = X[(len(Y[Y==0]) - len(Y[Y==1])):]
        Y = Y[(len(Y[Y==0]) - len(Y[Y==1])):]
    print('Ratio of snp vs non snp in the sample set is %f' %float(len(Y[Y==1]) / float(len(Y))))
    X, Y = shuffle(X, Y, random_state=0)
    return np.expand_dims(np.asarray(X), 3), np.asarray(Y)

def make_equal_size_dfs(df1, df2):
    if len(df1) >= len(df2):
        return df1[:len(df2)], df2
    else:
        return df1, df2[:len(df1)]

def get_cnfg_by_og(og):
    cnfg_list = [configs.col, configs.c24, configs.cvi, configs.ler, configs.sha]
    for cnfg in cnfg_list:
        if cnfg['og'] == og:
            return cnfg
    return None

def align_two_synthenies(df, assembly_ref, assembly_snp, meth_seq_ref, meth_seq_snp, window_size):
    syn_row = df[df.annot == 'SYN'].iloc[0]
    s1 = int(syn_row['start1'])
    s2 = int(syn_row['start2'])
    chr1 = syn_row['chr1']
    chr2 = syn_row['chr2']
    unique_annots = ['SNP', 'DEL', 'INS', 'CPL', 'HDR', 'TDM', 'CPG']
    sub_df = df[df.annot.isin(unique_annots)].sort_values('start1')
    if len(sub_df) == 0:
        seq1 = assembly_ref[chr1.lower()][s1:int(syn_row['end1'])]
        m_seq1 = meth_seq_ref[chr1.lower()][s1:int(syn_row['end1'])]
        seq2 = assembly_snp[chr2.lower()][s2:int(syn_row['end2'])]
        m_seq2 = meth_seq_snp[chr2.lower()][s2:int(syn_row['end2'])]
    else:
        seq1 = assembly_ref[chr1.lower()][s1:int(sub_df.iloc[0]['start1']) - 1]
        m_seq1 = meth_seq_ref[chr1.lower()][s1:int(sub_df.iloc[0]['start1']) - 1]
        seq2 = assembly_snp[chr2.lower()][s2:int(sub_df.iloc[0]['start2']) - 1]
        m_seq2 = meth_seq_snp[chr2.lower()][s2:int(sub_df.iloc[0]['start2']) - 1]
        seq1 += sub_df.iloc[0]['seq1']
        m_seq1 = np.concatenate([m_seq1, meth_seq_ref[chr1.lower()][int(sub_df.iloc[0]['start1']): int(sub_df.iloc[0]['start1']) + len(sub_df.iloc[0]['seq1'])]])
        seq2 += sub_df.iloc[0]['seq2']
        m_seq2 = np.concatenate([m_seq2, meth_seq_snp[chr2.lower()][int(sub_df.iloc[0]['start2']): int(sub_df.iloc[0]['start2']) + len(sub_df.iloc[0]['seq2'])]])
        if len(seq1) > len(seq2):
            dif_n = (len(seq1)-len(seq2))
            seq2 += 'X'*dif_n
            m_seq2 = np.concatenate([m_seq2, -1*np.ones((dif_n, 1))])
        elif len(seq2) > len(seq1):
            dif_n = (len(seq2)-len(seq1))
            seq1 += 'X'*dif_n
            m_seq1 = np.concatenate([m_seq1, -1*np.ones((dif_n, 1))])
        if len(seq1) != len(seq2):
            print('sequences with not equal lengths')
    for i in range(len(sub_df)-1):
        row = sub_df.iloc[i]
        row_ = sub_df.iloc[i+1]
        if (int(row_['start1']) - int(row['start1'])) - (int(row_['start2']) - int(row['start2'])) == len(row['seq1']) - len(row['seq2']):
            seq1 += assembly_ref[chr1.lower()][int(row['end1']):int(row_['start1']) - 1]
            m_seq1 = np.concatenate([m_seq1, meth_seq_ref[chr1.lower()][int(row['end1']): int(row_['start1']) - 1]])
            seq2 += assembly_snp[chr2.lower()][int(row['end2']):int(row_['start2']) - 1]
            m_seq2 = np.concatenate([m_seq2, meth_seq_snp[chr2.lower()][int(row['end2']): int(row_['start2']) - 1]])
            seq1 += row_['seq1']
            m_seq1 = np.concatenate([m_seq1, meth_seq_ref[chr1.lower()][int(row_['start1']): int(row_['start1']) + len(row_['seq1'])]])
            seq2 += row_['seq2']
            m_seq2 = np.concatenate([m_seq2, meth_seq_snp[chr2.lower()][int(row_['start2']): int(row_['start2']) + len(row_['seq2'])]])
            if len(seq1) > len(seq2):
                dif_n = (len(seq1)-len(seq2))
                seq2 += 'X'*dif_n
                m_seq2 = np.concatenate([m_seq2, -1*np.ones((dif_n, 1))])
            elif len(seq2) > len(seq1):
                dif_n = (len(seq2)-len(seq1))
                seq1 += 'X'*dif_n
                m_seq1 = np.concatenate([m_seq1, -1*np.ones((dif_n, 1))])
        else:
            seq1 += assembly_ref[chr1.lower()][int(row['end1']):int(row_['start1']) - 1]
            m_seq1 = np.concatenate([m_seq1, meth_seq_ref[chr1.lower()][int(row['end1']): int(row_['start1']) - 1]])
            seq2 += assembly_snp[chr2.lower()][int(row['end2']):int(row_['start2']) - 1]
            m_seq2 = np.concatenate([m_seq2, meth_seq_snp[chr2.lower()][int(row['end2']): int(row_['start2']) - 1]])
            if len(seq1) > len(seq2):
                dif_n = (len(seq1)-len(seq2))
                seq2 += 'X'* dif_n
                m_seq2 = np.concatenate([m_seq2, -1*np.ones((dif_n, 1))])
            elif len(seq2) > len(seq1):
                dif_n = (len(seq2)-len(seq1))
                seq1 += 'X'* dif_n
                m_seq1 = np.concatenate([m_seq1, -1*np.ones((dif_n, 1))])
        if len(seq1) != len(seq2) or m_seq1.shape != m_seq2.shape:
            print('process aborted for the synteny block with ID: ' + syn_row['ID'] + ' ' + str(i))
            break
    seq_df = pd.DataFrame({'seq1': list(seq1), 'seq2': list(seq2), 'idx': list(range(len(seq1)))})
    seq_size = len(seq_df)
    snp_lst = list(seq_df[(seq_df.seq1 != seq_df.seq2) & (seq_df.seq1 != 'X') & (seq_df.seq2 != 'X') & (seq_df.idx > window_size/2) & (seq_df.idx < seq_size - window_size/2)].idx)
    non_snp_lst = list(seq_df[(seq_df.seq1 == seq_df.seq2) & (seq_df.seq1 != 'X') & (seq_df.seq2 != 'X') & (seq_df.idx > window_size/2) & (seq_df.idx < seq_size - window_size/2)].idx)
    return seq1, seq2, m_seq1, m_seq2, snp_lst, non_snp_lst

def dictionary_sampling(snp_dic, sample_size):
    total_len = 0
    for v in snp_dic.values():
        total_len += len(v)
    dic_ar = np.zeros((total_len, 2))
    last_p = 0
    for key in snp_dic:
        dic_ar[last_p: last_p + len(snp_dic[key]), 0] = key
        dic_ar[last_p: last_p + len(snp_dic[key]), 1] = snp_dic[key]
        last_p += len(snp_dic[key])
    return dic_ar[np.random.choice(dic_ar.shape[0], sample_size, replace=False), :]


def make_sample(syri, assembly_ref, assembly_snp, meth_seq_ref, meth_seq_snp, window_size):
    seqs1, seqs2 = {}, {}
    m_seqs1, m_seqs2 = {}, {}
    snp_lsts, non_snp_lsts = {}, {}
    sid = 1
    syids = syri[syri.annot == 'SYN']['ID'].unique()
    try:
        for syn_ID in syids:
            df = syri[(syri.parent == syn_ID) | (syri.ID == syn_ID)]
            df = df.astype({'start1': int, 'start2': int, 'end1': int, 'end2': int})
            seq1, seq2, m_seq1, m_seq2, snp_lst, non_snp_lst = align_two_synthenies(df, assembly_ref, assembly_snp, meth_seq_ref, meth_seq_snp, window_size)
            seqs1[sid] = seq1
            seqs2[sid] = seq2
            if m_seq1.shape != m_seq2.shape:
                print('Error mseqs are not the same shape ' + str(syn_ID))
            m_seqs1[sid] = m_seq1
            m_seqs2[sid] = m_seq2
            snp_lsts[sid] = snp_lst
            non_snp_lsts[sid] = non_snp_lst
            sid += 1
            if sid % int(len(syids)/20) == 0:
                print(str(sid) + 'out of ' + str(len(syids)))
    except:
        print('Error in ' + str(syn_ID))
    return m_seqs1, m_seqs2, snp_lsts, non_snp_lsts

def make_input(m_seqs1, m_seqs2, snp_lsts, non_snp_lsts, sample_size, window_size):
    snp_sample = dictionary_sampling(snp_lsts, int(sample_size/2))
    non_snp_sample = dictionary_sampling(non_snp_lsts, int(sample_size/2))
    sample_size = 2*len(snp_sample)
    X = np.zeros((sample_size, window_size, 2))
    Y = np.zeros(sample_size)
    print('%s sampled from the SNPs and %s sampled frome non SNPs' %(str(len(snp_sample)), str(len(non_snp_sample))))
    if len(snp_sample) != len(non_snp_sample):
        print('WARNINNNNNNGG: Samples are not the same size')
    try:
        for i in range(len(snp_sample)):
            synid = snp_sample[i][0]
            p = int(snp_sample[i][1])
            X[i, :, 0] = m_seqs1[synid][p - int(window_size/2): p + int(window_size/2)][:, 0]
            X[i, :, 1] = m_seqs2[synid][p - int(window_size/2): p + int(window_size/2)][:, 0]
            Y[i] = 1
        for i in range(len(snp_sample), len(snp_sample)+len(non_snp_sample)):
            ii = i - len(snp_sample)
            synid = non_snp_sample[ii][0]
            p = int(non_snp_sample[ii][1])
            X[i, :, 0] = m_seqs1[synid][p - int(window_size/2): p + int(window_size/2)][:, 0]
            X[i, :, 1] = m_seqs2[synid][p - int(window_size/2): p + int(window_size/2)][:, 0]
            Y[i] = 0
    except:
        print(i, i)
    X, Y = shuffle(X, Y, random_state=0)
    return np.expand_dims(np.asarray(X), 3), np.asarray(Y)

def convert_avgs_dic_to_lists(avgs_dic):
    res_1 = []
    res_2 = []
    labels = []
    for key in avgs_dic.keys():
        i = 0
        lst1 =[]
        lst2 = []
        while 'ds-' + str(i) in avgs_dic[key].keys():
            lst1.append(avgs_dic[key]['ds-' + str(i)])
            i+=1
        i = 0
        while 'gbl-' + str(i) in avgs_dic[key].keys():
            lst1.append(avgs_dic[key]['gbl-' + str(i)])
            i+=1
        i = 0
        while 'gbr-' + str(i) in avgs_dic[key].keys():
            lst2.append(avgs_dic[key]['gbr-' + str(i)])
            i+=1
        i = 0
        while 'us-' + str(i) in avgs_dic[key].keys():
            lst2.append(avgs_dic[key]['us-' + str(i)])
            i+=1
        res_1.append(lst1)
        res_2.append(lst2)
        labels.append(key)
    return np.asarray(res_1), np.asarray(res_2), labels
