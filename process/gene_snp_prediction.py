import sys
import os

project_root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(project_root)
import pandas as pd
from preprocess import data_reader
from preprocess import preprocess
import preprocess.configs as configs
import preprocess.regression_preprocess as regrprep
from sklearn.model_selection import train_test_split
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Dropout, Flatten, Reshape, Dense
import numpy as np
import evaluate
from sklearn.linear_model import LinearRegression
from argparse import Namespace
from scipy.stats import pearsonr, boxcox
import statsmodels.api as sm
from preprocess import constants as cnstnts
from preprocess import regression_preprocess as regprep
import tensorflow as tf
from sklearn.ensemble import RandomForestClassifier



epi_files = {
    'H3K14ac': [],
    'H3K23ac': [],
    'H3K27ac': [],
    'H3K27me1': [],
    'H3K36ac': [],
    'H3K36me3': [],
    'H3K4me1': [],
    'H3K4me2': [],
    'H3K4me3': [],
    'H3K56ac': [],
    'H3K9ac': [],
    'H3K9me1': [],
    'H3K9me2': [],
    'ATAC': []
}


def get_fn(is_accession_spec, conversion_base, conv, model_class):
    if conversion_base:
        return "accession:" + str(is_accession_spec) + "_conversion:" + str(conv) + "_model:" + str(model_class)
    else:
        return "accession:" + str(is_accession_spec) + "_conversion:" + "NONE" + "_model:" + str(model_class)


def train(args):
    cnfg = configs.col
    rc = configs.rc
    conversion_base = args.conversion_base
    conv = args.conv
    model_class = args.model_class
    assembly = data_reader.readfasta(cnfg['assembly'])
    sequences_df = preprocess.convert_assembely_to_onehot(cnfg['og'], assembly, from_file=True)
    bw_dic = regrprep.get_bw_feature_seq(cnfg, from_file=True)
    meth_seq_dic = preprocess.get_meth_seq_dic(cnfg, rc, from_file=True)
    meth_seq_dic = {contxt: {chro: np.expand_dims(meth_seq_dic[contxt][chro], axis=1) for chro in meth_seq_dic[contxt].keys()} for contxt in meth_seq_dic.keys()}
    # if is_accession_spec:
    #     raw_snp_df = data_reader.read_syri(cnfg_snp['syri'], annot_type='SNP')
    #     raw_snp_df = raw_snp_df.rename(columns={'chr1': 'chr', 'start1': 'position'})
    #     raw_snp_df['position'] = raw_snp_df['position'].astype(int)
    #     raw_snp_df['value'] = 1
    #
    # else:
    #     raw_snp_df = data_reader.read_raw_mutation_bias_snps(configs.root + 'col-0/epi_data/raw_variants.txt')
    #     if conversion_base:
    #         raw_snp_df = raw_snp_df[((raw_snp_df.REF == conv[0][0]) & (raw_snp_df.ALT == conv[1][0])) | ((raw_snp_df.REF == conv[0][1]) & (raw_snp_df.ALT == conv[1][1]))]
    #     snp_seq = preprocess.make_seq_dic(assembly, raw_snp_df)
    raw_snp_df = preprocess.read_raw_snps(args.accession)
    if conversion_base:
        raw_snp_df = raw_snp_df[((raw_snp_df.seq1 == conv[0][0]) & (raw_snp_df.seq2 == conv[1][0])) | ((raw_snp_df.seq1 == conv[0][1]) & (raw_snp_df.seq2 == conv[1][1]))]
    snp_seq = preprocess.make_seq_dic(sequences_df, raw_snp_df)
    X_p, Y_p = regrprep.input_maker(cnfg, sequences_df, meth_seq_dic, bw_dic, snp_seq, '+', bin_count=args.bin_count, frs=args.frs, include_flanking=args.include_flanking)
    X_n, Y_n = regrprep.input_maker(cnfg, sequences_df, meth_seq_dic, bw_dic, snp_seq, '-', bin_count=args.bin_count, frs=args.frs, include_flanking=args.include_flanking)
    X = pd.concat([X_p, X_n], axis=0)
    Y = pd.concat([Y_p, Y_n], axis=0)
    print('Size of Input before removing outliers : ' + str(len(X)))
    #X, Y = regrprep.remove_outliers(X, Y, threshold=2)
    # if args.transform != None and args.transform == 'boxcox':
    #     Y = Y + 1e-10
    #     Y, _ = boxcox(Y)
    #print('Size of Input after removing outliers : ' + str(len(X)))
    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.1, random_state=42)
    if model_class == 'CNN':
        model = Sequential()
        model.add(Dense(64, input_dim=X.shape[1] - 1,  activation='relu'))
        model.add(Dense(32, activation='relu'))
        model.add(Dense(16, activation='relu'))
        model.add(Dense(1))
        model.compile(loss='mean_squared_error', optimizer='adam')
        model.fit(X_train.loc[:, ~X_train.columns.isin(['ID'])], y_train, epochs=5, verbose=0)
    elif model_class == 'LR':
        model = LinearRegression()
        model.fit(X_train.loc[:, ~X_train.columns.isin(['ID'])], y_train)
    elif model_class == 'SM':
        model = sm.OLS(np.expand_dims(y_train, axis=1), X_train.loc[:, ~X_train.columns.isin(['ID'])])
        model = model.fit()
    y_pred = model.predict(X_test.loc[:, ~X_test.columns.isin(['ID'])])
    if y_pred.shape != y_test.shape:
        y_pred = y_pred[:, 0]
    corr_coef, p_value = pearsonr(y_test, y_pred)
    print('corr_coef, p_value : ' + str(corr_coef) + ' ' + str(p_value), str(args))
    with open("gene_snp_prediction_results.txt", "a") as file_object:
       file_object.write('corr_coef, p_value : ' + str(corr_coef) + ' ' + str(p_value) + ' ' + str(args))
       file_object.write("\n")
    #np.save('y_test.npy', y_test)
    #np.save('y_pred.npy', y_pred)
    # res = pd.DataFrame({'pred': y_pred[:, 0], 'ID': X_test['ID']})
    # res = res.sort_values('pred', ascending=False)
    #evaluate.correlation_plot('./plots/' + get_fn(is_accession_spec, conversion_base, conv, model_class), y_test, y_pred, corr_coef)
    return

def gene_snp_prediction(args):
    cnfg = configs.col
    cnfg_snp = preprocess.convert_og_to_cnfg(args.accession)
    annot_df = data_reader.read_annot(cnfg['gene_annotation'])
    sequences_df = preprocess.convert_assembely_to_onehot(cnfg['og'], data_reader.readfasta(cnfg['assembly']), from_file=True)
    raw_snp_df = preprocess.read_raw_snps(cnfg_snp['og'])
    snp_seq = preprocess.make_seq_dic(sequences_df, raw_snp_df)
    bin_size = int(args.bin_size)
    flanking_region = int(args.flanking_region)
    reg_names = [rg+'-'+str(i) for i in range(int(flanking_region/bin_size)) for rg in ['ds', 'gbl', 'gbr', 'us']]
    feature_set = cnstnts.feature_set
    reg_type_idxs = {'ds':0, 'gbl':1, 'gbr':2, 'us':3}
    res = np.zeros((len(annot_df), len(reg_names), len(feature_set)))
    for idx, reg in enumerate(reg_names):
        if idx % (int(len(reg_names)//20)) == 0:
            print('{} out of {} regions processed to make X'.format(idx, len(reg_names)))
        fdf = pd.read_csv('./dump_files/general_intervals_averages_b%d_fr%d/' %(bin_size, flanking_region) +reg+'.csv')
        reg_type, reg_num = reg.split('-')[0], int(reg.split('-')[1])
        reg_idx = reg_type_idxs[reg_type] * int(flanking_region/bin_size) + reg_num
        res[fdf['Unnamed: 0'], reg_idx, :] = fdf[feature_set].to_numpy()
    res = res[annot_df[annot_df['type'] == 'gene'].index]
    X = res
    annot_df = annot_df[annot_df['type'] == 'gene']
    if args.include_flanking:
        Y = annot_df.apply(lambda row: regprep.count_snps_interval_seq(row['chr'], row['start']-flanking_region, row['end']+flanking_region, snp_seq, sequences_df, None), axis=1) #contain gene flanking regions SNPS
    else:
        Y = annot_df.apply(lambda row: regprep.count_snps_interval_seq(row['chr'], row['start'], row['end'], snp_seq, sequences_df, None), axis=1) #only gene body SNPs
    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.1)
    if args.model_class == 'CNN':
        with tf.device('/device:GPU:0'):
            model = Sequential()
            model.add(Conv2D(128, kernel_size=(1, 5), activation='relu', input_shape=(X.shape[1], X.shape[2], 1), padding='same'))
            model.add(MaxPooling2D(pool_size=(1, 5), strides=(1, 3)))
            model.add(Conv2D(64, kernel_size=(1, 5), activation='relu', padding='same'))
            model.add(MaxPooling2D(pool_size=(1, 5), strides=(1, 3)))
            model.add(Flatten())
            model.add(Dense(512, activation='relu'))
            model.add(Dense(128, activation='relu'))
            model.add(Dense(16, activation='relu'))
            model.add(Dense(1))
            model.compile(loss='mean_squared_error', optimizer='adam')
            model.fit(X_train, y_train, batch_size=32, epochs=5, verbose=1)
    elif args.model_class == 'LR':
        model = LinearRegression()
        model.fit(X_train.loc[:, ~X_train.columns.isin(['ID'])], y_train)
    elif args.model_class == 'SM':
        model = sm.OLS(np.expand_dims(y_train, axis=1), X_train.loc[:, ~X_train.columns.isin(['ID'])])
        model = model.fit()
    #elif args.model_class == 'RandomForest':

    y_pred = model.predict(X_test)
    if y_pred.shape != y_test.shape:
        y_pred = y_pred[:, 0]
    corr_coef, p_value = pearsonr(y_test, y_pred)
    print('corr_coef, p_value : ' + str(corr_coef) + ' ' + str(p_value), str(args))
    with open("gene_snp_prediction_results_new.txt", "a") as file_object:
       file_object.write('corr_coef, p_value : ' + str(corr_coef) + ' ' + str(p_value) + ' ' + str(args))
       file_object.write("\n")


#
#
# args = Namespace()
# accessions = ['c24']
# frss = [500, 1000, 2000]
# bin_counts = [100, 200, 300]
# args.model_class = 'CNN'
# args.conversion_base = False
# args.conv = ('AT', 'CG')
# for accession in accessions:
#     args.accession = accession
#     for include_flanking in [True, False]:
#         args.include_flanking = include_flanking
#         for frs in frss:
#             args.frs = frs
#             for bin_count in bin_counts:
#                 args.bin_count = bin_count
#                 train(args)

args = Namespace()
args.accession = 'c24'
args.model_class = 'CNN'
args.bin_size = 10
args.flanking_region = 2000


for i in range(5):
    for iflan in [True, False]:
        args.include_flanking = iflan
        gene_snp_prediction(args)

#
# with tf.device('/device:GPU:0'):
#     model = Sequential()
#     model.add(Conv2D(128, kernel_size=(1, 5), activation='relu', input_shape=(X.shape[1], X.shape[2], 1), padding='same'))
#     model.add(MaxPooling2D(pool_size=(1, 5), strides=(1, 3)))
#     model.add(Conv2D(64, kernel_size=(1, 5), activation='relu', padding='same'))
#     model.add(MaxPooling2D(pool_size=(1, 5), strides=(1, 3)))
#     model.add(Flatten())
#     model.add(Dense(512, activation='relu'))
#     model.add(Dense(128, activation='relu'))
#     model.add(Dense(16, activation='relu'))
#     model.add(Dense(1))
#     model.compile(loss='mean_squared_error', optimizer='adam')
#     model.fit(X_train, y_train, batch_size=32, epochs=5, verbose=1)
#
# y_pred = model.predict(X_test)
# if y_pred.shape != y_test.shape:
#     y_pred = y_pred[:, 0]
# corr_coef, p_value = pearsonr(y_test, y_pred)
# print('corr_coef, p_value : ' + str(corr_coef) + ' ' + str(p_value), str(args))
#
#
# from sklearn.ensemble import RandomForestRegressor
# rf_regressor = RandomForestRegressor(n_estimators=50, max_depth=10, n_jobs=-1, verbose=2)
# X_train_reshaped = X_train.reshape(X_train.shape[0], -1)
# x_test_reshaped = X_test.reshape(X_test.shape[0], -1)
# rf_regressor.fit(X_train_reshaped, y_train)
# corr_coef, p_value = pearsonr(y_test, y_pred)
# print('corr_coef, p_value : ' + str(corr_coef) + ' ' + str(p_value), str(args))
