import sys
import os

project_root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(project_root)
from preprocess import data_reader
from preprocess import preprocess
import preprocess.configs as configs
import numpy as np
import preprocess.regression_preprocess as regprep
import pandas as pd

#when using this function uncomment the import pandarallele. That is commented due to the python version inconsistencies in Dipankar machine.
def get_genic_avg_marks(annot_df, seq_dic, bin_size=10, flanking_region=2000, avgs=True, force_annot_df=False):
    pandarallel.initialize(progress_bar=False)
    gi_address = './dump_files/general_intervals_b%d_fr%d.csv' %(bin_size, flanking_region)
    if os.path.exists(gi_address) and not force_annot_df:
        general_intervals = pd.read_csv(gi_address)
    else:
        annot_df = annot_df[annot_df.type == 'gene']
        general_intervals = annot_df.parallel_apply(lambda row: regprep.get_gene_intervals(row, bin_size=bin_size, flanking_region=flanking_region), axis=1)
    print(len(general_intervals), ' size of general intervals')
    avg_vlus_df = general_intervals.parallel_apply(lambda row: regprep.get_general_inrvals_avg(row, seq_dic), axis=1)
    if avgs:
        return avg_vlus_df.mean().to_dict()
    else:
        return avg_vlus_df


cnfg = configs.col
annot_df = data_reader.read_annot(cnfg['gene_annotation'])
annot_df = annot_df[annot_df.type == 'gene']


res = {}
sequences_df = preprocess.convert_assembely_to_onehot(cnfg['og'], data_reader.readfasta(cnfg['assembly']), from_file=True)
seq_dic = {}
for key, array in sequences_df.items():
    binary_array = np.logical_or(np.all(array == [0, 1, 0, 0], axis=1), np.all(array == [0, 0, 1, 0], axis=1)).astype(int)
    seq_dic[key] = binary_array
res['GC_content'] = regprep.get_genic_avg_marks(annot_df, seq_dic, bin_size=10, flanking_region=2000, avgs=True)

cnfg = configs.col
rc = configs.rc
bw_root = cnfg['bw_root']
bw_seqs, bw_names = preprocess.read_bwseqs(bw_root, from_file=True)
bw_dic = {bw_names[i]: bw_seqs[i] for i in range(len(bw_names))}
bw_dic = {key.split('-')[0].split('_')[0]: value for key, value in bw_dic.items()}
meth_seq_dic = preprocess.get_meth_seq_dic(cnfg, rc, from_file=True)
seq_dics = {**bw_dic, **meth_seq_dic}

for key in seq_dics.keys():
    print(key)
    res[key] = get_genic_avg_marks(annot_df, seq_dics[key], bin_size=10, flanking_region=2000, avgs=True, force_annot_df=True)

data_reader.save_dic('genicreg_feature_averages_top5percent_gbus_snpdiff.pkl', res)

# bin_avgs = {'us' : np.zeros(100), 'gbl': np.zeros(100), 'gbr': np.zeros(100), 'ds': np.zeros(100)}
# for i in range(100):
#     print(i)
#     bin_avgs['us'][i] = np.mean(annot_df.apply(lambda row: np.mean(seq_dic[row['chr']][row['start']-1000 + (i * 10): row['start'] -1000 + ((i+1) * 10)]) if row['strand'] == '+' else np.mean(seq_dic[row['chr']][row['end']+1000 - ((i+1) * 10): row['end']+1000 - (i * 10)]), axis=1))
#     bin_avgs['gbl'][i] = np.mean(annot_df.apply(lambda row: np.mean(seq_dic[row['chr']][row['start'] + (i * 10): row['start'] + ((i+1) * 10)]) if row['strand'] == '+' else np.mean(seq_dic[row['chr']][row['end'] - ((i+1) * 10): row['end'] - (i * 10)]), axis=1))
#     bin_avgs['gbr'][i] = np.mean(annot_df.apply(lambda row: np.mean(seq_dic[row['chr']][row['end'] - 1000 + (i * 10): row['end']-1000 + ((i+1) * 10)]) if row['strand'] == '+' else np.mean(seq_dic[row['chr']][row['start']+1000 - ((i+1) * 10): row['start']+1000 - (i * 10)]), axis=1))
#     bin_avgs['ds'][i] = np.mean(annot_df.apply(lambda row: np.mean(seq_dic[row['chr']][row['end'] + (i * 10): row['end'] + ((i+1) * 10)]) if row['strand'] == '+' else np.mean(seq_dic[row['chr']][row['start'] - ((i+1) * 10): row['start'] - (i * 10)]), axis=1))
#




