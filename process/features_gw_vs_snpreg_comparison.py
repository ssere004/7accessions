import sys
import os

project_root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(project_root)

from preprocess import data_reader
from preprocess import preprocess
import preprocess.configs as configs
import re
import pandas as pd
from argparse import Namespace
import time
import evaluate.gw_snp_feature_violinplot as violin_plting


def report_feature(bw_dic, meth_seq_dic, snp_seq, args, avgs=True):
    seq_dics = {**bw_dic, **meth_seq_dic}
    if avgs:
        columns = [re.split('[-_]', s)[0] for s in bw_dic.keys()] + [cntx for cntx in meth_seq_dic.keys()] + ['type']
        res = pd.DataFrame(columns=columns, index=[0, 1])
        for seq_key in seq_dics.keys():
                feature_average, gw_average = preprocess.compare_feature_snps(seq_dics[seq_key], snp_seq, args.snp_seq_window)
                res.loc[0, re.split('[-_]', seq_key)[0]] = feature_average
                res.loc[1, re.split('[-_]', seq_key)[0]] = gw_average
        res.loc[0, 'type'] = 'feature_avg'
        res.loc[1, 'type'] = 'gw_avg'
        res['acc'] = args.accession
        res['conv_type'] = args.conv
        return res
    else:
        res = {}
        for seq_key in seq_dics.keys():
            feature_vals, gw_sample_vals = preprocess.compare_feature_snps(seq_dics[seq_key], snp_seq, args.snp_seq_window, avg=False)
            res[re.split('[-_]', seq_key)[0]+'_snps_vals'] = feature_vals
            res[re.split('[-_]', seq_key)[0]+'_gw_vals'] = gw_sample_vals
        return res

def feature_vs_gw_comparison(args):
    cnfg = configs.col
    cnfg_snp = preprocess.convert_og_to_cnfg(args.accession)
    rc = configs.rc
    print('feature analysis started for {}, {}'.format(args.accession, args.scope), time.ctime())
    bw_root = cnfg['bw_root']
    bw_seqs, bw_names = preprocess.read_bwseqs(bw_root, from_file=True, log=False)
    bw_dic = {bw_names[i]: bw_seqs[i] for i in range(len(bw_names))}
    sequences_df = preprocess.convert_assembely_to_onehot(cnfg['og'], data_reader.readfasta(cnfg['assembly']), from_file=True)
    meth_seq_dic = preprocess.get_meth_seq_dic(cnfg, rc, from_file=True)
    raw_snp_df = preprocess.read_raw_snps(cnfg_snp['og'])
    snp_seq = preprocess.make_seq_dic(sequences_df, raw_snp_df)
    if args.scope == 'genic' or args.scope == 'nongenic':
        annot_df = data_reader.read_annot(cnfg['gene_annotation'])
        annot_df = annot_df[annot_df.type == 'gene']
        snp_seq = preprocess.subset_seq_dic(snp_seq, annot_df, reverse=args.scope == 'nongenic')
        for bw in bw_dic.keys():
            bw_dic[bw] = preprocess.subset_seq_dic(bw_dic[bw], annot_df, reverse=args.scope == 'nongenic')
        for c_context in meth_seq_dic.keys():
            meth_seq_dic[c_context] = preprocess.subset_seq_dic(meth_seq_dic[c_context], annot_df, reverse=args.scope == 'nongenic')
    report_df = pd.DataFrame()
    args.conv = 'all'
    if args.averages:
        report_df = pd.concat([report_df, report_feature(bw_dic, meth_seq_dic, snp_seq, args)], ignore_index=True)
    else:
        violin_plting.plot(report_feature(bw_dic, meth_seq_dic, snp_seq, args, avgs=False), './dump_files/gw_snps_features/{}_genic_{}_snpwindow_{}_{}.png'.format((args.accession), str(args.scope), str(args.snp_seq_window), 'all'))
        #data_reader.save_dic('./dump_files/gw_snps_features/{}_genic_{}_snpwindow_{}_{}.pkl'.format((args.accession), str(args.scope), str(args.snp_seq_window), 'all'), report_feature(bw_dic, meth_seq_dic, snp_seq, args, avgs=False))
    print('{} report for all is done'.format(args.accession), time.ctime())
    # ##min_num_conversions = preprocess.find_min_conversion_num(raw_snp_df)
    # for conv in [('AT', 'CG'), ('AT', 'GC'), ('AT', 'TA'), ('CG', 'GC'), ('CG', 'TA'), ('GC', 'TA')]:
    #     #conv = ('AT', 'CG')
    #     raw_snp_sub_df = raw_snp_df[((raw_snp_df.REF == conv[0][0]) & (raw_snp_df.ALT == conv[1][0])) |
    #                                 ((raw_snp_df.REF == conv[0][1]) & (raw_snp_df.ALT == conv[1][1]))]
    #     snp_seq = preprocess.make_seq_dic(sequences_df, raw_snp_sub_df)
    #     if args.scope == 'genic' or args.scope == 'nongenic':
    #         snp_seq = preprocess.subset_seq_dic(snp_seq, annot_df, reverse=args.scope == 'nongenic')
    #     args.conv = conv[0]+'>'+conv[1]
    #     if args.averages:
    #         report_df = pd.concat([report_df, report_feature(bw_dic, meth_seq_dic, snp_seq, args)], ignore_index=True)
    #     else:
    #         violin_plting.plot(report_feature(bw_dic, meth_seq_dic, snp_seq, args, avgs=False), './dump_files/gw_snps_features/{}_scope_{}_snpwindow_{}_{}.png'.format((args.accession), str(args.scope), str(args.snp_seq_window), conv[0]+'-'+conv[1]))
    #         #data_reader.save_dic('./dump_files/gw_snps_features/{}_scope_{}_snpwindow_{}_{}.pkl'.format((args.accession), str(args.scope), str(args.snp_seq_window), conv[0]+'-'+conv[1]), report_feature(bw_dic, meth_seq_dic, snp_seq, args, avgs=False))
    #     print('{} report for {} is done'.format(args.accession, args.conv), time.ctime())
    return report_df

accessions = ['c24', 'mutation_bias', 'g1001_high05', 'g1001_high1', 'g1001_low05', 'g1001_low01', 'ler', 'sha', 'cvi']

accessions = ['c24', 'mutation_bias', 'g1001_high05', 'g1001_low05']
snp_seq_windows = [10]
dfs = []
for accession in accessions:
    for snp_seq_window in snp_seq_windows:
        for scope in ['nongenic', 'genic', 'gw']:
            args = Namespace()
            args.accession = accession
            args.scope = scope
            args.averages = False
            args.snp_seq_window = snp_seq_window
            dfs.append(feature_vs_gw_comparison(args))

#uncomment this if you want the means dataframe not the distributions
#pd.concat(dfs, ignore_index=True).to_csv('./reports/feature_vs_gw_comparison.csv', sep='\t', index=False)
