import sys
import os

project_root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(project_root)
from preprocess import data_reader
from preprocess import preprocess
import preprocess.configs as configs
#from statsmodels.tools.eval_measures import rmse
#import statsmodels.api as sm
import preprocess.regression_preprocess as regprep
from argparse import Namespace
import pandas as pd


def get_extreme_percentile_ids(df, percentile, retrieve_high):
    if retrieve_high:
        threshold = df['corr_pred_real'].quantile(1 - percentile)
        return set(df[df['corr_pred_real'] > threshold]['ID'].tolist())
    else:
        threshold = df['corr_pred_real'].quantile(percentile)
        return set(df[df['corr_pred_real'] <= threshold]['ID'].tolist())


#finding the common genes at top {10} percent of pred-real correlatoin among all the annotatoin dataframes with the corr_pred_real column
def find_shared_corr_coeff(accessions, top_pct=0.1, retrieve_high=True):
    dfs = []
    for accession in accessions:
        dfs.append(pd.read_csv('./dump_files/gene_by_gene_pred_real_coef/annot_df_with_corr_pred_real_{}.csv'.format(accession)))
    for df in dfs:
        df['ID'] = df['attributes'].str.extract(r'ID=([^;]+);')
    common_ids = set.intersection(*[get_extreme_percentile_ids(df, top_pct, retrieve_high) for df in dfs])
    return common_ids

def gene_by_gene_correlatoin(args):
    cnfg = configs.col
    cnfg_snp = preprocess.convert_og_to_cnfg(args.accession)
    reg_names = [rg+'-'+str(i) for i in range(int(args.flanking_region/args.bin_size)) for rg in ['ds', 'gbl', 'gbr', 'us']]
    #if files are not available here you should run the code to save the files with:
    #regprep.save_genic_region_feature_avgs(annot_df, sequences_df, meth_seq_dic, bw_dic, bin_size=10, flanking_region=2000)
    #avg_dics[conv] = regprep.get_snp_reg_prediction(model, bin_size=10, flanking_region=2000, avgs=True, annot_df=annot_df)
    #data_reader.save_dic('./dump_files/avg_dics_plts/avg_dics_convbase_' + args.accession +'_snp_cntx_base:'+str(args.snp_cntx_base) +'.pkl', avg_dics)
    dfs = data_reader.load_dic('./dump_files/pred_genebygene/avg_dics_'+args.accession+'.pkl')
    annot_df = data_reader.read_annot(cnfg['gene_annotation'])
    pred_vals = pd.DataFrame()
    for reg_name in reg_names:
        pred_vals[reg_name] = dfs[reg_name]
    pred_vals = pred_vals[annot_df.type == 'gene']
    annot_df = annot_df[annot_df.type == 'gene']
    sequences_df = preprocess.convert_assembely_to_onehot(cnfg['og'], data_reader.readfasta(cnfg['assembly']), from_file=True)
    raw_snp_df = preprocess.read_raw_snps(cnfg_snp['og'])
    snp_seq = preprocess.make_seq_dic(sequences_df, raw_snp_df)
    avg_vlus_df = regprep.get_genic_avg_marks(annot_df, snp_seq, avgs=False)
    avg_vlus_df = avg_vlus_df[pred_vals.columns]
    annot_df['corr_pred_real'] = preprocess.get_corr_rowwise(avg_vlus_df, pred_vals)
    annot_df.to_csv('./dump_files/gene_by_gene_pred_real_coef/annot_df_with_corr_pred_real_{}.csv'.format(args.accession))
    return annot_df

accessions = ['c24', 'mutation_bias', 'g1001_high05', 'g1001_high1', 'g1001_low05', 'g1001_low01', 'ler', 'sha', 'cvi']

accessions = ['g1001_high05', 'g1001_high1']

for accession in accessions:
    args = Namespace()
    args.accession = accession
    args.snp_cntx_base = False
    args.flanking_region = 2000
    args.bin_size = 10
    args.trans = ''
    args.feature_plotting = True
    gene_by_gene_correlatoin(args)

accessions = ['c24', 'ler', 'sha', 'cvi']

pd.Series(list(find_shared_corr_coeff(accessions, 0.01, retrieve_high=True))).to_csv('./dump_files/gene_by_gene_pred_real_coef/common_high_pred_real_coeff_genes.csv', index=False)
pd.Series(list(find_shared_corr_coeff(accessions, 0.01, retrieve_high=False))).to_csv('./dump_files/gene_by_gene_pred_real_coef/common_low_pred_real_coeff_genes.csv', index=False)

#this is an experiment to find out how much difference there is between the snp ratio gb vs flanking

import numpy as np
from scipy.stats import pearsonr
cnfg = configs.col
cnfg_snp = preprocess.convert_og_to_cnfg('c24')
annot_df = data_reader.read_annot(cnfg['gene_annotation'])
add = './dump_files/gene_by_gene_pred_real_coef/annot_df_with_corr_pred_real_c24.csv'
df = pd.read_csv(add, sep=',')
sequences_df = preprocess.convert_assembely_to_onehot(cnfg['og'], data_reader.readfasta(cnfg['assembly']), from_file=True)
raw_snp_df = preprocess.read_raw_snps(cnfg_snp['og'])
snp_seq = preprocess.make_seq_dic(sequences_df, raw_snp_df)


##you can use this part for plotting any sequence mean at genic region and flanking region.
# tmp1 = np.zeros(200)
# tmp2 = np.zeros(200)
# for i in range(200):
#     print(i)
#     if i < 100:
#         tmp1[i] = np.mean(df.apply(lambda row: np.mean(snp_seq[row['chr']][row['start']-1000 + (i * 10): row['start'] -1000 + ((i+1) * 10)]) if row['strand'] == '+' else np.mean(snp_seq[row['chr']][row['end']+1000 - ((i+1) * 10): row['end']+1000 - (i * 10)]), axis=1))
#     else:
#         j = i - 100
#         tmp2[i] = np.mean(df.apply(lambda row: np.mean(snp_seq[row['chr']][row['start'] + (j * 10): row['start'] + ((j+1) * 10)]) if row['strand'] == '+' else np.mean(snp_seq[row['chr']][row['end'] - ((j+1) * 10): row['end'] - (j * 10)]), axis=1))


# >>> tmp1
# array([0.0062904 , 0.00640856, 0.00630778, 0.00654758, 0.0065441 ,
#        0.00677   , 0.00683951, 0.00668659, 0.00648155, 0.00682561,
#        0.00646069, 0.00688121, 0.00647807, 0.00662404, 0.00669007,
#        0.00647112, 0.00657886, 0.00672482, 0.00663446, 0.00654063,
#        0.00653368, 0.00682561, 0.00654758, 0.00663099, 0.0065163 ,
#        0.00674915, 0.00699242, 0.0070237 , 0.00682213, 0.00661013,
#        0.0069959 , 0.00666226, 0.0070376 , 0.00678738, 0.00692987,
#        0.0067283 , 0.00710016, 0.00686731, 0.00686731, 0.00682561,
#        0.00704803, 0.00712449, 0.00682561, 0.00688469, 0.00678738,
#        0.00721832, 0.00710016, 0.00700285, 0.00735734, 0.0072496 ,
#        0.00753806, 0.0072218 , 0.00711754, 0.00710364, 0.00693334,
#        0.00705498, 0.00735386, 0.0072635 , 0.00730868, 0.00728088,
#        0.00671092, 0.00713144, 0.00711059, 0.00722875, 0.0072913 ,
#        0.00706541, 0.00749983, 0.00738514, 0.0070515 , 0.00711406,
#        0.00735039, 0.00710016, 0.00751025, 0.00726698, 0.00731563,
#        0.00710016, 0.00708278, 0.00691249, 0.00715577, 0.00705846,
#        0.00738514, 0.00715229, 0.00703065, 0.0072774 , 0.00664489,
#        0.00670397, 0.00678738, 0.00642594, 0.00637033, 0.00659276,
#        0.00665531, 0.00662751, 0.00659971, 0.00671092, 0.00649545,
#        0.00642594, 0.00645374, 0.00648132, 0.00646394, 0.0061477 ,


# >>> tmp2
# array([0.00515378, 0.00563684, 0.00534839, 0.00562989, 0.00550825,
#        0.00511208, 0.00541442, 0.00543546, 0.00561251, 0.00551868,
#        0.00550478, 0.00580712, 0.00559166, 0.00558123, 0.00545265,
#        0.00561599, 0.0054735 , 0.0054596 , 0.00508427, 0.00538315,
#        0.00535882, 0.00533797, 0.00543527, 0.00574457, 0.00527889,
#        0.00568549, 0.00558818, 0.00576195, 0.00567854, 0.00597741,
#        0.00549435, 0.00584535, 0.00555691, 0.00559861, 0.00536577,
#        0.00559861, 0.00580017, 0.00559513, 0.00564379, 0.00564379,
#        0.00565074, 0.00586273, 0.00594266, 0.00609557, 0.00593223,
#        0.00558471, 0.00565074, 0.00583145, 0.00568897, 0.00573067,
#        0.00561599, 0.00591833, 0.00577932, 0.00585925, 0.00576195,
#        0.00581755, 0.00537272, 0.00589748, 0.00583145, 0.00549435,
#        0.00583145, 0.00596003, 0.00554301, 0.00564726, 0.00572372,
#        0.00577932, 0.00564031, 0.00548393, 0.00566811, 0.00548393,
#        0.00570634, 0.00556038, 0.00567854, 0.0058108 , 0.00563684,
#        0.00577932, 0.00565074, 0.0055013 , 0.00543527, 0.0057967 ,
#        0.0058801 , 0.00572719, 0.0058106 , 0.00577585, 0.00569939,
#        0.00561946, 0.00550478, 0.00585925, 0.00584535, 0.00552911,
#        0.00587315, 0.00592876, 0.00557776, 0.00542137, 0.0057967 ,
#        0.00569244, 0.00559166, 0.00563336, 0.00557428, 0.00548045])

fls = 2000
tmp1 = df.apply(lambda row: np.mean(snp_seq[row['chr']][row['start']-fls: row['start']]) if row['strand'] == '+' else np.mean(snp_seq[row['chr']][row['end']: row['end']+fls]), axis=1)
tmp2 = df.apply(lambda row: np.mean(snp_seq[row['chr']][row['start']: row['start'] + fls]) if row['strand'] == '+' else np.mean(snp_seq[row['chr']][row['end'] - fls: row['end']]), axis=1)

df['us_gb_snpdiff'] = tmp1 - tmp2
#df['us_gb_snpratio'] = df['us_gb_snpratio'].replace(np.inf, 1000)

df.replace([np.inf, -np.inf], np.nan, inplace=True)
df = df.dropna(subset=['us_gb_snpdiff', 'corr_pred_real'])
corr, _ = pearsonr(df['us_gb_snpdiff'], df['corr_pred_real'])
#corr = 0.27

threshold = df['us_gb_snpdiff'].quantile(0.95)
annot_df = df[df['us_gb_snpdiff'] >= threshold]

df = df.sort_values(by='corr_pred_real', ascending=True)

